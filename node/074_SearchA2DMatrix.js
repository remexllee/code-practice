const searchMatrix = (matrix, target) => {
  if (matrix === null || matrix.length === 0 || matrix[0].length === 0) {
    return false
  }

  let rows = matrix.length
  let cols = matrix[0].length

  let y = cols - 1

  for (let x = 0; x < rows; x++) {
    while (y && target < matrix[x][y]) {
      y--
    }

    if (matrix[x][y] === target) {
      return true
    }
  }

  return false
}
