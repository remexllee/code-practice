const numTrees = n => {
  let result = [1, 1]
  for (let i = 2; i <= n; i++) {
    result[i] = 0
    for (let j = 0; j < i; j++) {
      result[i] += result[j] * result[i - 1 - j]
    }
  }
  return result[n]
}
