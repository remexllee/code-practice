/**
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(numbers, target) {
    let leftIdx = 0, rightIdx = numbers.length - 1
    while (leftIdx < rightIdx) {
        let sum = numbers[leftIdx] + numbers[rightIdx]
        if (sum < target) {
            leftIdx++;
        } else if (sum > target){
            rightIdx--;
        } else {
            return [leftIdx + 1, rightIdx + 1]
        }
    }
    return []
};