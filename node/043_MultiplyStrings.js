const multiply = (num1, num2) => {
  if (
    num1 == null ||
    num2 == null ||
    num1.length == 0 ||
    num2.length == 0 ||
    num1 === '0' ||
    num2 === '0') {
    return '0'
  }

  let arr1 = num1.split('').reverse()
  let arr2 = num2.split('').reverse()
  var result = []

  let i, j

  for (i = 0; i < arr1.length; i++) {
    let carry = 0
    let val1 = ~~(arr1[i])
    
    for (j = 0; j < arr2.length; j++) {
      let val2 = ~~(arr2[j])
      let product = val1 * val2 + carry
      let exist = result[i + j] || 0
      let sum = product + exist
      let digit = sum % 10
      carry = Math.floor(sum / 10)
      result[i + j] = digit
    }

    if (carry > 0) {
      result[i + j] = carry
    }
  }

  result.reverse()
  result = result.join('')

  return result
}

console.log(multiply('123', '456'))
