const swapPairs = (head) => {
  let dummy = new ListNode(0)
  dummy.next = head
  let n1 = dummy
  let n2 = head

  while (n2 !== null && n2.next !== null) {
    let nextStart = n2.next.next

    n1.next = n2.next
    n1.next.next = n2
    n2.next = nextStart

    n1 = n2
    n2 = n2.next
  }

  return dummy.next
}
