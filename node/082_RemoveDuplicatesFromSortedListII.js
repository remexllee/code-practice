const deleteDuplicates = (head) => {
  let root = new ListNode(-1)
  root.next = head
  let pre = head = root

  while (pre) {
    let cur = pre.next
    let isDup = false

    while (cur && cur.next && cur.val === cur.next.val) {
      isDup = true
      let next = cur.next
      cur.next = next.next
      next.next = null
    }

    if (isDup) {
      pre.next = cur.next
      cur.next = null
      continue
    }

    pre = cur
  }

  return root.next
}
