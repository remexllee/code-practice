var isBalanced = function (root) {
  return helper(root) !== -1
}

var helper = function (root) {
  if (!root) { return 0; }

  var left = helper(root.left)
  var right = helper(root.right)
  if (left === -1 || right === -1 || Math.abs(left - right) > 1) { return -1; }
  return 1 + Math.max(left, right)
}
