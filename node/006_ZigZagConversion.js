const convert = (s, numRows) => {
  if (s === null || s.length === 0 || numRows <= 0) {
    return ''
  }

  if (numRows === 1) {
    return s
  }

  let res = ''
  let size = 2 * numRows - 2
  for (let i = 0; i < numRows; i++) {
    for (let j = i; j < s.length; j += size) {
      res += s[j]

      if (i !== 0 && i !== numRows - 1) {
        let tmp = j + size - 2 * i
        if (tmp < s.length) {
          res += s[tmp]
        }
      }
    }
  }
  return res
}

const testString = 'PAYPALISHIRING'
console.log(convert(testString, 3))
