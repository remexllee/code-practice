const nextPermutation = (nums) => {
  let vioIndex = nums.length - 1

  while (vioIndex > 0) {
    if (nums[vioIndex - 1] < nums[vioIndex]) {
      break
    }
    vioIndex--
  }

  if (vioIndex > 0) {
    vioIndex--
    let first = nums.length - 1
    while (first > vioIndex && nums[first] <= nums[vioIndex]) {
      first--
    }

    let temp = nums[vioIndex]
    nums[vioIndex] = nums[first]
    nums[first] = temp

    vioIndex++
  }

  let end = nums.length - 1
  while (end > vioIndex) {
    temp = nums[end]
    nums[end] = nums[vioIndex]
    nums[vioIndex] = temp

    end--
    vioIndex++
  }
}

console.log(nextPermutation([1, 2, 3]))
