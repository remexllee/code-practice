const matchChar = (c, p) => {
  return p === '?' || p === c
}

const isMatch = (s, p) => {
  if (s === null || p === null) {
    return false
  }

  let idxS = 0
  let idxP = 0

  let lenS = s.length
  let lenP = p.length

  let back = false
  let preS = 0
  let preP = 0

  while (idxS < lenS) {
    let charS = s[idxS]
    let charP = p[idxP]

    if (idxP < lenP && matchChar(s[idxS], p[idxP])) {
      idxP++
      idxS++
    } else if (idxP < lenP && p[idxP] === '*') {
      while (idxP < lenP && p[idxP] === '*') {
        idxP++
      }

      back = true
      preS = idxS
      preP = idxP
    } else if (back) {
      idxS = ++preS
      idxP = preP
    } else {
      return false
    }
  }

  while (idxP < lenP && p[idxP] === '*') {
    idxP++
  }

  if (idxS === lenS && idxP === lenP) {
    return true
  }

  return false
}
