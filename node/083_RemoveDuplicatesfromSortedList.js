const deleteDuplicates = (head) => {
  if (head === null || head.next === null) {
    return head
  }

  const cur = head

  while(cur.next) {
    if (cur.val === cur.next.val) {
      cur.next = cur.next.next
    }else {
      cur = cur.next
    }
  }
  return head
}
