const combine = (n, k) => {
  if (k === 0 || n === 0) {
    return []
  }

  let result = []
  let output = []
  generate(result, output, n, k, 1)
  return result
}

let generate = (result, output, n, k, cur) => {
  if (output.length === k) {
    result.push(output.slice())
    return
  }

  if (cur > n) {
    return
  }

  output.push(cur)
  generate(result, output, n, k, cur + 1)
  output.pop()
  generate(result, output, n, k, cur + 1)
}

console.log(combine(4, 4))

// const combine = (n, k) => {
//
//   const result = []
//   if (n === 1) {
//     result.push([n])
//     return result
//   }
//   if (k === 1) {
//     for (let i = 0; i <= n; i++) {
//       result.push([i])
//     }
//     return result
//   }
//
//   for (let i = 1; i <= n; i++) {
//     for (let j = i + 1; j <= n; j++) {
//       if (i != j || n === 1) {
//         result.push([i, j])
//       }
//     }
//   }
//   return result
// }
//

