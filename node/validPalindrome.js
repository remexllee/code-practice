/**
 * @param {string} s
 * @return {boolean}
 */

const isPalindrome = (s, boolean) => {
  let left = 0, right = s.length - 1
  while (left < right) {
    while(left < right && !s[left].match(/^[0-9a-zA-Z]+$/)) left++
    while(left < right && !s[right].match(/^[0-9a-zA-Z]+$/)) right--
    if (s[left].toLowerCase() !== s[right].toLowerCase()) return false
    left++
    right--
  }
  return true
}
