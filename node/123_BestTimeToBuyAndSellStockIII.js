const maxProfit = (prices) => {
  let profitFromZeroToX = []
  let profitFromXToEnd = []
  let min = prices[0]

  for (let x = 1; x < prices.length; x++) {
    let price = prices[x]
    min = Math.min(price, min)
    profitFromZeroToX[x] = Math.max(profitFromZeroToX[x - 1] || 0, price - min)
  }

  let max = prices[prices.length - 1]
  for (x = prices.length - 2; x >= 0; x--) {
    price = prices[x]
    max = Math.max(price, max)
    profitFromXToEnd[x] = Math.max(profitFromXToEnd[x + 1] || 0, max - price)
  }

  let maxProfit = 0
  for (x = 0; x < prices.length; x++) {
    let maxProfitSeperateAtX = (profitFromZeroToX[x] || 0) + (profitFromXToEnd[x] || 0)
    maxProfit = Math.max(maxProfitSeperateAtX, maxProfit)
  }

  return maxProfit
}
