const grayCode = (n) => {
  if (n === 0) {
    return [0]
  }

  let result = grayCode(n - 1)
  let addNumber = 1 << (n - 1)
  let len = result.length

  for (let i = len; i--;) {
    result.push(result[i] + addNumber)
  }

  return result
}
