const singleNumber = nums => {
  let singleNum = 0
  for (let i = 0; i < 32; i++) {
    let bit = 0
    for (let j = 0; j < nums.length; j++) {
      let num = nums[j]
      bit = (bit + ((num >> 1) & 1)) % 3
    }
    singleNum += bit << i
  }

  return singleNum
}
