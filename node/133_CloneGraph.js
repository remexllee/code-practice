const cloneGraph = graph => {
  if (!graph) {
    return graph
  } else {
    return dfs(graph, {})
  }

  function dfs (node, visited) {
    let newNode = visited[node.label] = visited[node.label] || new UndirectedGraphNode(node.label)
    for (let i = 0; i < node.neighbors.length; i++) {
      let neighbor = node.neighbors[i]
      newNode.neighbors[i] = visited[neighbor.label] = visited[neighbor.label]
    }

    return newNode
  }
}
