const permuteUnique = (nums) => {
  return permute(nums.sort(), [])
}

const permute = (nums, partial) => {
  if (nums.length === 0) {
    return [partial]
  }

  let listSol = []
  for (let i = 0; i < nums.length; i++) {
    let endRepeated = i
    while (endRepeated < nums.length && nums[i] === nums[endRepeated]) {
      endRepeated++
    }

    let arrayWithoutI = nums.slice(0, i).concat(nums.slice(i + 1, nums.length))
    let partailSol = partial.concat([nums[i]])
    let sol = permute(arrayWithoutI, partailSol)
    if (sol.length > 0) {
      listSol = listSol.concat(sol)
    }
    i = endRepeated - 1
  }
  return listSol
}
