/**
 * @param {string} str
 * @returns {string}
 */

const reverseWords = (str) => str
  .split(' ')
  .filter(word => word !== '')
  .reverse()
  .join(' ')
