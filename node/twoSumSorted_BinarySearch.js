/**
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */
const binarySearch = (list, key, start) => {
  let leftIdx = start
  let rightIdx = list.length - 1
  while (leftIdx < rightIdx) {
    let middleIdx = ~~((leftIdx + rightIdx) / 2)
    if (list[middleIdx] < key) {
      leftIdx = middleIdx + 1
    } else {
      rightIdx = middleIdx
    }
  }
  return (leftIdx === rightIdx && list[leftIdx] === key) ? leftIdx : -1
}

const twoSum = (numbers, target) => {
  for (let i = 0; i < numbers.length; i++) {
    let j = binarySearch(numbers, target - numbers[i], i + 1)
    console.log(j)
    if (j !== -1) {
      return [i + 1, j + 1]
    }
  }
  return []
}

// O(nlogn) runtime, O(1) space

const numbers = [2, 7, 11, 15]
// console.log(binarySearch(numbers, 2, 0))
console.log(twoSum(numbers, 9))
