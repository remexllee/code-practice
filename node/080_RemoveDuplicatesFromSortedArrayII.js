const removeDuplicates = (nums) => {
  let oc = 1
  let sorted = 0
  for (let i = 1; i < nums.length; i++) {
    if (nums[i] === nums[sorted]) {
      if (oc === 2) {
        continue
      }
      oc++
    } else {
      oc = 1
    }

    sorted++
    nums[sorted] = nums[i]
  }
  return sorted + 1
}
