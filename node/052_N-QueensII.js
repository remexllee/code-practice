const maxSubArray = (nums) => {
  let sum = 0
  let maxSum = -Infinity
  // why Number.MIN_VALUE not work?
  
  for (let i = 0; i < nums.length; i++) {
    sum += nums[i]
    maxSum = Math.max(maxSum, sum)

    if (sum < 0) {
      sum = 0
    }
  }
  return maxSum
}

console.log(Number.MIN_VALUE)
console.log(-Infinity)
console.log(Number.MIN_VALUE > -Infinity)
console.log(Number.MIN_VALUE > -1)

