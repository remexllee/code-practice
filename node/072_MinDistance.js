const minDistance = (word1, word2) => {
  // need to debug to fix 
  let len1 = word1.length
  let len2 = word2.length
  let matrix = []
  let i, j

  if (len1 === 0 || len2 === 0) {
    return Math.max(len1, len2)
  }

  for (i = 0; i <= len1; i++) {
    matrix[i] = []
    matrix[i][0] = i
  }

  for (j = 0; j <= len2; j++) {
    matrix[0][j] = j
  }

  for (i = 1; i <= len1; i++) {
    for (j = 1; j <= len2; j++) {
      if (word1.charAt(i - 1) === word2.charAt(j - 1)) {
        matrix[i][j] = matrix[i - 1][j - 1]
      } else {
        matrix[i][j] = Math.min(matrix[i - 1][j], matrix[i][j - 1], matrix[i - 1][j - 1]) + 1
      }
    }
  }
}
