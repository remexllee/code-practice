const subsetsWithDup = (nums) => {
  nums.sort((a, b) => a > b)
  let result = [[]]
  let current = []
  generate(nums, 0, current, result)
  return result
}

const generate = (nums, index, current, result) => {
  for (let i = index; i < nums.length; i++) {
    let num = nums[i]
    current.push(num)
    result.push(current.slice())
    generate(nums, i + 1, current, result)
    current.pop()

    while (i < nums.length - 1 && nums[i] === nums[i + 1]) {
      i++
    }
  }
}
