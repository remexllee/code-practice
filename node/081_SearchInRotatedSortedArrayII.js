const search = (nums, target) => {
  let start = 0
  let end = nums.length - 1

  while (start <= end) {
    let mid = ~~((end + start) / 2)
    if (nums[mid] === target) {
      return true
    }

    if (nums[start] === nums[mid]) {
      start++
    } else if (nums[start] < nums[mid]) {
      if (target >= nums[start] && target < nums[mid]) {
        end = mid - 1
      } else {
        start = mid + 1
      }
    } else {
      if (target <= nums[end] && target > nums[mid]) {
        start = mid + 1
      } else {
        end = mid - 1
      }
    }
  }
  return false
}
