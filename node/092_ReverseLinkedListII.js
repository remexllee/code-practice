const reverseBetween = (head, m, n) => {
  if (m >= n) {
    return head
  }

  let dummy = new ListNode(0)
  dummy.next = head
  head = dummy

  for (let i = 0; i < m - 1; i++) {
    head = head.next
  }

  let pre = head.next
  let cur = pre.next

  for (i = 0; i < n - m; i++) [cur.next, pre, cur] = [pre, cur, cur.next]

  head.next.next = cur
  head.next = pre

  return dummy.next
}
