const partition = (head, x) => {
  let p = new ListNode(x - 1)
  p.next = head
  head = p
  let pre

  while (p !== null && p.val < x) {
    pre = p
    p = p.next
  }

  if (p !== null) {
    let cur = pre
    while (p !== null) {
      if (p.val < x) { [pre.next, cur.next, cur, p.next, p] = [p.next, p, p, cur.next, pre]
      }
      pre = p
      p = p.next
    }
  }

  temp = head
  head = head.next

  return head
}
