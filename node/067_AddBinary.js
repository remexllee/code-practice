const addBinary = (a, b) => {
  let lenA = a.length
  let lenB = b.length
  let ai = 0
  let bi = 0
  let sum = ''
  let carry = 0

  while (ai < lenA || bi < lenB) {
    let valA = ai < lenA ? ~~(a[lenA - 1 - ai]) : 0
    let valB = bi < lenB ? ~~(b[lenB - 1 - bi]) : 0
    let val = valA + valB + carry
    let rem = val % 2
    carry = val > 1 ? 1 : 0
    sum = rem + sum
    ai++
    bi++
  }

  return carry > 0 ? carry + sum : sum
}
