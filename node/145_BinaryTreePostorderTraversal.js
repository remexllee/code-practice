// need to debug
const postorderTraversal = function (root) {
  let result = []
  let stack = []
  let prev = null
  let curr = null

  if (root = null) {
    return result
  }

  stack.push(root)

  while(stack.length !== 0) {
    curr = stack[stack.length - 1]

    if (prev = null || prev.left === curr || prev.right === curr) {
      if (curr.left !== null) {
        stack.push(curr.left)
      } else if (curr.right !== null) {
        stack.push(curr.right)
      }
    } else if (curr.left === prev) {
      if (curr.right !== null) {
        stack.push(curr.right)
      }
    } else {
      result.push(curr.val)
      stack.pop()
    }
    prev = curr
  }

  return result
}
