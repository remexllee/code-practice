const removeElement = (A, elem) => {
  let i = 0
  let j = A.length - 1

  while (i <= j) {
    if (A[i] === elem) {
      let temp = A[i]
      A[i] = A[j]
      A[j] = temp
      j--
    } else {
      i++
    }
  }

  return i
}
