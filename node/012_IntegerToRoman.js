const intToRoman = (num) => {
  const dict = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I']
  let val = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
  let result = ''

  for (let i = 0; i < val.length; i++) {
    let v = val[i]

    if (num >= v) {
      let count = parseInt(num / v)
      num %= v
      for (let j = 0; j < count;j++) {
        result = result + dict[i]
      }
    }
  }

  return result
}

console.log(intToRoman(3))
console.log(intToRoman(4))
console.log(intToRoman(9))
console.log(intToRoman(58))
