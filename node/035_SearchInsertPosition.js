const searchInsert = (nums, target) => {
  let left = 0
  let right = nums.length - 1

  while (left < right) {
    var mid = left + ~~((right - left) / 2) + 1
    if (nums[mid] === target) {
      return mid
    } else if (nums[mid] > target) {
      right = mid - 1
    } else {
      left = mid + 1
    }
  }

  if (nums[left] < target) {
    return left + 1
  } else {
    return left
  }
}
