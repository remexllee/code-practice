// need to debug
const generateTrees = n => {
  if (n === 0) {
    return []
  }

  return genTreeHelper(1, n)
}

function TreeNode (val) {
  this.val = val
  this.left = this.right = null
}

function genTreeHelper (start, end) {
  let result = []
  if (start > end) {
    return [null]
  }

  for (let i = start; i <= end; i++) {
    let left = genTreeHelper(start, i - 1)
    let right = genTreeHelper(i + 1, end)
    for (let k = 0; k < left.length; k++) {
      for (let j = 0; j < right.length; j++) {
        let node = new TreeNode(i)
        node.left = left[k]
        node.right = right[j]
        result.push(node)
      }
    }
  }
  return result
}
