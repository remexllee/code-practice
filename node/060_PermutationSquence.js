var getPermutation = function (n, k) {
  var nums = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
  var v = 1
  for (var i = 2; i < n; i++) {
    v *= i
  }
  var ans = '',pos
  k = k - 1
  while (n--) {
    pos = 0 | (k / v)
    ans += nums[pos]
    k = k % v
    v /= n
    nums.splice(pos, 1)
  }
  return ans
}
