const minDepth = (root) => {
  if (root === null) {
    return 0
  }

  let queue = []
  queue.push(root)
  let height = 1
  let curLvlCnt = 1
  let nextLvlCnt = 0

  while (queue.length !== 0) {
    let node = queue.shift()
    curLvlCnt--

    if (node.left) {
      queue.push(node.left)
      nextLvlCnt++
    }

    if (node.right) {
      queue.push(node.right)
      nextLvlCnt++
    }

    if (node.left === null && node.right === null) {
      return height
    }

    if(curLvlCnt === 0) {
      height++
      curLvlCnt = nextLvlCnt
      nextLvlCnt = 0
    }
  }
  return height
}
