const lengthOfLastWord = (s) => {
  if (s === null || s.length === 0) {
    return 0
  }
  let count = 0
  for (i = s.length; i--;) {
    if (s[i] === ' ') {
      if (count === 0) {
        continue
      } else {
        return count
      }
    }
    count++
  }
  return count
}
