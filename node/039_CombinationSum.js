const combinationSum = (candidates, target) => {
  let results = []
  comb(candidates.sort(), 0, [], 0, target, results)
  return results
}

const comb = (cand, index, partial, partialSum, target, results) => {
  if (target === partialSum) {
    results.push(partial)
    return
  }

  if (cand.length === index || partialSum > target) {
    return
  }

  comb(cand, index, partial.concat([cand[index]]), partialSum + cand[index], target, results)
  comb(cand, index + 1, partial, partialSum, target, results)
}
