const jump = (nums) => {
  let curMax = 0
  let nJumps = 0
  let i = 0
  let n = nums.length

  while (curMax < n - 1) {
    let lastMax = curMax

    for (; i <= lastMax; i++) {
      curMax = Math.max(curMax, i + nums[i])
    }

    nJumps++

    if (lastMax === curMax) {
      return -1
    }
  }
  return nJumps
}
