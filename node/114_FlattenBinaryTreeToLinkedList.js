/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val
 *     this.left = this.right = null
 * }
 */
/**
 * @param {TreeNode} root
 * @return {void} Do not return anything, modify root in-place instead.
 */

const flatten = root => {
  let stack = []
  let p = root
  while (p !== null || stack.length !== 0) {
    if (p.right !== null) {
      stack.push(p.right)
    }

    if (p.left !== null) {
      p.right = p.left
      p.left = null
    } else if (stack.length !== 0) {
      let node = stack.pop()
      p.right = node
    }
    p = p.right
  }
}
