const generateMatrix = (n) => {
  let matrix = []
  for (let i = 0; i < n; i++) {
    matrix.push([])
  }
  let x = 0
  let y = 0
  let count = 1

  while (0 < n) {
    if (n === 1) {
      matrix[x][y] = count
      break
    }

    for (i = 0; i < n - 1; i++) {
      matrix[x][y++] = count++
    }

    for (i = 0; i < n - 1; i++) {
      matrix[x++][y] = count++
    }

    for (i = 0; i < n - 1; i++) {
      matrix[x][y--] = count++
    }

    for (i = 0; i < n - 1; i++) {
      matrix[x--][y] = count++
    }

    x++
    y++
    n -= 2
  }
  return matrix
}
