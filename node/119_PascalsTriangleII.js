const getRow = (rowIndex) => {
  if (rowIndex === null || rowIndex < 0) {
    return []
  }

  let result = [1]

  for (let i = 1; i <= rowIndex; i++) {
    let cur = []
    for (var j = 0; j <= i; j++) {
      cur[j] = (result[j - 1] || 0) + (result[j] || 0)
    }

    result = cur
  }
  return result
}
