const hasPathSum = (root, sum) => {
  if (root === null) {
    return false
  }

  let left = root.left
  let right = root.right
  if (left === null && right === null) {
    return root.val === sum
  }

  return hasPathSum(left, sum - root.val) ||
    hasPathSum(right, sum - root.val)
}
