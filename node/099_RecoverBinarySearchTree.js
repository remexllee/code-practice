// need to debug
var recoverTree = function(root) {
  var node1, node2;
  var prev = new TreeNode(-Number.MAX_VALUE);
  traverse(root);
  var tmp = node1.val;
  node1.val = node2.val;
  node2.val = tmp;
  function traverse(node) {
    if (!node) return;
    traverse(node.left);
    if (node.val < prev.val) {
      node2 = node;
      if (!node1) node1 = prev;
    }
    prev = node;
    traverse(node.right);
  }
};
