const sumNumbers = root => {
  let total = 0
  if (root === null) {
    return total
  }

  let queue = []
  queue.push(root)

  while(queue.length !== 0) {
    let node = queue.shift()
    if (node.left === null && node.right === null) {
      total += ~~(node.val)
    }

    if (node.left) {
      node.left.val = '' + node.val + node.left.val
      queue.push(node.left)
    }
    if (node.right) {
      node.right.val = '' + node.val + node.right.val
      queue.push(node.right)
    }
  }
  return total
}
