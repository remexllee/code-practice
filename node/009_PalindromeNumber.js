/**
 * @param {number} x
 * @return {boolean}
 */
const isPalindrome = (x) => {
  let sum = 0
  if (x < 0) return false
  if (x < 10) return true
  if (x % 10 === 0) return false

  while (x > sum) {
    sum = sum * 10 + x % 10
    x = parseInt(x / 10)
  }

  if (x === sum || x === parseInt(sum / 10)) {
    return true
  } else {
    return false
  }
}

console.log(isPalindrome(112344))
console.log(isPalindrome(12321))
