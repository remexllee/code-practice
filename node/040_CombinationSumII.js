const combinationSum2 = (candidates, target) => {
  let result = []
  if (candidates === null || candidates.length === 0) {
    return result
  }

  let output = []
  candidates.sort((a, b) => a > b ? 1 : -1)
  generate(candidates, target, 0, result, output)
  return result
}

const generate = (c, t, index, result, output) => {
  if (t < 0) {
    return
  }
  if (t === 0) {
    result.push(output.slice())
    return
  }

  for (let i = index; i < c.length; i++) {
    if (i > index && c[i] === c[i - 1]) {
      continue
    }

    let val = c[i]

    output.push(val)
    generate(c, t - val, i + 1, result, output)
    output.pop()
  }
}
