const levelOrderBottom = root => {
  let q = []
  let result = []
  let temp = []

  if (root === null) {
    return q
  }

  q.push(root)
  let curLvCnt = 1
  let nextLvCnt = 0

  while (q.length !== 0) {
    let p = q.shift()
    curLvCnt--
    temp.push(p.val)

    if (p.left !== null) {
      q.push(p.left)
      nextLvCnt++
    }

    if (p.right !== null) {
      q.push(p.right)
      nextLvCnt++
    }

    if (curLvCnt === 0) {
      curLvCnt = nextLvCnt
      nextLvCnt = 0
      result.unshift(temp.slice())
      temp = []
    }
  }
  return result
}
