const restoreIpAddresses = (s) => {
  let result = []
  let ipNums = []
  findIp(s, 0, result, ipNums)
  return result
}

const findIp = (s, index, result, ipNums) => {
  if (ipNums.length === 4) {
    if (index === s.length) {
      let ip = ipNums.join('.')
      result.push(ip)
    }
    return
  }

  for (let i = index; i < (index + 3) && i < s.length; i++) {
    let str = s.substring(index, i + 1)
    if (isValidNum(str)) {
      ipNums.push(str)
      findIp(s, i + 1, result, ipNums)
      ipNums.pop()
    }
  }
}

const isValidNum = (s) => {
  if (s.length === 0 || s.length > 3) {
    return false
  }

  if (s[0] === '0' && s.length !== 1) {
    return false
  }

  if (s.length === 3 && ~~(s) > 255) {
    return false
  }
  return true
}
