const rotateRight = (head, k) => {
  if (head === null || k === 0) {
    return head
  }
  let dummy = new ListNode(-1)
  dummy.next = head
  let p = head

  let len = 1
  while (p.next !== null) {
    len++
    p = p.next
  }

  k = len - k % len
  p.next = head

  while (k > 0) {
    p = p.next
    k--
  }

  head = p.next
  p.next = null
  return head
}
