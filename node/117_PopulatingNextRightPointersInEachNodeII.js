const connect = (root) => {
  if (!root) {
    return
  }

  let leftEnd = root

  while (leftEnd !== null) {
    let cur = leftEnd
    let dummy = new TreeLinkNode(0)
    let pre = dummy
    while(cur !== null) {
      if (cur.left !== null) {
        pre.next = cur.left
        pre = cur.left
      }

      if (cur.right !== null) {
        pre.next = cur.right
        pre = cur.right
      }

      cur = cur.next
    }
    leftEnd = dummy.next
  }
}
