// Divide and Conquer
function mergeSort (arr) {
  let len = arr.length
  if (len < 2) {
    return arr
  }

  let mid = len >> 1
  let left = arr.slice(0, mid)
  let right = arr.slice(mid)
  return merge(mergeSort(left), mergeSort(right))
}

function merge (left, right) {
  let result = []
  let lLen = left.length
  let rLen = left.length
  l = 0
  r = 0

  while (l < lLen && r < rLen) {
    left[l] < right[r] ? result.push(left[l++]) : result.push(right[r++])
  }

  return result.concat(left.slice(l).concat(right.slice(r)))
}

console.log(mergeSort([1, 5, 6, 2, 7, 7, 23, 11, 42, 7, 66]))