function quickSort (arr) {
  if (arr.length < 2) {
    return arr
  }
  // Random
  // const pivot = arr[Math.floor(Math.random() * arr.length)]

  // Right
  // const pivot = arr[arr.length - 1]

  // Middle
  const pivot = arr[arr.length >> 1]

  let left = []
  let right = []
  let equal = []

  for (let val of arr) {
    if (val < pivot) {
      left.push(val)
    } else if (val > pivot) {
      right.push(val)
    } else {
      equal.push(val)
    }
  }
  return [
    ...quickSort(left),
    ...equal,
    ...quickSort(right)
  ]
}

console.log(quickSort([2, 6, 3, 4, 12, 41, 65, 32]))