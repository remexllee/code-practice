var slidingWindow = function (s, p) {
  const map = {}
  let left = 0
  let right = 0
  let count = p.length
  for (let c of p) {
    map[c] = map[c] ? map[c] + 1 : 1
  }

  while (right < s.length || count === 0) {
    console.log(map)
    if (map[s[left++]]++ >= 0) count++
    if (map[s[right++]]-- >= 1) count--
  }
}

const s = 'ADOBECODEBANC'
const p = 'ABC'

console.log(slidingWindow(s, p))