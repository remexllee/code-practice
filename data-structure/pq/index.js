const PriorityQueue = require('priorityqueuejs')

var queue = new PriorityQueue((a, b) => b - a)
const data = [3, 5, 1, 1, 20]

for (let item of data) {
  queue.enq(item)
  if(queue.size () > 2) {
    queue.deq()
  }
}

console.log(queue.peek())
// const teams = [
//   {count: 6},
//   {count: 2},
//   {count: 16},
//   {count: 100},
//   {count: 23}
// ]
//
// const k = 2
//
// for (let team of teams) {
//   queue.enq(team)
//   if (queue.size() > k) {
//     queue.deq()
//   }
// }
//
// console.log(queue.peek())