const TreeNode = require('../tree/TreeNode')
// const ConvertSortedArrayToBinarySearchTree = require('../06_BinaryTree/108_ConvertSortedArrayToBinarySearchTree')

const DEFAULT_BINARY_SEARCH_TREE_VALUE_BY_ARRAY = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

// const generateBSTByArray = (numberArray = DEFAULT_BINARY_SEARCH_TREE_VALUE_BY_ARRAY) => {
//   return ConvertSortedArrayToBinarySearchTree(numberArray)
// }

// example data from http://btechsmartclass.com/DS/U3_T4.html
const generateBinaryTree = () => {
  const nodeA = new TreeNode('A')

  const nodeB = new TreeNode('B')
  const nodeC = new TreeNode('C')

  const nodeD = new TreeNode('D')
  const nodeF = new TreeNode('F')
  const nodeG = new TreeNode('G')
  const nodeH = new TreeNode('H')

  const nodeI = new TreeNode('I')
  const nodeJ = new TreeNode('J')
  const nodeK = new TreeNode('K')
  const nodeE = new TreeNode('E')

  nodeA.left = nodeB
  nodeA.right = nodeC

  nodeB.left = nodeD
  nodeB.right = nodeF

  nodeC.left = nodeG
  nodeC.right = nodeH

  nodeD.left = nodeI
  nodeD.right = nodeJ

  nodeG.right = nodeK
  nodeK.left = nodeE

  return nodeA
}

const generateSymmetricTree = () => {
  const node1 = new TreeNode(1)
  const node2 = new TreeNode(2)
  const node3 = new TreeNode(2)
  const node4 = new TreeNode(3)
  const node5 = new TreeNode(4)
  const node6 = new TreeNode(4)
  const node7 = new TreeNode(3)

  node1.left = node2
  node1.right = node3

  node2.left = node4
  node2.right = node5

  node3.left = node6
  node3.right = node7

  return node1
}

const generateBinarySearchTree = () => {
  const node1 = new TreeNode(1)
  const node2 = new TreeNode(2)
  const node3 = new TreeNode(3)
  const node4 = new TreeNode(4)
  const node5 = new TreeNode(5)

  node4.left = node2
  node4.right = node5

  node2.left = node1
  node2.right = node3

  return node4
}

module.exports = {
  // generateBSTByArray,
  generateBinaryTree,
  generateBinarySearchTree,
  generateSymmetricTree
}
