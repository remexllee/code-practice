const array = [-2, 1, -3, 4, -1, 2, 1, -5, 4]

function maxSubArray (arr) {
  let maxEndingHere = 0
  let maxSoFar = 0

  for (let num of arr) {
    maxEndingHere = Math.max(0, maxEndingHere + num)
    maxSoFar = Math.max(maxSoFar, maxEndingHere)
  }

  return maxSoFar
}

console.log(maxSubArray(array)) // [4, -1, 2, 1]