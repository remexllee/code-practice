const getUpperBoundDescOrder = (nums, target) => {
  let left = 0
  let right = nums.length

  while (left < right) {
    let mid = ~~(left + (right - left) / 2)
    if (nums[mid] > target) {
      left = mid + 1
    } else {
      right = mid
    }
  }
  return left
}

const nums = [5, 3, 2, 1]
console.log(getUpperBoundDescOrder(nums, 1))
console.log(getUpperBoundDescOrder(nums, 3))
console.log(getUpperBoundDescOrder(nums, 5))
console.log(getUpperBoundDescOrder(nums, 4))