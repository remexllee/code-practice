const TreeNode = require('./TreeNode')

const sortedArrayToBST = (nums) => {
  if (nums.length === 0) {
    return null
  }

  let middle = ~~(nums.length / 2)
  let root = new TreeNode(nums[middle])
  root.left = sortedArrayToBST(nums.slice(0, middle))
  root.right = sortedArrayToBST(nums.slice(middle + 1))
  return root
}

var serialize = function (root) {
  const res = []
  const queue = root ? [root] : []
  while (queue.length) {
    let node = queue.shift()
    if (node) {
      res.push(node.val)
      queue.push(node.left || null)
      queue.push(node.right || null)
    } else {
      res.push(null)
    }
  }
  while (res[res.length - 1] === null) res.pop()
  return JSON.stringify(res)
}

var deserialize = function (data) {

  const getNodeVal = (val) => (val || val === 0) ? new TreeNode(val) : null

  const arr = JSON.parse(data)
  if (!arr.length) return null
  const root = new TreeNode(arr.shift())
  const queue = [root]
  while (queue.length) {
    console.log(arr)
    let node = queue.shift(), val
    node.left = getNodeVal(arr.shift())
    node.right = getNodeVal(arr.shift())
    if (node.left) queue.push(node.left)
    if (node.right) queue.push(node.right)
  }
  return root
}

const flatten = (root) => {
  flattenTree(root)
}

const flattenTree = (node) => {
  if (!node) return node
  let leftTail = flattenTree(node.left)
  let rightTail = flattenTree(node.right)

  if (!leftTail) return rightTail ? rightTail : node
  leftTail.right = node.right
  node.right = node.left
  node.left = null
  return rightTail ? rightTail : leftTail
}

const generateBinaryTree = () => {
  const nodeA = new TreeNode('A')

  const nodeB = new TreeNode('B')
  const nodeC = new TreeNode('C')

  const nodeD = new TreeNode('D')
  const nodeF = new TreeNode('F')
  const nodeG = new TreeNode('G')
  const nodeH = new TreeNode('H')

  const nodeI = new TreeNode('I')
  const nodeJ = new TreeNode('J')
  const nodeK = new TreeNode('K')

  nodeA.left = nodeB
  nodeA.right = nodeC

  nodeB.left = nodeD
  nodeB.right = nodeF

  nodeC.left = nodeG
  nodeC.right = nodeH

  nodeD.left = nodeI
  nodeD.right = nodeJ

  nodeG.right = nodeK

  return nodeA
}
/******
 *           4
 *          / \
 *         2  5
 *        / \
 *       1   3
 *
 * */
const generateBinarySearchTree = () => {
  const node1 = new TreeNode(1)
  const node2 = new TreeNode(2)
  const node3 = new TreeNode(3)
  const node4 = new TreeNode(4)
  const node5 = new TreeNode(5)

  node4.left = node2
  node4.right = node5

  node2.left = node1
  node2.right = node3

  return node4
}

const generateInValidBST = () => {
  // const node0 = new TreeNode(0)
  // const nodeMinus1 =new TreeNode(-1)
  // node0.right = nodeMinus1
  // return node0

  const node1 = new TreeNode(1)
  const node2 = new TreeNode(2)
  const node3 = new TreeNode(3)

  node2.left = node1
  node2.right = node3

  return node2
}

// http://btechsmartclass.com/DS/U3_T4.html
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
function traversalRecursionPreOrder (node) {
  if (!node) return []
  return [
    node.val,
    ...traversalRecursionPreOrder(node.left),
    ...traversalRecursionPreOrder(node.right)
  ]
}

function traversalRecursionInOrder (node) {
  if (!node) return []
  return [
    ...traversalRecursionInOrder(node.left),
    node.val,
    ...traversalRecursionInOrder(node.right)
  ]
}

function traversalRecursionPostOrder (node) {
  if (!node) return []
  return [
    ...traversalRecursionPostOrder(node.left),
    ...traversalRecursionPostOrder(node.right),
    node.val
  ]
}

var preorderTraversal = function (root) {
  if (!root) return []

  const result = []
  const stack = [root]

  while (stack.length) {
    let currentNode = stack.pop()
    result.push(currentNode.val)
    if (currentNode.right) stack.push(currentNode.right)
    if (currentNode.left) stack.push(currentNode.left)
  }

  return result
}

function traversalIterationPreOrder (root) {
  if (!root) return []

  const result = []
  const stack = [root]

  while (stack.length) {
    let currentNode = stack.pop()
    result.push(currentNode.val)
    if (currentNode.right) stack.push(currentNode.right)
    if (currentNode.left) stack.push(currentNode.left)
  }

  return result
}

/******************************
 *  1) Create an empty stack S
 *  2) Initialize current node as root
 *  3) Push the current node to S and set current = current -> left until current is NULL
 *  4) If current is NULL and stack is not empty then
 *     a ) Pop the top item from stack
 *     b ) Print the popped item, set current = poppedItem -> right
 *     c ) Go to step 3
 *  5) If currrent is NULL and stack is empty then we are done
 *
 *  https://www.youtube.com/watch?v=VsxLHGUqAKs
 *******************************/

function traversalIterationInOrder (root) {
  if (!root) return []

  const result = []
  const stack = []
  let currentNode = root

  while (currentNode || stack.length) {
    // console.log(stack.reduce((accu, curr) => {
    //   return accu.concat(curr.val)
    // }, []).join(', '))

    if (currentNode) {
      // console.log(currentNode.val)
      // console.log('---------')
      stack.push(currentNode)
      currentNode = currentNode.left
    } else {
      currentNode = stack.pop()
      result.push(currentNode.val)
      currentNode = currentNode.right
    }
  }

  return result
}

function traversalIterationPostOrder (root) {
  if (!root) return []

  const result = []
  const stack = [root]

  while (stack.length) {
    let currentNode = stack.pop()
    result.unshift(currentNode.val)
    if (currentNode.left) stack.push(currentNode.left)
    if (currentNode.right) stack.push(currentNode.right)
  }

  return result
}

function traversalIterationTemplate (root) {
  if (!root) return []

  const result = []
  const stack = [{node: root, visited: false}]

  while (stack.length) {
    // console.log(stack.reduce((accu, curr) => {
    //   return accu.concat(`${curr.node ? curr.node.val : 'null'}: ${curr.visited}`)
    // }, []).join(', '))
    let {
      node: currentNode,
      visited
    } = stack.pop()

    if (!currentNode) continue

    if (visited) {
      result.push(currentNode.val)
    }
    else {
      stack.push(...[
          {node: currentNode.left, visited: false},
          {node: currentNode, visited: true},
          {node: currentNode.right, visited: false}
        ].reverse()
      )
    }
  }

  return result
}

function traversalByLevel (root) {
  if (!root) return []

  const queue = [root]
  const result = []
  while (queue.length) {
    let levelResult = []

    // key: loop all item in current leve queue.
    let size = queue.length
    for (let i = 0; i < size; i++) {
      let currentNode = queue.shift()
      levelResult.unshift(currentNode.val)

      if (currentNode.left) queue.push(currentNode.left)
      if (currentNode.right) queue.push(currentNode.right)
    }
    result.push(levelResult)
  }

  return result
}

function traversalByZigzagLevel (root) {
  if (!root) return []

  let isLeftToRight = true
  const result = []
  const queue = [root]

  while (queue.length) {
    let size = queue.length
    let levelResult = []
    for (let i = 0; i < size; i++) {

      let currentNode = queue.shift()
      isLeftToRight ? levelResult.push(currentNode.val) : levelResult.unshift(currentNode.val)
      if (currentNode.left) queue.push(currentNode.left)
      if (currentNode.right) queue.push(currentNode.right)
    }
    isLeftToRight = !isLeftToRight
    result.push(levelResult)
  }

  return result
}

function traversalIterationTemplateIsValidBST (root) {
  if (!root) return true

  const result = []
  const stack = [{node: root, visited: false}]
  let lastVal = null

  while (stack.length) {
    let {
      node: currentNode,
      visited
    } = stack.pop()

    if (!currentNode) continue
    // console.log(lastVal, currentNode.val)

    if (lastVal !== null && lastVal >= currentNode.val) return false

    if (visited) {
      result.push(currentNode.val)
      lastVal = currentNode.val
    } else {
      stack.push(...[
          {node: currentNode.left, visited: false},
          {node: currentNode, visited: true},
          {node: currentNode.right, visited: false}
        ].reverse()
      )
    }
  }

  return true
}

function traversalIterationTemplateKthSmallest (root, k) {
  if (!root) return true

  const result = []
  const stack = [{node: root, visited: false}]

  while (stack.length) {
    let {
      node: currentNode,
      visited
    } = stack.pop()

    if (!currentNode) continue
    // console.log(lastVal, currentNode.val)

    if (visited) {
      result.push(currentNode.val)
      console.log(currentNode.val)
      if (--k === 0) return currentNode.val
    } else {
      stack.push(...[
          {node: currentNode.left, visited: false},
          {node: currentNode, visited: true},
          {node: currentNode.right, visited: false}
        ].reverse()
      )
    }
  }
}

module.exports = {
  sortedArrayToBST,
  serialize,
  deserialize,
  generateBinaryTree,
  generateBinarySearchTree,
  generateInValidBST,
  traversalRecursionPreOrder,
  traversalIterationPreOrder,
  traversalRecursionInOrder,
  traversalIterationInOrder,
  traversalRecursionPostOrder,
  traversalIterationPostOrder,
  traversalIterationTemplate,
  traversalByLevel,
  traversalByZigzagLevel,
  traversalIterationTemplateIsValidBST,
  traversalIterationTemplateKthSmallest
}
