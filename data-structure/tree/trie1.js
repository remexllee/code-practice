function Trie () {
  const rootNode = {}

  function insert (word) {
    let curr = rootNode
    word.split('').forEach(ch => curr = curr[ch] = curr[ch] || {})
    curr.isWord = true
  }

  function traverse (word) {
    let curr = rootNode
    for (var i = 0; i < word.length; i++) {
      if (!curr) return null
      curr = curr[word[i]]
    }
    return curr
  }

  function search (word) {
    let node = traverse(word)
    return !!node && !!node.isWord
  }

  function startsWith (word) {
    return !!traverse(word)
  }

  function print () {
    console.log(rootNode)
  }

  return {
    insert,
    search,
    traverse,
    startsWith,
    print
  }
}

const trie = new Trie()
trie.insert('abc')
trie.insert('abe')
trie.insert('ebc')
trie.print()