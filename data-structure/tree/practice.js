function TreeNode (val) {
  this.val = val
  this.left = this.right = null
}

/***************
 *
 *      1
 *     / \
 *    2  3
 *   / \
 *  4  5
 *
 *  preOrder: 1, 2, 4, 5, 3
 *  inOrder: 4, 2, 5, 1, 3
 *  PostOrder: 4, 5, 2, 3, 1
 *
 ********************/

const root = new TreeNode(1)
const left1 = new TreeNode(2)
const right1 = new TreeNode(3)
const left1left = new TreeNode(4)
const left1right = new TreeNode(5)

root.left = left1
root.right = right1
left1.left = left1left
left1.right = left1right

const binaryTreeTraveralRecursive = (root) => {
  if (!root) return

  //preOrder
  //console.log(root.val)

  binaryTreePreOrder(root.left)

  //inOrder
  //console.log(root.val)

  binaryTreePreOrder(root.right)
  //postOrder
  //console.log(root.val)

}

binaryTreePreOrder(root)