const TreeNode = require('./TreeNode')

function inOrderTraversal (root) {
  if (!root) return []
  const result = []
  const stack = []

  while (root || stack.length) {
    while (root) {
      stack.push(root)
      root = root.left
    }
    root = stack.pop()
    result.push(root.val)
    root = root.right
  }

  return result
}

function kthSmallest (root, k) {
  const stack = []

  while (root || stack.length) {
    while (root) {
      stack.push(root)
      root = root.left
    }
    root = stack.pop()
    if (--k == 0) break
    root = root.right
  }

  return root.val
}

function isValidBST (root) {
  if (!root) return true
  const stack = []
  let pre = null

  while (root || stack.length) {
    while (root) {
      stack.push(root)
      root = root.left
    }
    root = stack.pop()
    console.log(root.val)
    if (pre && root.val <= pre.val) return false
    pre = root
    root = root.right
  }

  return true
}

module.exports = {
  inOrderTraversal,
  kthSmallest,
  isValidBST
}