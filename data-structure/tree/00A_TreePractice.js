class Node {
  constructor (val) {
    this.val = val
    this.left = null
    this.right = null
  }
}


const treeNode1 = new Node(1)
const treeNode2 = new Node(2)
const treeNode3 = new Node(3)
const treeNode4 = new Node(4)
const treeNode5 = new Node(5)
const treeNode6 = new Node(6)
const treeNode7 = new Node(7)

// treeNode4.left = treeNode2
// treeNode4.right = treeNode6
// treeNode2.left = treeNode1
// treeNode2.right = treeNode3
// treeNode6.left = treeNode5
// treeNode6.right = treeNode7

treeNode1.left = treeNode2
treeNode1.right = treeNode3
treeNode2.left = treeNode4
treeNode2.right = treeNode5

const inOrderTree = (node) => {
  if(node === null) return
  inOrderTree(node.left)
  console.log(node.val)
  inOrderTree(node.right)
}

const preOrderTree = (node) => {
  if(node === null) return
  console.log(node.val)
  preOrderTree(node.left)
  preOrderTree(node.right)
}

const postOrderTree = (node) => {
  if(node === null) return
  inOrderTree(node.left)
  inOrderTree(node.right)
  console.log(node.val)
}

// console.log('-------------')
// preOrderTree(treeNode1)
//
// console.log('-------------')
// postOrderTree(treeNode1)

const maxTreeDepth = (node, level) => {
  if(node.left) {
    return maxTreeDepth(node.left, level + 1)
  }
  return level
}

console.log(maxTreeDepth(treeNode1, 0))