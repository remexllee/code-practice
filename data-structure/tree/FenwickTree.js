// https://en.wikipedia.org/wiki/Fenwick_tree
// https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/
// https://petr-mitrichev.blogspot.com/2013/05/fenwick-tree-range-updates.html
function update (data, at, by) {
  while (at < data.length) {
    data[at] += by
    at |= (at + 1)
  }
}

function query (data, at) {
  var res = 0
  while (at > 0) {
    res += data[at]
    at = (at & (at + 1)) - 1
  }
  return res
}

//////////////
function updater (dataMul, dataAdd, left, right, by) {
  internalUpdate(dataMul, dataAdd, left, by, -by * (left - 1))
  internalUpdate(dataMul, dataAdd, right, -by, by * right)
}

function internalUpdate (dataMul, dataAdd, at, mul, add) {
  while (at < dataMul.length) {
    dataMul[at] += mul
    dataAdd[at] += add
    at |= (at + 1)
  }
}

function queryr (dataMul, dataAdd, at) {
  var mul = 0
  var add = 0
  var start = at
  while (at >= 0) {
    mul += dataMul[at]
    add += dataAdd[at]
    at = (at & (at + 1)) - 1
  }
  return mul * start + add
}

/////////////////////////////////////
// index start from 1
function update1 (data, at, by) {
  while (at < data.length) {
    data[at] += by
    at += (at & -at)
  }
}

function query1 (data, at) {
  var ans = 0
  while (at > 0) {
    ans += data[at]
    at -= (at & -at)
  }
  return ans
}

function log () {
  var e = document.getElementById('log')
  for (var i = 0; i < arguments.length; i++) {
    e.innerHTML += `${JSON.stringify(arguments[i], null, 2)}`
    if (i < arguments.length - 1)
      e.innerHTML += '&nbsp;|&nbsp;'
  }
  e.innerHTML += '<br/>'
}

// var n=16;
// var data=Array.apply(null, new Array(n)).map(x=>0);
// for (var i=0; i<data.length; i++) {
//   log(data);
//   update(data, i, 1);
// }

//log('==========');
// var data=_.range(n+1).map(_=>0);
// for (var i=1; i<data.length; i++) {
//   //log(data);
//   update1(data,i, 1);
// }

// log('==========');
// var dataMul=Array.apply(null, new Array(n)).map(x=>0);
// var dataAdd=Array.apply(null, new Array(n)).map(x=>0);
// for (var i=0; i<dataMul.length; i++) {
//   log(dataMul, dataAdd);
//   updater(dataMul, dataAdd,i, i,1);
// }
// log('add 5 to 0-4');
// updater(dataMul,dataAdd,0,4,5);
// log(dataMul,dataAdd);
// log('add 3 to 0-3');
// updater(dataMul,dataAdd,0,3,3);
// log(dataMul, dataAdd);
// var q=dataMul.map((_,i)=>queryr(dataMul,dataAdd,i));
// log('cum');
// log(q);

function gentree_ (idx, max, children) {
  var thisnode = {id: idx, children: children}
  if (idx === 0) return thisnode

  var p = idx - (idx & -idx)
  thisnode.parent = p
  return gentree_(p, max, thisnode)
}

function mergeChildren (a, b) {
  if (a.id === b.id && a.parent === b.parent) {
    return {id: a.id, parent: a.parent, children: a.children.concat(b.children)}
  }
  throw new Error('should never go here')
}

function firstBitOn (n) {
  return n === 0 ? -1 : Math.log2(n & -n)
}

function merge (roots) {
  var mergedRoot = roots.reduce((acc, n) => mergeChildren(acc, n))
  var groupedChildren = _.groupBy(mergedRoot.children, 'id')
  mergedRoot.children = _.map(groupedChildren, (children, id) => merge(children))
  return mergedRoot
}

function gentree (max) {
  var paths = _.range(max + 1).map((_, i) => gentree_(i, max, []))
  return merge(paths)
}

function visastree (treedata, div, {width, height, padding}) {
  var tree = d3.layout.tree().size([width, height])

  var nodes = tree.nodes(treedata)
  var links = tree.links(nodes)

  var svg = div
    .append('svg')
    .attr('width', width)
    .attr('height', height)
    .style('padding', padding)

  var diag = d3.svg.diagonal()
  svg.selectAll('.link').data(links)
    .enter().append('path').attr('class', 'link').attr('d', diag)

  var domnodes = svg.selectAll('.node').data(nodes)
  domnodes.enter()
    .append('rect').attr('class', 'node')
    .attr('x', d => d.x - 10).attr('y', d => d.y - 10)
    .attr('width', '20px').attr('height', '20px')
  domnodes.enter()
    .append('text').text(d => d.id).attr('class', 'nodetext')
    .attr('x', d => d.x).attr('y', d => d.y)
}

String.prototype.pad = function (n, ch) {
  return n <= this.length ? this : ch.repeat(n - this.length) + this
}

function visasbit (data, treedata, div, {width, height, padding}) {
  var n = data.length - 1
  var tree = d3.layout.tree().size([width, height])

  var nodes = tree.nodes(treedata)
  var links = tree.links(nodes)

  var scaley = d3.scale.linear().domain([n, 0]).range([0, height])
  var scalex = d3.scale.linear().domain([Math.log2(n), -1]).range([0, width])

  nodes.forEach(n => {
    n.y = scaley(n.id)
    return n
  })
  nodes.forEach(n => {
    n.x = scalex(firstBitOn(n.id))
    return n
  })

  var svg = div
    .append('svg')
    .attr('width', width)
    .attr('height', height)
    .style('padding', padding)

  var svgdefs = svg.append('defs')
  svgdefs.append('marker')
    .attr('id', 'markerArrow')
    .attr('markerWidth', '6')
    .attr('markerHeight', '6')
    .attr('refX', '1')
    .attr('refY', '5')
    .attr('orient', 'auto')
    .attr('viewBox', '0 0 10 10')
    .append('path')
    .attr('d', 'M 0 0 L 10 5 L 0 10 z')
    .style('fill', '#000')

  var diag = function (d) {
    var source = d.source, target = d.target
    var sourcey = scaley(source.id) - 5, targety = scaley(target.id)
    var sourcex = scalex(firstBitOn(source.id)), targetx = scalex(firstBitOn(target.id))
    return `M${targetx} ${targety} L${targetx} ${sourcey}`
  }
  svg.selectAll('.link').data(links)
    .enter().append('path').attr('class', 'link').attr('d', diag)

  var domnodes = svg.selectAll('.node').data(nodes)
  domnodes.enter()
    .append('rect')
    .filter(d => d.id !== 0)
    .attr('class', 'node')
    .attr('x', d => d.x - 10).attr('y', d => d.y - 10)
    .attr('width', '20px').attr('height', d => {
    var from = d.id, to = from - (from & -from) + 0.5
    var fromy = scaley(from), toy = scaley(to)
    return toy - fromy
  })
  domnodes.enter()
    .append('rect')
    .filter(d => d.id !== 0)
    .attr('class', 'nodebox')
    .attr('x', d => d.x - 10).attr('y', d => d.y - 10)
    .attr('width', '20px').attr('height', '20px')
  domnodes.enter()
    .append('text')
    .filter(d => d.id !== 0)
    .text(d => data[d.id]).attr('class', 'nodetext')
    .attr('x', d => d.x).attr('y', d => d.y)

  var noOfBits = Math.ceil(Math.log2(nodes.length))
  // axis
  domnodes.enter()
    .append('path').attr('class', d => d.id === 0 ? 'yaxisline0' : 'yaxisline')
    .attr('d', d => {
      var fromy = scaley(d.id)
      var fromx = scalex(-1) - noOfBits * 15
      var tox = d.id === 0 ? 0 : scalex(firstBitOn(d.id)) + 15
      return `M${fromx} ${fromy} L${tox} ${fromy}`
    })

  domnodes.enter()
    .append('text').text(d => `${d.id} - ${(d.id).toString(2).pad(noOfBits, '0')}`).attr('class', 'yaxis')
    .attr('x', width).attr('y', d => d.y)
    .on('mouseover', function () {console.log(arguments)})
}

function draw (n) {
  const width = 600
  const height = 480
  const padding = 20
  const n = parseInt(document.getElementById('n').value)
  const data = Array.apply(null, new Array(n + 1)).map(x => 0)
  for (var i = 1; i <= data.length; i++) {
    update1(data, i, 1)
  }
  const treedata = gentree(n)
  // log(data);
  // log(treedata);

  visasbit(data, treedata, d3.select('#ft'), {width, height, padding})
  visastree(treedata, d3.select('#ft'), {width, height, padding})
}

window.onload = _ => draw(parseInt(document.getElementById('n').value))
document.getElementById('n').oninput = e => {
  var ft = document.getElementById('ft')
  while (ft.firstChild) ft.removeChild(ft.firstChild)
  draw(parseInt(e.target.value))
}

//new Vivus(document.querySelector('#ft svg'), {duration: 200});
// todo:
// visualize update
// range query with 2 ft
// animation

