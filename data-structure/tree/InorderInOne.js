function TreeNode (val) {
  this.val = val
  this.left = this.right = null
}

/***************
 *
 *      4
 *     / \
 *    2  5
 *   / \
 *  1  3
 *
 *  preOrder: 1, 2, 4, 5, 3
 *  inOrder: 4, 2, 5, 1, 3
 *  PostOrder: 4, 5, 2, 3, 1
 *
 ********************/

const root = new TreeNode(4)
const left1 = new TreeNode(2)
const right1 = new TreeNode(5)
const left1left = new TreeNode(1)
const left1right = new TreeNode(3)

root.left = left1
root.right = right1
left1.left = left1left
left1.right = left1right

// https://leetcode.com/problems/validate-binary-search-tree/discuss/32112/Learn-one-iterative-inorder-traversal-apply-it-to-multiple-tree-questions-(Java-Solution)
const inorderTraversal = (root) => {
  let list = []
  if (!root) return list
  let stack = []
  while (root || stack.length) {
    while (root) {
      stack.push(root)
      root = root.left
    }

    root = stack.pop()
    list.push(root.val)
    root = root.right
  }
  return list
}

console.log(inorderTraversal(root))

const isValidBST = root => {
  if (!root) return true
  let stack = []
  let prev = null

  while (root || stack.length !== 0) {
    while (root) {
      stack.push(root)
      root = root.left
    }

    root = stack.pop()
    if (prev && root.val <= prev.val) return false
    prev = root
    root = root.right
  }
  return true
}

console.log(isValidBST(root))

const kthSmallest = (root, k) => {
  let stack = []
  while (root || stack.length !== 0) {
    while (root) {
      stack.push(root)
      root = root.left
    }
    root = stack.pop()
    if (--k === 0) break
    root = root.right
  }
  return root.val
}

console.log(kthSmallest(root, 2))