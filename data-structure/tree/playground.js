const tree = require('.')
const allInOne = require('./AllInOneSolution')

const defaultBinaryTreeRoot = tree.generateBinaryTree()
const defaultBinarySearchTreeRoot = tree.generateBinarySearchTree()
const inVaildBST = tree.generateInValidBST()

// console.log(tree.traversalRecursionPreOrder(defaultBinaryTreeRoot))
// console.log(tree.traversalIterationPreOrder(defaultBinaryTreeRoot))

// console.log(tree.traversalRecursionInOrder(defaultBinaryTreeRoot))
// console.log(tree.traversalIterationInOrder(defaultBinaryTreeRoot))

// console.log(tree.traversalRecursionPostOrder(defaultBinaryTreeRoot))
// console.log(tree.traversalIterationPostOrder(defaultBinaryTreeRoot))
// console.log(tree.traversalIterationTemplateIsValidBST(defaultBinarySearchTreeRoot))

// console.log(tree.traversalByLevel(defaultBinaryTreeRoot))
// console.log(tree.traversalByZigzagLevel(defaultBinaryTreeRoot))

// console.log(allInOne.inOrderTraversal(defaultBinaryTreeRoot))
// console.log(allInOne.isValidBST(defaultBinarySearchTreeRoot))

// const bst1 = tree.sortedArrayToBST([-10, -3, 0, 5, 9])
const bst2 = tree.deserialize('[3, 1, 4, null, 2]')
console.log(tree.serialize(bst2))
// console.log(bst1)
// console.log(tree.traversalIterationTemplateIsValidBST(inVaildBST))
// console.log(tree.traversalIterationTemplateKthSmallest(bst2, 1))