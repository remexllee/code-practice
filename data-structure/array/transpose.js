const util = require('util')

function transpose (matrix) {
  const width = matrix.length ? matrix.length : 0
  const height = matrix[0] instanceof Array ? matrix[0].length : 0
  if (height === 0 || width === 0) return []
  const newMatrix = Array(height).fill(0).map(item => [])
  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      newMatrix[i][j] = matrix[j][i]
    }
  }
  return newMatrix
}

const transposeInplace = (matrix) =>
  matrix
    .reverse()
    .forEach((row, rowIndex) =>
      [...Array(rowIndex).keys()]
        .forEach(columnIdx => [
            matrix[rowIndex][columnIdx],
            matrix[columnIdx][rowIndex]
          ] = [
            matrix[columnIdx][rowIndex],
            matrix[rowIndex][columnIdx]
          ]
        ))

const matrix = [
  [1, 2, 3, 4],
  [5, 6, 7, 8]
]

const prettyPrintArray = (matrix) => {
  matrix.forEach(row => console.log(row))
}

// prettyPrintArray(matrix)
console.log('--------------------')
prettyPrintArray(transpose(matrix))
// prettyPrintArray(transposeInplace(matrix))

// const clockWise90degree = (matrix) => transpose(matrix.reverse())

// const counterClockWise90degree = (matrix) => transpose(matrix).reverse()
// prettyPrintArray(counterClockWise90degree(matrix))

const buildMatrixByNM = (n, m, fillValue = 0) =>
  [...Array(n)]
    .map(row =>
      [...Array(m)]
        .map(column => fillValue))

console.log(buildMatrixByNM(3, 2))