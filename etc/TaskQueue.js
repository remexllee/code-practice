class TaskQueue {
  constructor (concurrency) {
    this.concurrency = concurrency
    this.running = 0
    this.queue = []
  }

  pushTask (task) {
    this.queue.push(task)
    this.next()
  }

  next () {
    while (this.running < this.concurrency && this.queue.length) {
      const task = this.queue.shift()
      task(() => {
        this.running--
        this.next()
      })
      this.running++
    }
  }
}

let taskQueue = new TaskQueue(2)
let taskGenerator = (name, timeout) => {
  return (done) => {
    setTimeout(() => {
      console.log(name)
      done()
    }, timeout)
  }
}
let task1 = taskGenerator('task1', 3000)
let task2 = taskGenerator('task2', 1000)
let task3 = taskGenerator('task3', 1000)
let task4 = taskGenerator('task4', 1000)
// order: task2 task3 task1 task4

taskQueue.pushTask(task1)
taskQueue.pushTask(task2)
taskQueue.pushTask(task3)
taskQueue.pushTask(task4)
