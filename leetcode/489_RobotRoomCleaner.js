function cleanRoom (robot) {
  const seen = new Set()
  const dirs = [[-1, 0], [0, 1], [1, 0], [0, -1]]
  dfs(0, 0, 0)

  function dfs (dir, r, c) {
    robot.clean()
    seen.add(r + ':' + c)
    for (let i = 0; i < 4; i++) {
      const [dr, dc] = dirs[(dir + i) % 4]
      const rr = r + dr
      const cc = c + dc

      if (seen.has(rr + ':' + cc) || !robot.move()) {
        robot.turnRight()
      } else {
        dfs((dir + i) % 4, rr, cc)
        robot.turnLeft()
      }
    }
    robot.turnRight()
    robot.turnRight()
    robot.move()
  }
}