const DIRECTIONS = [[-1, 0], [0, 1], [0, -1], [1, 0]]

function isValidDirection (matrix, i, j) {
  return i >= 0 && i < matrix.length && j >= 0 && j < matrix[i].length
}

function getCache (cache, i, j) {
  if (!cache[i]) { cache[i] = [] }
  return cache[i][j]
}

function setCache (cache, i, j, val) {
  if (!cache[i]) { cache[i] = [] }
  cache[i][j] = val
}

function dfs (matrix, i, j, cache) {
  if (getCache(cache, i, j) !== undefined) {
    return getCache(cache, i, j)
  }
  let val = matrix[i][j], count = 0
  for (let direction of DIRECTIONS) {
    const x = direction[0], y = direction[1]
    if (isValidDirection(matrix, i + x, j + y) && matrix[i + x][j + y] > val) {
      count = Math.max(count, dfs(matrix, i + x, j + y, cache))
    }
  }
  setCache(cache, i, j, count + 1)
  return count + 1
}

var longestIncreasingPath = function (matrix) {
  let count = 0, cache = [], i, j
  for (i = 0; i < matrix.length; i++) {
    for (j = 0; j < matrix[i].length; j++) {
      count = Math.max(count, dfs(matrix, i, j, cache))
    }
  }
  return count
}