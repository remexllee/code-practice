var numFriendRequests = function (ages) {
  var numInAge = []
  var noMoreThanAge = []
  ages.forEach((age) => {
    if (!numInAge[age]) {
      numInAge[age] = 1
    } else {
      numInAge[age] += 1
    }
  })
  var count = 0
  for (var i = 15; i < 121; i++) {
    noMoreThanAge[i] = (numInAge[i] || 0) + (noMoreThanAge[i - 1] || 0)
    count +=
      (numInAge[i] || 0) *
      ((noMoreThanAge[i] || 0) - (noMoreThanAge[parseInt(i / 2) + 7] || 0) - 1)
  }
  return count
}