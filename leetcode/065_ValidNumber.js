var isNumber = function (s) {
  const valid = new Set('012345678910'.split(''))
  const prefix = new Set('-+')
  let eCount = 0
  let decCount = 0
  let numStart = 0
  let afterE = false

  s = s.trim()
  if (s.length === 0) return false

  for (let i = 0; i < s.length; i++) {
    const c = s.charAt(i)

    if (i === 0) {
      if (!prefix.has(c) && !valid.has(c) && c !== '.') {
        return false
      } else if (prefix.has(c)) {
        numStart = i + 1
      } else if (c === '.') {
        decCount++
        numStart = i + 1
      }

    } else if (i === numStart) {
      if (decCount === 0 && c === '.') {
        decCount++
        numStart++
      } else if (!valid.has(c)) return false
    } else if (afterE) {
      if (!prefix.has(c) && !valid.has(c)) {
        return false
      } else if (prefix.has(c)) {
        numStart = i + 1
      }
      afterE = false
    } else {
      if (c === 'e') {
        afterE = true
        if (eCount < 1 && i < s.length - 1) eCount++
        else return false
      } else if (c === '.') {
        if (decCount === 1 || eCount === 1) return false
        else decCount++
      } else if (!valid.has(c)) {
        return false
      }
    }
  }
  return (numStart < s.length)
}

const isNumber = s => {
  if (s === ' ') return false
  return !isNaN(Number(s))
}