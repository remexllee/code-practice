const multiply = (num1, num2) => {
  let pos = Array(num1.length + num2.length).fill(0),
    res = ''

  for (let i = num1.length - 1; i >= 0; i--) {
    for (let j = num2.length - 1; j >= 0; j--) {
      let mul = +num1[i] * +num2[j]
      let p1 = i + j, p2 = i + j + 1
      let sum = mul + pos[p2]

      pos[p1] += Math.floor(sum / 10)
      pos[p2] = sum % 10
    }
  }

  for (let i = 0; i < num1.length + num2.length; i++) {
    if (!(res.length === 0 && pos[i] === 0)) {
      res += pos[i]
    }
  }

  return res.length === 0 ? '0' : res
}