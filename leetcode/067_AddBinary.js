var addBinary = function (a, b) {
  let result = ''
  let carry = 0

  let length = Math.max(a.length, b.length)

  let stringA = a.padStart(length, '0')
  let stringB = b.padStart(length, '0')

  // from right to left do the add math
  for (let i = length - 1; i >= 0; i--) {
    let numberA = ~~(stringA[i])
    let numberB = ~~(stringB[i])

    let currrentResult = numberA + numberB + carry
    result = (currrentResult % 2).toString() + result
    carry = currrentResult >= 2 ? 1 : 0
  }
  return carry === 1 ? 1 + result : result
}

// console.log(addBinary('11', '1'))
// console.log(addBinary('1010', '1011'))
// console.log(addBinary('1111', '1111')) // 11110
console.log(addBinary('1', '100000001'))