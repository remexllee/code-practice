const maxDepth = (root) => {
  if (root === null) {
    return 0
  }

  return 1 + Math.max(maxDepth(root.left), maxDepth(root.right))
}

class Node {
  constructor (val) {
    this.val = val
    this.left = null
    this.right = null
  }
}


const treeNode1 = new Node(1)
const treeNode2 = new Node(2)
const treeNode3 = new Node(3)
const treeNode4 = new Node(4)
const treeNode5 = new Node(5)
const treeNode6 = new Node(6)
const treeNode7 = new Node(7)


treeNode1.left = treeNode2
treeNode2.right = treeNode3
treeNode3.left = treeNode4
treeNode4.left = treeNode5

const depth = () => {

}