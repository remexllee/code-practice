var canAttendMeetings = function (intervals) {
  intervals.sort(function (a, b) {
    return a.start - b.start
  })

  // loop length is intervals length - 1
  for (var i = 0; i < intervals.length - 1; i++) {
    if (intervals[i].end > intervals[i + 1].start) {
      return false
    }
  }
  return true
}
