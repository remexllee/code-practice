/**
 * initialize your data structure here.
 */
var MinStack = function () {
  this.values = []
  this.minValue
}

/**
 * @param {number} x
 * @return {void}
 */
MinStack.prototype.push = function (x) {
  this.values.push([x, this.minValue])
  if (this.values.length === 1) {
    this.minValue = x
  } else if (x < this.minValue) {
    this.minValue = x
  }
}

/**
 * @return {void}
 */
MinStack.prototype.pop = function () {
  let items = this.values.pop()
  this.minValue = items[1]
}

/**
 * @return {number}
 */
MinStack.prototype.top = function () {
  return this.values[this.values.length - 1][0]
}

/**
 * @return {number}
 */
MinStack.prototype.getMin = function () {
  return this.minValue
}

const minStack = new MinStack()
console.log(minStack.push(-2))
console.log(minStack.push(0))
console.log(minStack.push(-3))
console.log(minStack.getMin())   // --> Returns -3.
console.log(minStack.pop())
console.log(minStack.top())      // --> Returns 0.
console.log(minStack.getMin())   // --> Returns -2.