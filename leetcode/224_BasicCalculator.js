var calculate = function (s) {
  if (!s.length) {
    return 0
  }
  s = '(' + s + ')'
  let p = [0]
  return calc(s, p)

  function calc (s, p) {
    let val = 0
    let i = p[0]
    let oper = 1
    let num = 0
    while (i < s.length) {
      let c = s.charAt(i)

      switch (c) {
        case '+':
          val = val + oper * num
          num = 0
          oper = 1
          i++
          break
        case '-':
          val = val + oper * num
          num = 0
          oper = -1
          i++
          break
        case '(':
          p[0] = i + 1
          val = val + oper * calc(s, p)
          i = p[0]
          break
        case ')':
          p[0] = i + 1
          return val + oper * num
        case ' ':
          i++
          continue
        default:
          num = num * 10 + Number(c)
          i++
      }
    }
    return val
  }
}