var rightSideView = function (root) {
  let lastDepth = 0
  let ans = []

  let traverse = (node, level) => {
    if (!node) {
      return
    }

    if (lastDepth < level) {
      ans.push(node.val)
      lastDepth = level
    }

    if (node.right) {
      traverse(node.right, level + 1)
    }
    if (node.left) {
      traverse(node.left, level + 1)
    }
  }

  traverse(root, 1)
  return ans
}