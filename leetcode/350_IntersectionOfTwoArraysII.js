var intersect = function (nums1, nums2) {
  let map = nums1.reduce((acc, num) => {
    if (acc[num]) acc[num]++
    else acc[num] = 1
    return acc
  }, {})

  return nums2.filter(num => {
    if (map[num]) {
      map[num]--
      return true
    }
  })
}