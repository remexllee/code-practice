function TreeNode (val) {
  this.val = val
  this.left = this.right = null
}

var longestUnivaluePath = function (root) {
  var helper = function (node) {
    if (!node) return 0

    var left = helper(node.left)
    var right = helper(node.right)

    if (node.left) {
      left += node.left.val === node.val ? 1 : -left
    }

    if (node.right) {
      right += node.right.val === node.val ? 1 : -right
    }

    max = Math.max(max, left + right)
    return Math.max(left, right)
  }

  var max = 0
  helper(root)
  return max
}

const root = new TreeNode(5)
const node1 = new TreeNode(4)
const node2 = new TreeNode(5)
const node3 = new TreeNode(1)
const node4 = new TreeNode(1)
const node5 = new TreeNode(5)

root.left = node1
root.right = node2
node1.left = node3
node1.right = node4
node2.right = node5

console.log(longestUnivaluePath(root))

/****************************
 *
 *            5
 *           / \
 *          4   5
 *        /  \   \
 *       1   1    5
 *
 ****************************/