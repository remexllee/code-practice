// https://leetcode.com/problems/continuous-subarray-sum/discuss/99499/Java-O(n)-time-O(k)-space

// num sum set         mod   has   preMod
// 23  23  {}           5     f     0
// 2   25  {0}          1     f     5
// 6   31  {0, 5}       1     f     1
// 4   35  {0, 5, 1}    5     t

var checkSubarraySum = function (nums, k) {
  let sum = 0
  let preMod = 0
  let set = new Set()
  for (let num of nums) {
    sum += num
    let mod = k === 0 ? sum : sum % k
    if (set.has(mod)) {
      return true
    } else {
      set.add(preMod)
      preMod = mod
    }
  }
  return false
}

const example1 = [23, 2, 4, 6, 7]
const example2 = [23, 2, 6]

// console.log(checkSubarraySum(example1, 6))
// console.log(checkSubarraySum(example2, 6))
// console.log(checkSubarraySum(example2, 0))
console.log(checkSubarraySum([23, 2, 6], 6))
// console.log(checkSubarraySum([0, 0], 0))
// console.log(checkSubarraySum([0], 0))

