/**
 * @param {number} k
 * @param {number[]} nums
 */
var KthLargest = function (k, nums) {
  this.k = k
  this.heap = [-1].concat(nums.slice(0, k))
  if (this.heap.length <= k) return this
  heapMake(this.heap)
  nums.slice(k).forEach(x => heapAdd(this.heap, x))
}

/**
 * @param {number} val
 * @return {number}
 */
KthLargest.prototype.add = function (val) {
  if (this.heap.length === this.k) {
    this.heap.push(val)
    heapMake(this.heap)
    return this.heap[1]
  }
  heapAdd(this.heap, val)
  return this.heap[1]
}

function heapAdd (arr, num) {
  if (num <= arr[1]) return
  arr[1] = num
  heapAjust(arr, 1, arr.length - 1)
  return arr[1]
}

function heapMake (arr) {
  for (let i = Math.floor((arr.length - 1) / 2); i > 0; i--) {
    heapAjust(arr, i, arr.length - 1)
  }
}

function heapAjust (arr, s, e) {
  for (let i = 2 * s; i <= e; i *= 2) {
    if (i < e && arr[i] > arr[i + 1]) i++
    if (arr[s] <= arr[i]) break
    let tmp = arr[i]
    arr[i] = arr[s], arr[s] = tmp
    s = i
  }
}

let k = 3
let arr = [4, 5, 8, 2]
let kthLargest = new KthLargest(3, arr)
console.log(kthLargest.add(3))   // returns 4
console.log(kthLargest.add(5))   // returns 5
console.log(kthLargest.add(10))  // returns 5
console.log(kthLargest.add(9))   // returns 8
console.log(kthLargest.add(4))   // returns 8