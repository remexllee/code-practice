var largestPalindrome = function(n) {
  if(n == 1) return 9
  var high = 10 ** n, low = high / 10
  for(var i = high - 1, d; i > low; i--){
    var j = i,r = 0
    while(j != 0){
      d = j % 10
      j = (j - d)/ 10
      r = r*10 + d
    }
    r += i * high
    for(var k = high - 1; k * k > r; k--){
      if(r % k ==  0)
        return r % 1337
    }
  }
}

console.log(largestPalindrome(10))