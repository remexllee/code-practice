const rotate = (matrix) => {
  rotateColumns(matrix)
  rotateEachDiagonal(matrix)
}

const rotateColumns = (matrix) => {
  for (let j = 0; j < matrix.length; j++) {
    let low = 0
    let ceil = matrix.length - 1
    while (low < ceil) {
      swap(matrix, low, j, ceil, j)
      low++
      ceil--
    }
  }
}

const rotateEachDiagonal = (matrix) => {
  for (let i = 0; i < matrix.length; i++) {
    for (let j = i; j < matrix.length; j++) {
      swap(matrix, i, j, j, i)
    }
  }
}

const swap = (matrix, i1, j1, i2, j2) => {
  let aux = matrix[i1][j1]
  matrix[i1][j1] = matrix[i2][j2]
  matrix[i2][j2] = aux
}
