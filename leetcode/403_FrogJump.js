function canCross (stones) {
  let dp = [[0]]

  for (let i = 1; i < stones.length; i++) {
    dp[i] = []
    for (let j = 0; j < i; j++) {
      for (let step of dp[j]) {
        if (Math.abs(stones[j] + step - stones[i]) <= 1) {
          dp[i].push(stones[i] - stones[j])
          break
        }
      }
    }
  }

  return dp[stones.length - 1].length > 0
}