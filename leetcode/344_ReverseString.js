var reverseString = function (s) {
  let length = s.length
  let half = length % 2 === 1 ? ~~(length / 2) + 1 : ~~(length / 2)
  let stringArray = Array.from(s)
  for (let i = 0; i < half; i++) {
    [stringArray[i], stringArray[length - i]] = [stringArray[length - i], stringArray[i]]
  }
  return stringArray.join('')
}

console.log('1234567')

var reverseString = function (s) {
  let result = ''
  for (let i = s.length - 1; i >= 0; i--) {
    result += s[i]
  }
  return result
}

// How do I replace a character at a particular index in JavaScript?
// https://stackoverflow.com/questions/1431094/how-do-i-replace-a-character-at-a-particular-index-in-javascript
// In JavaScript, strings are immutable, which means the best you can do is create a new string with the changed content,
// and assign the variable to point to it.
//
// Java版本的解释很好
// https://leetcode.com/problems/reverse-string/discuss/80937/JAVA-Simple-and-Clean-with-Explanations-6-Solutions
//
// 三种方法
// 1. for loop from end to start, result += s[i], 最快
// 2. Array.from(s).reverse().join() 16%
// 3. in place swap, need to convert to array 最慢 6%