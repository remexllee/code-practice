var maxPoints = function (points) {
  let [res, roots, n] = [0, new Set(), points.length]
  for (let i = 0; i < n; i++) {
    if (!roots.has((points[i].x, points[i].y))) {
      roots.add((points[i].x, points[i].y))
      let m = {}, dup = path = 0, cur
      for (let j = 0; j < n; j++) {
        if (i != j) {
          cur = (points[i].y - points[j].y) * 100 / (points[i].x - points[j].x)
          if (isNaN(cur)) {
            if (points[i].y == points[j].y) {
              dup += 1
              continue
            } else {
              cur = 'ver'
            }
          }
          m[cur] = cur in m ? m[cur] + 1 : 1
          if (m[cur] > path) {
            path = m[cur]
          }
        }
      }
      if (path + dup + 1 > res) {
        res = path + dup + 1
      }
    }
  }
  return res
}
