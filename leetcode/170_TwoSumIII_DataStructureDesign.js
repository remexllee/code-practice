var TwoSum = function () {
  this.numbers = []
  this.map = new Map()
}

TwoSum.prototype.add = function (number) {
  if (this.map.has(number)) {
    this.map.set(number, 2)
  }
  else {
    this.map.set(number, 1)
  }
  this.numbers.push(number)
}

TwoSum.prototype.find = function (value) {
  let test = value / 2
  if (this.map.has(test) && this.map.get(test) > 1) {
    return true
  }

  for (let i = 0; i < this.numbers.length; i++) {
    if (this.map.has(value - this.numbers[i]) && (value - this.numbers[i] != this.numbers[i])) return true
  }
  return false
}
