/**
 * @param {string[]} words1
 * @param {string[]} words2
 * @param {string[][]} pairs
 * @return {boolean}
 */
var areSentencesSimilar = function (words1, words2, pairs) {
  if (words1.length !== words2.length) return false
  const map = {}
  for (const p of pairs) {
    map[p[0]] = map[p[0]] || {}
    map[p[1]] = map[p[1]] || {}
    map[p[0]][p[1]] = 1
    map[p[1]][p[0]] = 1
  }

  for (let i = 0; i < words1.length; i++) {
    if (words1[i] !== words2[i]
      && (!map[words1[i]] ||
        !map[words1[i]][words2[i]])) {
      return false
    }
  }
  return true
}
