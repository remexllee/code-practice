// const searchRange = (nums, target) => {
//   let left = 0
//   let right = nums.length - 1
//
//   while (left < right) {
//     let mid = ~~(left + (right - left) / 2)
//     if (nums[mid] > target) {
//       right = mid
//     } else {
//       left = mid + 1
//     }
//   }
//
//   return left - 1
// }
//
// console.log(searchRange([1, 2, 2, 2, 4, 4, 5], 2))

var searchRange = function (nums, target) {
  if (!nums.length) {
    return [-1, -1]
  }
  const start = binarySearch(nums, target, 0)
  const end = binarySearch(nums, target, 1)
  return [start, end]
}

const binarySearch = (nums, target, first) => {
  let start = 0
  let end = nums.length - 1
  let foundIndex = -1
  while (start <= end) {
    let mid = Math.floor((start + end) / 2)
    if (nums[mid] === target) {
      foundIndex = mid
      if (first) {
        start = mid + 1
      } else {
        end = mid - 1
      }
    } else if (nums[mid] < target) {
      start = mid + 1
    } else {
      end = mid - 1
    }
  }
  return foundIndex
}