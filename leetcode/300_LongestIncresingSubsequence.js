// https://leetcode.com/problems/longest-increasing-subsequence/discuss/134357/Concise-99-JavaScript-(DP-with-binary-search)

var lengthOfLIS = function (nums) {
  if (!nums.length) { return 0 }
  let dp = [nums[0]]
  for (let i = 1; i < nums.length; i++) {
    let min = 0
    let max = dp.length
    while (max > min) {
      mid = ~~((min + max) / 2)
      dp[mid] < nums[i] ? min = mid + 1 : max = mid
    }
    dp[max] = nums[i]
  }
  return dp.length
}
