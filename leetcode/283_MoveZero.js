/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function (nums) {
  let count = 0

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] !== 0) {
      nums[count] = nums[i]
      count++
    }
  }

  while (count < nums.length) {
    nums[count++] = 0
  }
}

// const test = [1, 2, 3, 0, 0]
const test = [0, 0, 1, 2, 3]
console.log(moveZeroes(test))
console.log(test)

const str = 'A man, a plan, a canal: Panama'
console.log(str.replace(/\W/g, '').toLowerCase())
// amanaplanacanalpanama