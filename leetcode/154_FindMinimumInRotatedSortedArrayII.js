let findMin = function (nums) {
  let last = nums.length - 1
  let r = last
  let l = 0
  while (l <= r) {
    if (nums[l] === nums[last]) {
      --last
      r = Math.min(r, last)
      continue
    }
    if (nums[l] < nums[last]) {
      return nums[l]
    }
    let m = (l + r) >> 1
    if (nums[m] > nums[last]) {
      l = m + 1
    } else {
      r = m - 1
    }
  }
  return nums[l]
}