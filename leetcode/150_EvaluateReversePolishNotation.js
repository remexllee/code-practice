// https://leetcode.com/problems/evaluate-reverse-polish-notation/discuss/134672/JavaScript-solution

function evalRPN (tokens) {
  const ops = {
    '+': (a, b) => a + b,
    '-': (a, b) => a - b,
    '*': (a, b) => a * b,
    '/': (a, b) => ~~(a / b)
  }

  let stack = []

  for (let n of tokens) {
    if (ops[n]) {
      const fn = ops[n]
      const b = stack.pop()
      const a = stack.pop()

      stack.push(fn(a, b))
    } else {
      stack.push(Number(n))
    }
  }

  return stack[0]
}