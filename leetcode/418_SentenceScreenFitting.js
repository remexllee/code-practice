var wordsTyping = function (sentence, rows, cols) {
  const nextIndex = Array(sentence.length).fill(0)
  const times = Array(sentence.length).fill(0)
  for (let i = 0; i < sentence.length; i++) {
    let currLen = 0
    let curr = i
    let time = 0

    while (currLen + sentence[curr].length <= cols) {
      currLen += sentence[curr].length + 1
      curr++
      if (curr === sentence.length) {
        curr = 0
        time++
      }
    }
    nextIndex[i] = curr
    times[i] = time
  }
  let ans = 0
  let curr = 0
  for (let i = 0; i < rows; i++) {
    ans += times[curr]
    curr = nextIndex[curr]
  }
  return ans
}