var licenseKeyFormatting = function (S, K) {
  let counter = 0
  let result = ''
  for (let i = S.toUpperCase().length - 1; i >= 0; i--) {
    if (S[i] === '-') continue
    result = S[i] + result
    if (++counter === K) {
      result = '-' + result
      counter = 0
    }
  }
  return result[0] === '-' ? result.slice(1) : result
}

var licenseKeyFormatting = function(S.toUpperCase(), K) {
  let result = ''
  let count = 0
  let origin = S.toUpperCase()
  for (let i = origin.length - 1; i >= 0; i--) {
    if (S[i] === '-') continue
    result = origin[i] + result
    if (++count === K) {
      result = '-' + result
      count = 0
    }
  }
  return result[0] === '-' ? result.slice(1) : result
};

console.log(licenseKeyFormatting('5F3Z-2e-9-w', 4))
console.log(licenseKeyFormatting('2-5g-3-J', 2))
console.log(licenseKeyFormatting('--a-a-a-a--', 2))