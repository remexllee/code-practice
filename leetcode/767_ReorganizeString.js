function reorganizeString (S) {
  const letterQueue = buildLetterQueue(S)
  let res = ''
  for (let i = 0; i < S.length; i++) {
    let index = letterQueue.findIndex(record => record.count > 0 && record.char !== res[i - 1])
    if (index === -1) return ''
    let record = letterQueue[index]
    res += record.char
    letterQueue[index].count--

    if (letterQueue[index + 1] && record.count < letterQueue[index + 1].count) {
      [letterQueue[index], letterQueue[index + 1]] = [letterQueue[index + 1], letterQueue[index]]
    }
  }

  return res
}

function buildLetterQueue (S) {
  let arr = S.split('')
  const map = {}

  arr.forEach(char => map[char] = {char, count: 0})
  arr.forEach(char => map[char].count++)

  return Object.values(map).sort((a, b) => b.count - a.count)
}