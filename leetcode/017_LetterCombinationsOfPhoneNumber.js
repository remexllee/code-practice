const numToletters = {
  '0': ' ',
  '1': '',
  '2': 'abc',
  '3': 'def',
  '4': 'ghi',
  '5': 'jkl',
  '6': 'mno',
  '7': 'pqrs',
  '8': 'tuv',
  '9': 'wxyz'
}

const letterCombinations = (digits) => {
  let res = []
  if (digits.length === 0) {
    return res
  }

  function dfs (dights, idx, curr) {
    if (idx === dights.length) {
      res.push(curr)
      return
    }

    var letters = numToletters[digits[idx]]

    for (let i = 0; i < letters.length; i++) {
      dfs(dights, idx + 1, curr + letters[i])
    }
  }

  dfs(digits, 0, '')
  return res
}

console.log(letterCombinations('23'))

// https://leetcode.com/problems/letter-combinations-of-a-phone-number/discuss/126398/JavaScript-solution-with-Queue-(currently-beats-100)

var letterCombinations = function (digits) {
  if (digits === '') return []

  const lookup = {
    '2': ['a', 'b', 'c'],
    '3': ['d', 'e', 'f'],
    '4': ['g', 'h', 'i'],
    '5': ['j', 'k', 'l'],
    '6': ['m', 'n', 'o'],
    '7': ['p', 'q', 'r', 's'],
    '8': ['t', 'u', 'v'],
    '9': ['w', 'x', 'y', 'z']
  }

  const letterReducer = (acc, arr) => {
    const length = acc[0].length
    while (acc[0].length === length) {
      const s = acc.shift()
      arr.forEach(c => {
        acc.push(s + c)
      })
    }
    return acc
  }

  return digits.split('')
    .map(c => lookup[c])
    .reduce(letterReducer, [''])
}