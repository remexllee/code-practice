var MaxStack = function () {
  var stack
  this.stack = []
}

MaxStack.prototype.push = function (x) {
  this.stack.push(x)
}

MaxStack.prototype.pop = function () {
  return this.stack.pop()
}

MaxStack.prototype.top = function () {
  return this.stack[this.stack.length - 1]
}

MaxStack.prototype.peekMax = function () {
  return Math.max(...this.stack)
}

MaxStack.prototype.popMax = function () {
  var max = Number.MIN_SAFE_INTEGER
  var k = 0
  if (this.stack.length == 1) {
    return this.stack.pop()

  }
  for (var i = 0; i < this.stack.length; i++) {
    if (max <= this.stack[i]) {
      max = this.stack[i]
      k = i
    }
  }
  this.stack.splice(k, 1)
  return max
}