/**
 * @param {number[][]} matrix
 * @return {boolean}
 */
var isToeplitzMatrix = function (matrix) {
  for (let x = 0; x < matrix.length - 1; x++) {
    for (let y = 0; y < matrix[x].length - 1; y++) {

      if (matrix[x][y] != matrix[x + 1][y + 1])
        return false
    }
  }
  return true
}

const data = [
  [1, 2, 3, 4],
  [5, 1, 2, 3],
  [9, 5, 1, 2]
]
console.log(isToeplitzMatrix())