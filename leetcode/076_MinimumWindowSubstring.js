var minWindow = function (s, p) {
  let result = ''
  const map = {}
  let left = 0
  let right = 0
  let count = p.length
  let minLen = Number.MAX_SAFE_INTEGER
  // init map
  for (let c of p) {
    map[c] = map[c] ? map[c] + 1 : 1
  }

  while (right < s.length || count === 0) {
    if (count === 0) {
      if (right - left + 1 < minLen) {
        minLen = right - left + 1
        result = s.substring(left, right)
      }
      if (map[s[left++]]++ >= 0) count++
    } else {
      if (map[s[right++]]-- >= 1) count--
    }
  }
  return result
}

const s = 'ADOBECODEBANC'
const p = 'ABC'

console.log(minWindow(s, p))