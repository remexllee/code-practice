var printTree = function (root) {
  let size = [0, 0]
  sizeTree(root, size, 1)
  let result = []
  for (let i = 0; i < size[0]; i++) {
    result.push(new Array(size[1]).fill(''))
  }
  placeValues(root, result, 0, 0, size[1])
  return result
}

var sizeTree = function (root, size, height) {
  if (root == null) return
  if (height > size[0]) {
    size[0] = height
    size[1] = 2 * size[1] + 1
  }
  sizeTree(root.left, size, height + 1)
  sizeTree(root.right, size, height + 1)
}
var placeValues = function (node, result, row, left, right) {
  if (node == null) return
  let mid = Math.floor((right - left) / 2) + left
  result[row][mid] = node.val.toString()
  placeValues(node.left, result, row + 1, left, mid - 1)
  placeValues(node.right, result, row + 1, mid + 1, right)

}