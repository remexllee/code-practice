// https://leetcode.com/problems/merge-two-binary-trees/discuss/137279/JavaScript-recursive-and-iterative-solutions
// https://leetcode.com/problems/merge-two-binary-trees/discuss/104299/Java-Solution-6-lines-Tree-Traversal
// https://leetcode.com/problems/merge-two-binary-trees/discuss/104299/Java-Solution-6-lines-Tree-Traversal
// javascript solution not work for online testcase

const mergeTrees = (t1, t2) => {
  if (!t1) {
    return t2
  }
  if (!t2) {
    return t1
  }
  t1.val += t2.val
  t1.left = mergeTrees(t1.left, t2.left)
  t1.right = mergeTrees(t1.right, t2.right)
  return t1
}
