/**
 * @param {string} paragraph
 * @param {string[]} banned
 * @return {string}
 */
const mostCommonWord = (paragraph, banned) => {
  const bset = new Set(banned)
  const parr = paragraph
    .toLowerCase()
    .split(/\W+/)
    .filter(item => item)

  const countMap = new Map()
  let maxCount = 0
  let maxCountKey = null

  for (const word of parr) {
    if (!bset.has(word)) {
      let count = ~~(countMap.get(word)) + 1
      countMap.set(word, count)
      if (count > maxCount) {
        maxCount = count
        maxCountKey = word
      }
    }
  }
  return maxCountKey
}

const data = 'Bob hit a ball, the hit BALL flew far after it was hit.'
console.log(data.toLowerCase().split(/\W+/))
console.log(mostCommonWord(data, ['hit']))