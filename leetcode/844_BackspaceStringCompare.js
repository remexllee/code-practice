/**
 * @param {string} S
 * @param {string} T
 * @return {boolean}
 */
var backspaceCompare = function (S, T) {
  return reformatString(S) === reformatString(T)
  function reformatString (s) {
    var res = []
    for (var i = 0; i < s.length; i++) {
      if (s[i] != '#') {
        res.push(s[i])
      } else {
        res.pop()
      }
    }
    return res.join('')
  }
}

console.log(backspaceCompare('ab#c', 'ad#c'))
console.log(backspaceCompare('ab##', 'c#d#'))
console.log(backspaceCompare('a##c', '#a#c'))
console.log(backspaceCompare('a#c', 'b'))


