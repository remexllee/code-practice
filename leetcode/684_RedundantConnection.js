var findRedundantConnection = function (edges) {
  let parent = []
  let res = []

  for (let edge of edges) {
    let u = edge[0]
    let v = edge[1]
    let root1 = find(u, parent)
    let root2 = find(v, parent)

    if (root1 === root2) {
      return edge
    } else {
      parent[root1] = root2
    }
  }

  return res

  function find (idx, parent) {
    return parent[idx] === undefined ? idx : find(parent[idx], parent)
  }
}

console.log(findRedundantConnection([[1, 2], [1, 3], [2, 3]]))