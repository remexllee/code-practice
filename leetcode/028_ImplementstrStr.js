// const strStr = (haystack, needle) => {
//   for (let i = 0;; i++) {
//     for (let j = 0;; j++) {
//       if (j === needle.length) return i
//       if (i + j === haystack.length) return -1
//       if (needle[j] != haystack[i + j]) break
//     }
//   }
// }

// https://leetcode.com/problems/implement-strstr/discuss/123604/Simple-javascript-solution-(52-ms-faster-than-99.83-JS-solutions)

/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function (haystack, needle) {
  if (!needle.length) {
    return 0
  }

  for (let i = 0; i <= haystack.length - needle.length; i++) {
    if (haystack[i] === needle[0]) {
      if (haystack.substring(i, i + needle.length) === needle) {
        return i
      }
    }
  }
  return -1
}