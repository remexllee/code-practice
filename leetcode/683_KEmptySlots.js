// https://leetcode.com/problems/k-empty-slots/discuss/128033/JavaScript-Solution-Beats-100
// Very slow, but easy undertand check whether all number is 0
const search = (status, start, end) => {
  // console.log('search start: ', start)
  // console.log('search end: ', end)
  return status
    .slice(start, end + 1)
    .every(num => !num)
}

var kEmptySlots = function (flowers, k) {
  let status = new Array(flowers.length).fill(0)
  for (let i = 0; i < flowers.length; i++) {
    let slotIndex = flowers[i] - 1
    status[slotIndex] = 1
    let statusStartIndex = slotIndex - k
    let statusEndIndex = slotIndex + k

    console.log('slotIndex: ', slotIndex)
    console.log('statusStartIndex: ', statusStartIndex)
    console.log('statusEndIndex: ', statusEndIndex)
    console.log('status[statusStartIndex - 1]:', status[statusStartIndex - 1])
    console.log('status[statusEndIndex + 1]:', status[statusEndIndex + 1])
    console.log('--------------------')

    if (statusStartIndex >= 0 && status[statusStartIndex - 1] === 1) {
      console.log('OK 1')
      if (search(status, statusStartIndex, slotIndex - 1)) {
        console.log('Here')
        return i + 1
      }
    }

    if (statusEndIndex < status.length && status[statusEndIndex + 1] === 1) {
      console.log('OK 2')
      if (search(status, slotIndex + 1, statusEndIndex)) {
        console.log('There')
        return i + 1
      }
    }
  }
  return -1
}

// console.log(kEmptySlots([1, 3, 2], 1))
console.log(kEmptySlots([1, 2, 5, 3, 4], 1))
