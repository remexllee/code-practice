var longestValidParentheses = function (s) {
  var max = 0
  var st = [-1]
  //  pushing ( idx into stack
  //  when there's a ), 1. pop a ( when exists, check the idx cur - pre of (
  //  2. clear stack and push ) idx into stack
  for (var j = 0; j < s.length; ++j) {
    var c = s.charAt(j)
    if (c == '(') {
      st.push(j)
    } else {
      var top = st.pop()
      if (st.length > 0) {
        max = Math.max(max, j - st[st.length - 1])
      } else {
        st = [j]
      }
    }
  }

  return max
}