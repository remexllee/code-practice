var findCheapestPrice = function (n, flights, src, dst, K) {
  let prev = []
  let current = []
  for (let i = 0; i < n; i++) {
    if (i === src) {
      prev[i] = 0
      current[i] = 0
    } else {
      prev[i] = Number.POSITIVE_INFINITY
      current[i] = Number.POSITIVE_INFINITY
    }
  }

  for (let i = 0; i <= K; i++) {
    current = prev.map(elem => elem)
    for (let j = 0; j < flights.length; j++) {
      const source = flights[j][0]
      const dest = flights[j][1]
      const distance = flights[j][2]

      current[dest] = Math.min(current[dest], prev[source] + distance)
    }
    prev = current
  }

  return current[dst] === Number.POSITIVE_INFINITY ? -1 : current[dst]
}

