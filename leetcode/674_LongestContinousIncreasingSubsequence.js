var findLengthOfLCIS = function (nums) {
  if (nums.length === 0) return 0 // edge case
  let count = 1 // start from 1
  let max = 1 // start from 1

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] < nums[i + 1]) {
      count++
      max = Math.max(max, count)
    } else {
      count = 1
    }
  }
  return max
}