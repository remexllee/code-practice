var findAnagrams = function (s, p) {
  const result = []
  const map = {}
  let left = 0
  let right = 0
  let count = p.length
  // init map
  for (let c of p) {
    map[c] = map[c] ? map[c] + 1 : 1
  }

  while (right < s.length) {
    if (map[s[right++]]-- >= 1) {
      count--
    } // 1. 扩展窗口，窗口中包含一个T中子元素，count--
    if (count === 0) {
      result.push(left)
    }  // 2. 通过count或其他限定值，得到一个可能值
    if (right - left === p.length && map[s[left++]]++ >= 0) {
      count++
    } // 3. 只要窗口中有可能解，那么缩小窗口直到不包含可能解
  }
  return result
}

console.log(findAnagrams('cbaebabacd', 'abc'))
//console.log(findAnagrams('baa', 'aa')) // 1