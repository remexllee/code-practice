var search = function (nums, target) {
  if (!nums.length) return -1

  let lo = 0
  let hi = nums.length - 1

  while (lo < hi) {
    let mid = ~~((hi - lo) / 2) + lo
    if (nums[mid] === target) return mid
    if (nums[hi] < nums[mid]) { // inversion
      if (target < nums[mid] && target > nums[hi]) {
        hi = mid - 1
      } else {
        lo = mid + 1
      }
    } else {
      if (target < nums[mid] || target > nums[hi]) {
        hi = mid - 1
      } else {
        lo = mid + 1
      }
    }

  }
  return nums[lo] === target ? lo : -1
}