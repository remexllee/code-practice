var isOneEditDistance = function (s, t) {
  let issues = 0
  if (s.length === t.length) {
    for (let i = 0; i < s.length; i++) {
      if (s[i] !== t[i]) {
        issues++
      }
    }
  } else {
    let longer = s.length > t.length ? s : t
    let shorter = s.length > t.length ? t : s
    let longerIndex = 0
    let shorterIndex = 0
    while (longerIndex < longer.length) {
      if (longer[longerIndex] === shorter[shorterIndex]) {
        longerIndex++
        shorterIndex++
      } else {
        issues++
        longerIndex++
      }
    }
  }
  return issues === 1
}