// https://leetcode.com/problems/merge-sorted-array/discuss/170363/Javascript-Solution.-Beats-100

/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  let i = m - 1, j = n - 1

  while (i >= 0 && j >= 0) {
    if (nums1[i] > nums2[j]) {
      nums1[i + j + 1] = nums1[i]
      i--
    } else {
      nums1[i + j + 1] = nums2[j]
      j--
    }
  }
  while (j >= 0) {
    nums1[j] = nums2[j]
    j--
  }
}

let nums1 = [1, 2, 3, 0, 0, 0]
let nums2 = [2, 5, 6]

// TestCase 1
// Steps:
//   [ 1, 2, 3, 0, 0, 6 ]
//   [ 1, 2, 3, 0, 5, 6 ]
//   [ 1, 2, 3, 3, 5, 6 ]
//   [ 1, 2, 2, 3, 5, 6 ]
//

merge(nums1, 3, nums2, 3)

// Testcase 2
let nums3 = [4, 5, 6, 0, 0, 0]
let nums4 = [1, 2, 3]

// Steps:
// 如果 nums1 insert结束，nums2还没有结束，说明nums2剩下的都是和index相同的数字
//   [ 4, 5, 6, 0, 0, 6 ]
//   [ 4, 5, 6, 0, 5, 6 ]
//   [ 4, 5, 6, 4, 5, 6 ]
//

merge(nums3, 3, nums4, 3)
