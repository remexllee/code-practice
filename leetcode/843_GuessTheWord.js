var findSecretWord = function (wordlist, master) {
  let curScore = 0
  let n = 10
  while (n) {
    let randomIndex = Math.floor(Math.random() * (wordlist.length - 1))
    let guess = wordlist[randomIndex]
    curScore = master.guess(guess)
    let newWordlist = []
    for (let i = 0; i < wordlist.length; i++) {
      if (i != randomIndex && curScore == myMaster(wordlist[i], guess)) {
        newWordlist.push(wordlist[i])
      }
    }
    wordlist = newWordlist
    n--
  }
}

const myMaster = (hypothesis, word) => {
  let score = 0
  for (let i = 0; i < hypothesis.length; i++) {
    if (hypothesis[i] == word[i]) {
      score++
    }
  }
  return score
}