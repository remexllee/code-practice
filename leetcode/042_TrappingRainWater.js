// https://leetcode.com/problems/trapping-rain-water/discuss/116478/JavaScript-O(n)-time-O(1)-space-easy-to-read
//
// var trap = function (height) {
//   const peakIndex = getPeakIndex(height)
//   const firstHalf = getTrap(height, 0, peakIndex)
//   console.log(firstHalf)
//   console.log(height)
//   reverse(height, peakIndex, height.length - 1)
//   console.log(height)
//   const secondHalf = getTrap(height, peakIndex, height.length - 1)
//   console.log(secondHalf)
//   return firstHalf + secondHalf
// }
//
// function getPeakIndex (arr) {
//   const peak = arr.reduce((max, val) => Math.max(max, val), 0)
//   return arr.indexOf(peak)
// }
//
// function getTrap (arr, start, end) {
//   let trap = 0
//   let max = arr[start]
//   for (let i = start; i <= end; i++) {
//     if (arr[i] < max)
//       trap += max - arr[i]
//     else
//       max = arr[i]
//   }
//   return trap
// }
//
// function reverse (arr, start, end) {
//   const len = end - start + 1
//   for (let i = 0; i < len / 2; i++) {
//     let tmp = arr[start + i]
//     arr[start + i] = arr[end - i]
//     arr[end - i] = tmp
//   }
// }

// https://segmentfault.com/a/1190000012739798 很详细的讲解 Java

/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (elevs) {
  var trapped = 0
  var left = 0
  var leftHeight = 0
  var right = elevs.length - 1
  var rightHeight = 0

  while (left <= right) {
    if (leftHeight <= rightHeight) {
      leftHeight = Math.max(leftHeight, elevs[left])
      trapped += leftHeight - elevs[left]
      left++
    } else {
      rightHeight = Math.max(rightHeight, elevs[right])
      trapped += rightHeight - elevs[right]
      right--
    }
  }

  return trapped
}

const input = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
// https://leetcode.com/problems/trapping-rain-water/discuss/183237/Another-JavaScript-solution-(99)-w-bonus-illustration
console.log(trap(input))