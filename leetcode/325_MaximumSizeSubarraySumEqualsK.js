var maxSubArrayLen = function (nums, k) {
  const presums = new Map([[0, 0]])
  let res = 0
  for (let sum = nums[0], i = 0; i < nums.length; sum += nums[++i]) {
    if (presums.has(sum - k)) {
      res = Math.max(res, i + 1 - presums.get(sum - k))
    }
    if (!presums.has(sum)) presums.set(sum, i + 1)
  }
  return res
}