// https://leetcode.com/problems/fraction-to-recurring-decimal/discuss/134656/Clean-JavaScript-solution

var fractionToDecimal = function (numerator, denominator) {
  if (numerator === 0) return '0'
  let res = ''
  if (Math.sign(numerator) !== Math.sign(denominator)) res += '-'
  let n = Math.abs(numerator)
  let d = Math.abs(denominator)
  res += Math.floor(n / d)
  n %= d//remainded
  if (n === 0) return res
  res += '.'
  let map = {}
  while (n !== 0) {
    map[n] = res.length
    n *= 10
    res += Math.floor(n / d)//result add
    n %= d//remained
    const i = map[n]//get the current number from map
    if (i !== undefined) return `${res.substr(0, i)}(${res.substr(i)})`//repeat in ()
  }
  return res//end with zero
}