// https://www.cnblogs.com/yuzhangcmu/p/4221998.html
var nextPermutation = function (nums) {
  if (!nums || nums.length === 0) {
    return
  }

  let len = nums.length

  // Find the index which drop
  let dropIndex = -1
  for (let i = len - 1; i >= 0; i--) {
    if (i !== len - 1 && nums[i] < nums[i + 1]) {
      dropIndex = i
      break
    }
  }

  // replace the drop index
  if (dropIndex !== -1) {
    for (let i = len - 1; i >= 0; i--) {
      if (nums[i] > nums[dropIndex]) {
        swap(nums, dropIndex, i)
        break
      }
    }
  }

  // reverse the link
  let l = dropIndex + 1
  let r = len - 1
  while (l < r) {
    swap(nums, l++, r--)
  }

  console.log(nums)

  function swap (nums, l, r) {
    let temp = nums[l]
    nums[l] = nums[r]
    nums[r] = temp
  }
}

// const nextPermutation = nums => {
//   const n = nums.length
//
//   // Step 1. scan from right and find the first digit that is less than its right
//   for (let i = n - 2; i >= 0; i--) {
//     if (nums[i] < nums[i + 1]) {
//       // Step 2. scan from right and find the digit that is larger than nums[i]
//       for (let j = n - 1; j > i; j--) {
//         if (nums[j] > nums[i]) {
//           // Step 3. swap nums[i] and nums[j], reverse from i+1
//           swap(nums, i, j)
//           reverse(nums, i + 1, n - 1)
//           return
//         }
//       }
//     }
//   }
//
//   nums.reverse()
//   console.log(nums)
// }
//
// const swap = (nums, i, j) => ([nums[i], nums[j]] = [nums[j], nums[i]])
//
// const reverse = (nums, start, end) => {
//   while (start < end) {
//     swap(nums, start++, end--)
//   }
// }

console.log(nextPermutation([1, 2, 3]))
console.log(nextPermutation([3, 2, 1, 0]))
console.log(nextPermutation([1, 3, 6]))
console.log(nextPermutation([5, 6, 1]))
console.log(nextPermutation([1, 2, 4, 3, 2, 1]))

// SOLUTION 1:
// 1.  From the tail to find the first digital which drop
// example: 12 4321
//
// 首先我们找到从尾部到头部第一个下降的digit. 在例子中是：2
// 2. 把从右到左第一个比dropindex大的元素换过来。
// 3. 把dropindex右边的的序列反序
// 4. 如果1步找不到这个index ，则不需要执行第二步。