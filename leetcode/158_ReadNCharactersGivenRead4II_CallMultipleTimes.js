var solution = function (read4) {
  var internalBuf = []

  /**
   * @param {character[]} buf Destination buffer
   * @param {number} n Maximum number of characters to read
   * @return {number} The number of characters read
   */
  return function (buf, n) {
    let readChars = 0
    while (n > 0) {
      if (internalBuf.length === 0) {
        if (read4(internalBuf) === 0) {
          return readChars
        }
      }

      buf.push(internalBuf.shift())
      readChars++
      n--
    }
    return readChars
  }
}

// var solution = function (read4) {
//   let temp = []
//   return function (buf, n) {
//     while (buf.length < n) {
//       // if no bytes unused from last call, read 4 bytes
//       if (temp.length === 0) { read4(temp) }
//       if (temp.length === 0) { break } // end of file
//       while (buf.length < n && temp.length > 0) {
//         buf.push(temp.shift()) // copy from temp to buf
//       }
//     }
//     return buf.length
//   }
// }