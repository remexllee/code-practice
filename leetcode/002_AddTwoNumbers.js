/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val
 *     this.next = null
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

function ListNode(val) {
  this.val = val
  this.next = null
}

const addTwoNumbers = (l1, l2) => {
  if (l1 === null || l2 === null) {
    return l1 || l2
  }

  let result = new ListNode(0)
  let current = result
  let p = l1
  let q = l2
  let carry = 0

  while (p || q) {
    let pVal
    let qVal
    if (q) {
      qVal = q.val
      q = q.next
    } else {
      qVal = 0
    }

    if (p) {
      pVal = p.val
      p = p.next
    } else {
      pVal = 0
    }

    var val = qVal + pVal + carry

    if (val > 9) {
      carry = 1
      val %= 10
    } else {
      carry = 0
    }

    current.next = new ListNode(val)
    current = current.next
  }

  if (carry !== 0) {
    current.next = new ListNode(1)
  }
  return result.next
}

const l1 = new ListNode(2)
l1.next = new ListNode(4)
l1.next.next = new ListNode(3)

const l2 = new ListNode(5)
l2.next = new ListNode(6)
l2.next.next = new ListNode(4)

//  console.log(l1)
//  console.log(l2)
console.log(addTwoNumbers(l1, l2))
