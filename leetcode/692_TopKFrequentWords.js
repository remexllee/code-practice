var topKFrequent = function (words, k) {
  const wordCounts = words
    .reduce((a, b) => {
      a[b] ? a[b]++ : a[b] = 1
      return a
    }, {})
  return Object
    .keys(wordCounts)
    .sort((a, b) => {
      if (wordCounts[a] > wordCounts[b]) {
        return -1
      }
      if (wordCounts[b] > wordCounts[a]) {
        return 1
      } else {
        return a.localeCompare(b)
      }
    }).slice(0, k)
}