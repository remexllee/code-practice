// https://leetcode.com/problems/largest-number/discuss/53227/faster-javascript-solution

function sortfunc (a, b) {
  var as = a.toString()
  var bs = b.toString()
  var ab = as + bs
  var ba = bs + as
  return ba - ab
}

var largestNumber = function (nums) {
  var result = nums.sort(sortfunc).join('')
  return parseInt(result) === 0 ? '0' : result
}