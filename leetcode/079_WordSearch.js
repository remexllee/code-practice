// var exist = function (board, word) {
//   for (let i = 0; i < board.length; i++) {
//     for (let j = 0; j < board[i].length; j++) {
//       const result = findWord(board, 0, word, j, i)
//       if (result) return true
//     }
//   }
//   return false
// }
//
// function findWord (board, found, word, col, row,) {
//   if (found === word.length) return true
//   if (col < 0 || row < 0) return false
//   if (col >= board[0].length || row >= board.length) return false
//   if (!board[row][col]) return false
//
//   const temp = board[row][col]
//   board[row][col] = false
//
//   if (temp === word[found]) {
//     const right = findWord(board, found + 1, word, col + 1, row)
//     const down = findWord(board, found + 1, word, col, row + 1)
//     const left = findWord(board, found + 1, word, col - 1, row)
//     const up = findWord(board, found + 1, word, col, row - 1)
//     if (right || down || left || up) {
//       return true
//     }
//   }
//
//   board[row][col] = temp
//   return false
// }

var exist = function (board, word) {
  const wordLength = word.length
  const wordArray = word.split('')

  var verify = function (row, col, matrix, path) {
    if (
      row < 0 ||
      col < 0 ||
      row >= matrix.length ||
      col >= matrix[0].length ||
      matrix[row][col] != wordArray[path] ||
      path > wordLength
    )
      return false
    // Up to this point, we found the char we were looking for
    path++
    matrix[row][col] = '#'

    //If we find the word
    if (path === wordLength) return true

    //Up
    if (verify(row - 1, col, matrix, path)) return true
    //Right
    if (verify(row, col + 1, matrix, path)) return true
    //Down
    if (verify(row + 1, col, matrix, path)) return true
    //Left
    if (verify(row, col - 1, matrix, path)) return true
    // Backtrack
    matrix[row][col] = wordArray[--path]
    return false
  }

  for (let i = 0; i < board.length; i++) {
    for (let j = 0; j < board[i].length; j++) {
      if (verify(i, j, board, 0))
        return true
    }
  }

  return false
}

const board = [['A', 'B', 'C', 'C'], ['S', 'C', 'C', 'S'], ['A', 'D', 'E', 'E']]
const word = 'ABCCED'

console.log(exist(board, word))