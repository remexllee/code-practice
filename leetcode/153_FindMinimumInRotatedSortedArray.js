/**
 * @param {number[]} nums
 * @return {number}
 */
// template 2
// https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/discuss/48484/A-concise-solution-with-proof-in-the-comment
var findMin = function (nums) {
  let left = 0
  let right = nums.length - 1

  while (left < right) {
    let mid = ~~(left + (right - left) / 2)
    // nums right is the target number
    if (nums[mid] < nums[right]) {
      // the mininum is in the left part
      right = mid
    }
    else if (nums[mid] > nums[right]) {
      // the mininum is in the right part
      left = mid + 1
    }
  }

  return nums[left]
}

//console.log(findMin([3, 4, 5, 1, 2]))
console.log(findMin([4, 5, 6, 7, 0, 1, 2]))

