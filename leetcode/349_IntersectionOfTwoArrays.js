var intersection = function (nums1, nums2) {
  let set = new Set(nums2)
  return [...new Set(nums1.filter(num => set.has(num)))]
}