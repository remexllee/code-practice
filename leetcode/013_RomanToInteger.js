const romanToInt = (s) => {
  let result = 0
  for (let i = 0; i < s.length; i++) {
    if (i > 0 && (c2n(s[i]) > c2n(s[i - 1]))) {
      result -= 2 * c2n(s[i - 1])
    }
    result += c2n(s[i])
  }
  return result
}

const c2n = (c) => {
  switch (c) {
    case 'I':
      return 1
    case 'V':
      return 5
    case 'X':
      return 10
    case 'L':
      return 50
    case 'C':
      return 100
    case 'D':
      return 500
    case 'M':
      return 1000
    default:
      return 0
  }
}

console.log(romanToInt('CM'))
// console.log(romanToInt('IX'))
// console.log(romanToInt('MCMXCIV'))


//Interesting solution

var romanToInt1 = function(s) {
  let res = 0;
  for (let i=0; i<s.length; i++) {
    switch(s.substring(i, i+2)) {
      case "CM": res += 900; i++; break;
      case "CD": res += 400; i++; break;
      case "XC": res += 90; i++; break;
      case "XL": res += 40; i++; break;
      case "IX": res += 9; i++; break;
      case "IV": res += 4; i++; break;
      default:
        switch(s.substring(i, i+1)) {
          case "M": res += 1000; break;
          case "D": res += 500; break;
          case "C": res += 100; break;
          case "L": res += 50; break;
          case "X": res += 10; break;
          case "V": res += 5; break;
          case "I": res += 1;
        }
    }
  }
  return res;
};

const RToInt = (s) => {
  const map = new Map()
  map.set('I', 1)
  map.set('V', 5)
  map.set('X', 10)
  map.set('L', 50)
  map.set('C', 100)
  map.set('D', 500)
  map.set('M', 1000)
  map.set('IV', 4)
  map.set('IX', 9)
  map.set('XL', 40)
  map.set('XC', 90)
  map.set('CD', 400)
  map.set('CM', 900)

  let result = 0
  for (let i = 0; i < s.length; i++) {
    if (map.has(s.substring(i, i + 2))) {
      result += map.get(s.substring(i, i + 2))
      i++
    } else if (map.has(s[i])) {
      result += map.get(s[i])
    }
  }
  return result
}

// 1. substring use (i, i + 2)
// 2. dual symbol need to index + 1 after find out

console.log(RToInt('IV'))
console.log(RToInt('III'))