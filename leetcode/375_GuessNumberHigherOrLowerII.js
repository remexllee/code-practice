var getMoneyAmount = function (n) {
  let dp = []
  for (let i = 0; i < n; i++)
    dp.push([0])
  return getMinCost(0, n - 1, dp)
}

let getMinCost = (start, end, dp) => {
  if (start >= end)
    return 0
  if (dp[start][end])
    return dp[start][end]

  let minCost = Number.MAX_SAFE_INTEGER
  for (let i = start; i < end; i++) {
    let left = getMinCost(start, i - 1, dp),
      right = getMinCost(i + 1, end, dp)
    minCost = Math.min(minCost, Math.max(left, right) + (i + 1))
  }
  dp[start][end] = minCost
  return minCost
}