var checkValidString = function (s) {
  let minPossibleOpenParenthesis = 0
  let maxPossibleOpenParenthesis = 0

  for (let i of s) {
    i === '(' ? minPossibleOpenParenthesis++ : minPossibleOpenParenthesis--
    i === ')' ? maxPossibleOpenParenthesis-- : maxPossibleOpenParenthesis++

    if (maxPossibleOpenParenthesis < 0) return false
    minPossibleOpenParenthesis = Math.max(minPossibleOpenParenthesis, 0)
  }
  return minPossibleOpenParenthesis === 0
}

// min_op, max_op to track the min/max possible open parenthesis
// max_op should be always >= 0
// if min_op < 0  reset it to 0
// if max_op < 0 return false
// finally check min_op === 0

// console.log(checkValidString('()'))
console.log(checkValidString('(*)'))
// console.log(checkValidString('(*))'))

//leetcode.com/problems/valid-parenthesis-string/discuss/107611/Very-concise-C%2B%2B-solution-with-explaination.-No-DP