/**
 * Initialize your data structure here.
 */
var MagicDictionary = function () {
  this.root = new Trie()
}

/**
 * Build a dictionary through a list of words
 * @param {string[]} dict
 * @return {void}
 */
MagicDictionary.prototype.buildDict = function (dict) {
  for (let i = 0, n = dict.length; i < n; i++) {
    this.root.build(dict[i])
  }
  console.log(this.root)
}

/**
 * Returns if there is any word in the trie that equals to the given word after modifying exactly one character
 * @param {string} word
 * @return {boolean}
 */

MagicDictionary.prototype.search = function (word) {
  let root = this.root
  for (let i = 0, n = word.length; i < n; i++) {
    const ch = word.charAt(i)
    if (root === undefined) {
      return false
    }
    const letters = root.children.keys()
    for (let letter of letters) {
      if (ch !== letter && root.find(word.substring(i + 1), root.children.get(letter))) {
        return true
      }
    }
    root = root.children.get(ch)
  }
  return false
}

var Trie = function () {
  this.children = new Map()
  this.isWord = false
}

Trie.prototype.build = function (word) {
  let root = this
  for (let ch of word) {
    if (!root.children.has(ch)) {
      root.children.set(ch, new Trie())
    }
    root = root.children.get(ch)
  }
  root.isWord = true
}

Trie.prototype.find = function (word, root) {
  for (let ch of word) {
    if (root === undefined || !root.children.has(ch)) {
      return false
    }
    root = root.children.get(ch)
  }
  return root !== undefined && root.isWord
}
