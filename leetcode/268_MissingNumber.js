// https://leetcode.com/problems/missing-number/discuss/139505/JavaScript...-my-first-100-fastest
/**
 * @param {number[]} nums
 * @return {number}
 */
var missingNumber = function (nums) {
//  n(n+1)/2 is the formula for totaling numbers in a sequence. You can ignore 0 since its an identity number
  const shouldBeTotal = (nums.length * (nums.length + 1)) / 2
//  total up the nums Array using reduce
  const total = nums.reduce((a, b) => a + b)
// get whats missing by subtracting the two. 'total' if missing something will always be smaller
  return shouldBeTotal - total
}