const reverse = (x) => {
  let isNeg = x < 0
  let result = 0

  x = Math.abs(x)

  while (x) {
    let lastDigit = x % 10
    result *= 10
    result += lastDigit
    x = parseInt(x / 10)
  }

  result = isNeg ? -result : result

  // check overflow
  if (result > Math.pow(2, 31) - 1 || result < -Math.pow(2, 31)) {
    return 0
  }

  return result
}

const num = 123
console.log(reverse(num))
