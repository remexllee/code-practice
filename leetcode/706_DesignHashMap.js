var MyHashMap = function () {
  this.mySet = Array.from({length: 1000}, x => [])
  this.mapFn = function (key) {
    return key % 1000
  }
}

MyHashMap.prototype.put = function (key, value) {
  let arr = this.mySet[this.mapFn(key)]
  let index = arr.indexOf(key)
  if (index === -1) arr.push(key, '' + value)
  else arr.splice(index + 1, 1, '' + value)
}

MyHashMap.prototype.get = function (key) {
  let arr = this.mySet[this.mapFn(key)]
  let index = arr.indexOf(key)
  if (index !== -1) return +arr[index + 1]
  return -1
}

MyHashMap.prototype.remove = function (key) {
  let arr = this.mySet[this.mapFn(key)]
  let index = arr.indexOf(key)
  if (index !== -1) arr.splice(index, 2)
}