const helper = function (num, target, str, startIndex, lastFactor, sum, result) {
  if (startIndex === num.length && sum === target) {
    result.push(str)
    return
  }

  for (let i = startIndex; i < num.length; i++) {
    if (i > startIndex && num[startIndex] === '0') {
      continue
    }

    let currStr = num.slice(startIndex, i + 1)
    let currNum = Number(currStr)

    if (startIndex === 0) {
      helper(num, target, currStr, i + 1, currNum, currNum, result)
    } else {
      helper(num, target, str + '*' + currStr, i + 1, lastFactor * currNum, sum - lastFactor + lastFactor * currNum, result)
      helper(num, target, str + '+' + currStr, i + 1, currNum, sum + currNum, result)
      helper(num, target, str + '-' + currStr, i + 1, -currNum, sum - currNum, result)
    }
  }
}
var addOperators = function (num, target) {
  const result = []
  helper(num, target, '', 0, 0, 0, result)
  return result
}