var solution = function (isBadVersion) {
  /**
   * @param {integer} n Total versions
   * @return {integer} The first bad version
   */
  return function (n) {
    let left = 0
    let right = n

    while (left < right) {
      let mid = left + ~~((right - left) / 2)
      if (isBadVersion(mid)) {
        right = mid
      } else {
        left = mid + 1
      }
    }
    return left
  }
}

// console.log(binarySearch([1, 3, 5, 6, 7, 10], 5))
// console.log(binarySearch([1, 3, 5, 6, 7, 10], 8))
console.log(binarySearch([1, 3, 5, 6, 7, 10], 10))