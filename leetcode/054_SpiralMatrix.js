// const spiralOrder = (matrix) => {
//   let result = []
//   if (matrix === null || matrix.length === 0 || matrix[0].length === 0) {
//     return result
//   }
//
//   let rows = matrix.length
//   let cols = matrix[0].length
//
//   let x = 0
//   let y = 0
//
//   while (rows > 0 && cols > 0) {
//     if (rows === 1) {
//       for (let i = 0; i < cols; i++) {
//         result.push(matrix[x][y++])
//       }
//       break
//     } else if (cols === 1) {
//       for (i = 0; i < rows; i++) {
//         result.push(matrix[x++][y])
//       }
//       break
//     }
//
//     for (i = 0; i < cols - 1; i++) {
//       result.push(matrix[x][y++])
//     }
//
//     for (i = 0; i < rows - 1; i++) {
//       result.push(matrix[x++][y])
//     }
//
//     for (i = 0; i < cols - 1; i++) {
//       result.push(matrix[x][y--])
//     }
//
//     for (i = 0; i < rows - 1; i++) {
//       result.push(matrix[x--][y])
//     }
//
//     x++
//     y++
//     cols -= 2
//     rows -= 2
//   }
//
//   return result
// }

// https://leetcode.com/problems/spiral-matrix/discuss/20571/1-liner-in-Python-+-Ruby

transpose = m => m[0].map((x,i) => m.map(x => x[i]))

var spiralOrder = function(matrix) {
  return matrix[0] ? matrix.shift() + spiral_order(transpose(matrix).reverse) : []
};

