// https://leetcode.com/problems/power-of-three/discuss/77937/Javascript-one-liner-without-any-hard-coded-numbers-and-is-generalizable-beats-89.39
var isPowerOfThree = function (n) {
  return n.toString(3).split('').reduce((prev, curr) => parseInt(prev) + parseInt(curr)) == 1
}
