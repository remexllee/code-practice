var toGoatLatin = function (S) {
  // the second map is to append the extra "A's"
  // at the end of each word dependent on the index
  // that we are on
  return S.split(' ')
    .map(word => goatLatinize(word))
    .map((word, idx) => word + 'a'.repeat(idx + 1))
    .join(' ')
}

function goatLatinize (str) {
  // if str starts with vowel, just append 'ma' at the
  // end of it else, pop off the consonant, append it
  // at the end, and ad ma

  let vowels = 'aeiou'
  if (vowels.includes(str[0].toLowerCase())) {
    return str + 'ma'
  } else {
    return str.slice(1) + str[0] + 'ma'
  }
}