const TreeTool = require('../../Utils/TreeTool')
const root = TreeTool.generateBSTByHardCode()

var minDepth = function (root) {
  if (!root) return 0
  let depth = 0

  if (root.left && root.right) {
    let left = minDepth(root.left)
    let right = minDepth(root.right)
    depth = Math.min(left, right)
  } else if (root.left) {
    depth = minDepth(root.left)
  } else if (root.right) {
    depth = minDepth(root.right)
  }

  return depth + 1
}

console.log(minDepth(root))