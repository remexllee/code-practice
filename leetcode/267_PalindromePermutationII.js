function helper (map, res, len, s, mid) {
  if (s.length === len) {
    const reverse = s.split('').reverse().join('')
    return res.push(s + mid + reverse)
  }
  Object.keys(map).forEach(e => {
    if (map[e] > 0) {
      map[e]--
      helper(map, res, len, s + e, mid)
      map[e]++
    }
  })
}

var generatePalindromes = function (s) {
  const res = []
  if (!s) {
    return res
  }
  let oddNum = 0
  const map = s.split('').reduce((obj, e) => {
    obj[e] = obj[e] ? obj[e] + 1 : 1
    oddNum = obj[e] % 2 === 0 ? oddNum - 1 : oddNum + 1
    return obj
  }, {})
  if (oddNum > 1) {
    return res
  }
  let mid = ''
  const len = Object.keys(map).reduce((curr, key) => {
    if (map[key] > 0) {
      if (map[key] % 2 !== 0) {
        mid = key
        map[key]--
      }
      map[key] = Math.floor(map[key] / 2)
      curr += map[key]
    }
    return curr
  }, 0)
  helper(map, res, len, '', mid)
  return res
}