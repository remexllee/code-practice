
var levelOrder = function (root) {
  if (!root) return []

  const queue = [root]
  const result = []
  while (queue.length) {
    let levelResult = []

    // key: loop all item in current leve queue.
    let size = queue.length
    for (let i = 0; i < size; i++) {
      let currentNode = queue.shift()
      levelResult.push(currentNode.val)

      if (currentNode.left) queue.push(currentNode.left)
      if (currentNode.right) queue.push(currentNode.right)
    }
    result.push(levelResult)
  }
  return result
}