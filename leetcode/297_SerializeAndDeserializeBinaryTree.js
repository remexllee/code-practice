/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * Encodes a tree to a single string.
 *
 * @param {TreeNode} root
 * @return {string}
 */
var serialize = function (root) {
  let str = []
  var buildString = function (root) {
    if (root == null) {
      str.push(null)
      return
    }

    str.push(root.val)
    buildString(root.left)
    buildString(root.right)
  }
  buildString(root)
  return str
}

/**
 * Decodes your encoded data to tree.
 *
 * @param {string} data
 * @return {TreeNode}
 */
var deserialize = function (data) {
  var buildTree = function (data) {
    let value = data.shift()
    if (value == null) {
      return null
    } else {
      let node = new TreeNode(value)
      node.left = buildTree(data)
      node.right = buildTree(data)
      return node
    }
  }
  return buildTree(data)
}

/**
 * Your functions will be called as such:
 * deserialize(serialize(root));
 */