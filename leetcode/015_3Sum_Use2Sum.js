var threeSum = nums => {
  var result = []

  nums.sort((a, b) => a - b)
  for (let i = 0; i < nums.length - 2; ++i) {
    //此两步可以节省时间；
    if (nums[i] == nums[i - 1]) continue
    if (nums[i] > 0) break

    var temp = twoSum(nums.slice(i + 1), -nums[i])
    if (temp) {
      result = result.concat(temp)
    }
  }

  return result
}

console.log(threeSum([-1, 0, 1, 2, -1, -4]))

function twoSum (nums, target) {
  console.log('twoSum: ', nums, target)
  var result = [], sum
  for (let i = 0, j = nums.length - 1; i < j; i, j) {
    sum = nums[i] + nums[j]
    if (nums[i] == nums[i - 1] || sum < target) {
      ++i
      continue
    }

    if (sum == target) {

      result.push([-target, nums[i], nums[j]])
      ++i
    }
    else if (sum > target) {
      --j
    }
  }
  console.log('result:', result)
  return result
}


