var lengthOfLongestSubstringTwoDistinct = function (s) {
  if (!s || s.length === 0) {
    return 0
  } else if (s.length === 1) {
    return 1
  }

  let max = 0
  let start = 0
  let count = 0
  let letter1 = null
  let letter2 = null

  for (let i = 0; i < s.length; i++) {
    if (letter1 === s[i] || letter2 === s[i]) {
      count++
    } else if (!letter1) {
      letter1 = s[i]
      count++
    } else if (!letter2) {
      letter2 = s[i]
      count++
    } else {
      if (max < count) {
        max = count
      }
      letter1 = null
      letter2 = null
      i = start
      start = start + 1
      count = 0
    }
  }

  if (max < count) {
    max = count
  }

  return max
}