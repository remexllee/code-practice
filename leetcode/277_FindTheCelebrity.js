var solution = function (knows) {
  /**
   * @param {integer} n Total people
   * @return {integer} The celebrity
   */
  return function (n) {
    let i = 0

    for (let k = i; k < n; k++) if (knows(i, k)) i = k
    for (let k = 0; k < i; k++) {
      if (knows(i, k))
        return -1
    }
    for (let k = i + 1; k < n; k++) {
      if (!knows(k, i))
        return -1
    }

    return i
  }
}