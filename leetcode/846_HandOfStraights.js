var isNStraightHand = function (hand, W) {
  if (hand.length % W !== 0) {
    return false
  }

  var freq = {}
  hand.forEach(num => {
    if (!freq[num]) {
      freq[num] = 0
    }
    freq[num]++
  })

  hand.sort(function (a, b) {
    if (a < b) {
      return -1
    } else if (a > b) {
      return 1
    }
    return 0
  })

  for (var i = 0; i < hand.length; i++) {
    if (!freq[hand[i]]) {
      continue
    }

    for (var j = 0; j < W; j++) {
      if (freq[hand[i] + j]) {
        freq[hand[i] + j]--
      } else {
        return false
      }
    }
  }

  return true
}