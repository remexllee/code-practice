/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
var findTarget = function (root, k) {
  let visited = new Set()
  return find(root)

  function find (root) {
    if (!root) return false
    let t = k - root.val
    if (visited.has(t)) return true
    visited.add(root.val)
    return find(root.left) || find(root.right)
  }
}

// const tree = TreeTool.generateBST()
const target = 9

// console.log(findTarget(tree, target))