const merge = (l1, l2) => {
  if (!l1) return l2
  if (!l2) return l1
  if (l1.val < l2.val) {
    l1.next = merge(l1.next, l2)
    return l1
  } else {
    l2.next = merge(l1, l2.next)
    return l2
  }
}
var mergeKLists = function (lists) {
  if (lists.length <= 1) {
    return lists.length ? lists[0] : null
  }

  const mid = lists.length >> 1
  const left = lists.slice(0, mid)
  const right = lists.slice(mid)
  return merge(mergeKLists(left), mergeKLists(right))
}

// itereval
// var mergeKLists = function(lists) {
//   return lists.reduce((accu, list) => merge(accu, list), null)
// };