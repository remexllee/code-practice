const palindromePairs = function (words) {
  const result = []
  const wordLookup = new Map()
  for (let i = 0; i < words.length; i++) {
    wordLookup.set(words[i], i)
  }
  for (let i = 0; i < words.length; i++) {
    // split words[i] into to 2 pieces
    const w = words[i]
    for (let cut = 0; cut <= w.length; cut++) {
      if (cut >= 1 && isPalindrome(w, 0, cut - 1)) {
        const revR = w.slice(cut).split('').reverse().join('')
        if (wordLookup.has(revR) && wordLookup.get(revR) !== i) {
          result.push([wordLookup.get(revR), i])
        }
      }
      if (isPalindrome(w, cut, w.length - 1)) {
        const revL = w.slice(0, cut).split('').reverse().join('')
        if (wordLookup.has(revL) && wordLookup.get(revL) !== i) {
          result.push([i, wordLookup.get(revL)])
        }
      }
    }
  }
  return result
}

const isPalindrome = function (s, l, r) {
  while (l < r) {
    if (s[l] !== s[r]) {
      return false
    }
    l++, r--
  }
  return true
}