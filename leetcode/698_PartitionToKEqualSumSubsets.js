var canPartitionKSubsets = function (nums, k) {
  if (nums == null) {
    return false
  }
  let sum = 0
  for (let num of nums) sum += num
  if (sum % k !== 0) {
    return false
  } else {
    let visited = new Array(nums.length).fill(false)
    return helper(nums, 0, visited, k, 0, sum / k)
  }
}

var helper = (nums, index, visited, k, curr, target) => {
  if (k === 1) {
    return true
  }
  let ret = false
  for (let i = index; i < nums.length; i++) {
    if (ret === true) {
      break
    }
    if (visited[i] === true || curr + nums[i] > target) {
      continue
    }
    visited[i] = true
    let sum = curr + nums[i]
    if (sum === target) {
      ret = ret || helper(nums, 0, visited, k - 1, 0, target)
    } else {
      ret = ret || helper(nums, i + 1, visited, k, sum, target)
    }
    visited[i] = false
  }
  return ret
}