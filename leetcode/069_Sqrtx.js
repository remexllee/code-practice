// use binarySearch template 1

const binarySearchT1 = (nums, target) => {
  if (nums === null || nums.length === 0) return -1

  let left = 0
  let right = nums.length - 1

  while (left <= right) {
    let mid = ~~(left + (right - left) / 2)
    if (nums[mid] === target) return mid
    else if (nums[mid] < target) left = mid + 1
    else right = mid - 1
  }

  return -1
}

const mySqrt = (x) => {
  if (x === 0 || x === 1) return x
  let left = 0
  let right = x

  while (left <= right) {
    let mid = ~~(left + (right - left) / 2)
    
    // 但是不是很明为什们用sqrt 命名变量
    // 这里 mid 即是下标也是下标代表的数值
    // 这里的sqrt就是target
    // Target是动态变化的数值结果

    let target = ~~(x / mid)

    if (target === mid) {
      return mid
    } else if (mid < target) {
      left = mid + 1
    } else {
      right = mid - 1
    }
  }

  // 在寻找逼近sqrt的过程，但是不是很明为什们用sqrt 命名变量
  // right+1 == left, and sqrt is between left and right values, so truncate the decimal and return right
  console.log(left, right)
  return right
}

console.log(mySqrt(16))
