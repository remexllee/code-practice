// var isValidSudoku = function (board) {
//   const boxes = [{}, {}, {}, {}, {}, {}, {}, {}, {}]
//   const cols = [{}, {}, {}, {}, {}, {}, {}, {}, {}]
//   const rows = [{}, {}, {}, {}, {}, {}, {}, {}, {}]
//
//   for (let i = 0; i < 9; i++) {
//     for (let j = 0; j < 9; j++) {
//       const digit = board[i][j]
//       if (digit !== '.') {
//         const k = Math.floor(j / 3) + (Math.floor(i / 3) * 3)
//         if (boxes[k][digit] || cols[j][digit] || rows[i][digit]) {
//           return false
//         }
//         boxes[k][digit] = cols[j][digit] = rows[i][digit] = true
//       }
//     }
//   }
//
//   return true
// }

var isValidSudoku = function (board) {
  const set = new Set()
  for (let i = 0; i < 9; i++) {
    for (let j = 0; j < 9; j++) {
      let number = board[i][j]
      if (number != '.') {
        if (
          set.has(number + 'in row' + i) ||
          set.has(number + 'in col' + j) ||
          set.has(number + 'in block' + Math.floor(i / 3) + '-' + Math.floor(j / 3))
        ) {
          return false
        } else {
          set.add(number + 'in row' + i)
          set.add(number + 'in col' + j)
          set.add(number + 'in block' + Math.floor(i / 3) + '-' + Math.floor(j / 3))
        }
      }
    }
  }
  return true
}

console.log(isValidSudoku([
  ['5', '3', '.', '.', '7', '.', '.', '.', '.'],
  ['6', '.', '.', '1', '9', '5', '.', '.', '.'],
  ['.', '9', '8', '.', '.', '.', '.', '6', '.'],
  ['8', '.', '.', '.', '6', '.', '.', '.', '3'],
  ['4', '.', '.', '8', '.', '3', '.', '.', '1'],
  ['7', '.', '.', '.', '2', '.', '.', '.', '6'],
  ['.', '6', '.', '.', '.', '.', '2', '8', '.'],
  ['.', '.', '.', '4', '1', '9', '.', '.', '5'],
  ['.', '.', '.', '.', '8', '.', '.', '7', '9']
]))
