var calculate = function (s) {
  if (!s) return 0
  let num = 0, opr = '+'
  let stack = []

  for (let i = 0; i < s.length; i++) {
    let c = s[i]

    if (c !== ' ' && !isNaN(c)) num = num * 10 + Number(c)

    if (i === s.length - 1 || (c !== ' ' && isNaN(c))) {
      if (opr === '+') stack.push(num)
      if (opr === '-') stack.push(-num)
      if (opr === '*') stack.push(stack.pop() * num)
      if (opr === '/') stack.push(parseInt(stack.pop() / num))

      opr = c
      num = 0
    }
  }

  let sum = 0
  while (stack.length !== 0) sum += stack.pop()

  return sum
}