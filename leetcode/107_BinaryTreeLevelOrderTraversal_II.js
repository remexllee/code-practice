var levelOrderBottom = function(root) {
  if (!root) return []

  const result = []
  const queue = [root]

  while (queue.length) {
    const level = []
    let size = queue.length
    for (let i = 0; i < size; i++) {
      let currentNode = queue.shift()
      level.push(currentNode.val)
      if (currentNode.left) queue.push(currentNode.left)
      if (currentNode.right) queue.push(currentNode.right)
    }
    result.unshift(level)
  }
  return result
};