// https://leetcode.com/problems/subarray-sum-equals-k/discuss/134689/Three-Approaches-With-Explanation
// const subarraySum = (nums, k) => {
//   let count = 0, sum = 0
//   let map = new Map()
//   map.set(0, 1)
//   nums.map(v => {
//     sum += v
//     if (map.has(sum - k))
//       count += map.get(sum - k)
//     map.has(sum) ? map.set(sum, map.get(sum) + 1) : map.set(sum, 1)
//   })
//   return count
// }

const subarraySum = (nums, k) => {
  if (nums.length === 0) {
    return 0
  }

  const map = new Map()
  map.set(0, 1)
  let sum = 0
  let count = 0

  for (let num of nums) {
    sum += num
    let target = sum - k
    if (map.has(target)) {
      count += map.get(target)
    }
    map.set(sum, ~~(map.get(sum)) + 1)
  }
  return count
}

console.log(subarraySum([1, 1, 1], 2))

// [3 4 7 2 -3 1 4 2] k = 7
//
// init    ,  sum = 0,                 count = 0,  map {(0, 1)}
// curr = 3,  sum = 3,   sum - k = -4, count = 0,  map {(0, 1), (3, 1)}
// curr = 4   sum = 7,   sum - k = 0   count = 1,  map {(0, 1), (3, 1), (7, 1)}
// curr = 7   sum = 14,  sum - k = 7   count = 2,  map {(0, 1), (3, 1), (7, 1), (14, 1)}
// curr = 2   sum = 16,  sum - k = 9   count = 2,  map {(0, 1), (3, 1), (7, 1), (14, 1), (16, 1)}
// ...
// https://leetcode.com/articles/subarray-sum-equals-k/