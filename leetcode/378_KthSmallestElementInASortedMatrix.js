// https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/discuss/85221/Javascript-binary-search-beats-94
var kthSmallest = function (matrix, k) {
  return [].concat(...matrix).sort((a, b) => a - b)[k - 1]
}