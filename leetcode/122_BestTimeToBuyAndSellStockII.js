var maxProfit = function (prices) {
  let maxProfit = 0
  let minValue = Number.MAX_VALUE

  for (let price of prices) {
    minValue = Math.min(price, minValue)
    if (price - minValue > 0) {
      maxProfit += price - minValue
      minValue = price
    }
  }
  return maxProfit
}