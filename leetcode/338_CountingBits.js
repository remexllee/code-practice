var countBits = function (num) {
  let f = [0]
  for (let i = 1; i <= num; i++) {
    if (i % 2 === 1) { // it's odd...
      f.push(f[i - 1] + 1)
    } else {
      f.push(f[i / 2])
    }
  }
  return f
}