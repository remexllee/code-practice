/**
 * @param {string} s
 * @return {number}
 */
function titleToNumber(s) {
  let n = 0
  for (let c of s) {
    n = n * 26 + c.charCodeAt(0) - 'A'.charCodeAt(0) + 1
  }
  return n;
}
console.log('A'.charCodeAt(0) - 64)

// console.log(titleToNumber('A'))
// console.log(titleToNumber('Z'))
// console.log(titleToNumber('AA'))
console.log(titleToNumber('ZZ'))
console.log(titleToNumber('AAA'))