// var majorityElement = function (nums) {
//   if (nums.length === 1) {
//     return nums[0]
//   }
//
//   const map = new Map()
//   const halfSize = ~~(nums.length / 2)
//   for (let i = 0; i < nums.length; i++) {
//     if (map.has(nums[i])) {
//       let curVal = map.get(nums[i])
//       if (curVal + 1 > halfSize) {
//         return nums[i]
//       } else {
//         map.set(nums[i], curVal + 1)
//       }
//     } else {
//       map.set(nums[i], 1)
//     }
//   }
// }

var majorityElement = function (nums) {
  let major = nums[0], count = 1
  for (let i = 1; i < nums.length; i++) {
    if (count === 0) {
      count++
      major = nums[i]
    } else if (major === nums[i]) {
      count++
    } else {
      count--
    }
  }
  return major
}
console.log(majorityElement([3]))
console.log(majorityElement([3, 2, 3]))
console.log(majorityElement([2, 2, 1, 1, 1, 2, 2]))