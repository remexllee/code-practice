var isMonotonic = function (A) {
  var increasing = true
  var decreasing = true

  for (var i = 0; i < A.length - 1; i++) {
    if (A[i] > A[i + 1]) {
      increasing = false
    }
    if (A[i] < A[i + 1]) {
      decreasing = false
    }
  }
  return increasing || decreasing
}