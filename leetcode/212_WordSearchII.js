var findWords = function (board, words) {
  const trie = new Trie()
  words.forEach(word => trie.add(word))
  const foundWords = []
  for (let x = 0; x < board[0].length; x++) {
    for (let y = 0; y < board.length; y++) {
      checkWordsAt(x, y, trie, board, foundWords)
    }
  }

  return foundWords
}

var checkWordsAt = function (x, y, node, board, foundWords) {
  if (node.word) {
    foundWords.push(node.word)
    node.word = null
  }

  if (x < 0 || y < 0 || x >= board[0].length || y >= board.length) return

  const c = board[y][x]

  if (c === '#' || !node.children.has(c)) return

  const newNode = node.children.get(c)

  board[y][x] = '#'
  checkWordsAt(x + 1, y, newNode, board, foundWords)
  checkWordsAt(x, y + 1, newNode, board, foundWords)
  checkWordsAt(x - 1, y, newNode, board, foundWords)
  checkWordsAt(x, y - 1, newNode, board, foundWords)

  board[y][x] = c
}

var Trie = function () {
  this.children = new Map()
  this.word = null
}

Trie.prototype.add = function (word) {
  let curr = this
  for (let i = 0; i < word.length; i++) {
    const c = word[i]
    if (!curr.children.has(c)) curr.children.set(c, new Trie())
    curr = curr.children.get(c)
  }
  curr.word = word
}

const board = [
  ['o', 'a', 'a', 'n'],
  ['e', 't', 'a', 'e'],
  ['i', 'h', 'k', 'r'],
  ['i', 'f', 'l', 'v']
]

let words = ['oath', 'pea', 'eat', 'rain']

console.log(findWords(board, words))
