function maxProduct (nums) {
  let res = -Number.MAX_VALUE
  let min = 1
  let max = 1
  for (let num of nums) {
    [min, max] = [
      Math.min(num, min * num, max * num),
      Math.max(num, min * num, max * num),
    ]
    res = Math.max(res, max)
  }
  return res
}

maxProduct([2, 3, -2, -4])
// maxProduct([2, 3, -2, 4])
// https://leetcode.com/problems/maximum-product-subarray/discuss/119171/Simple-JavaScript-solution

