// https://leetcode.com/problems/binary-tree-vertical-order-traversal/discuss/76433/Easy-JavaScript-solution-using-BFS-no-map-beats-95

function TreeNode (val) {
  this.val = val
  this.left = this.right = null
}

/***************
 *
 *      1
 *     / \
 *    2  3
 *   / \
 *  4  5
 *
 *  preOrder: 1, 2, 4, 5, 3
 *  inOrder: 4, 2, 5, 1, 3
 *  PostOrder: 4, 5, 2, 3, 1
 *
 ********************/

const root = new TreeNode(1)
const left1 = new TreeNode(2)
const right1 = new TreeNode(3)
const left1left = new TreeNode(4)
const left1right = new TreeNode(5)

root.left = left1
root.right = right1
left1.left = left1left
left1.right = left1right

var verticalOrder = function (root) {
  const queue = [[root, 0]]
  const nodesByColumn = {}

  while (queue.length > 0) {
    const [node, column] = queue.shift()

    if (node === null) continue

    nodesByColumn[column] = nodesByColumn[column] || []
    nodesByColumn[column].push(node.val)

    queue.push([node.left, column - 1])
    queue.push([node.right, column + 1])
  }

  console.log(nodesByColumn)
  const orderedColumns = Object.keys(nodesByColumn).sort((a, b) => a - b)
  console.log(orderedColumns)
  return orderedColumns.map(columnKey => {
    return nodesByColumn[columnKey]
  })
}

console.log(verticalOrder(root))

// nodesByColumn
// { '0': [ 1, 5 ], '1': [ 3 ], '-1': [ 2 ], '-2': [ 4 ] }
//
// orderedColumns
// [ '-2', '-1', '0', '1' ]
//
// result
// [ [ 4 ], [ 2 ], [ 1, 5 ], [ 3 ] ]
//