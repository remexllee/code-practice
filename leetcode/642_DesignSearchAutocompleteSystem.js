const a = 'a'.charCodeAt(0)

class TrieNode {
  constructor () {
    this.children = []
    this.isEnd = false
    this.values = []
  }
}

class Trie {
  constructor (sentences, times) {
    this.root = new TrieNode()
    this.sentenceMap = new Map()
    sentences.forEach((sentence, i) => {
      this.sentenceMap.set(sentence, times[i])
    })
    this.buildTree()
  }

  buildTree () {
    for (let [key, value] of this.sentenceMap) {
      this.add(key)
    }
  }

  add (sentence) {
    let node = this.root
    for (let i = 0; i < sentence.length; i++) {
      let char = sentence[i] === ' ' ? 26 : sentence.charCodeAt(i) - a
      if (!node.children[char]) {
        node.children[char] = new TrieNode()
      }
      node.children[char].values.push(sentence)
      node = node.children[char]
    }
    node.isEnd = true
  }

  search (node, ch) {
    let char = ch === ' ' ? 26 : (ch.charCodeAt(0) - a)
    if (node.children[char]) return node.children[char]
    return null
  }
}

function AutocompleteSystem (sentences, times) {
  this.trie = new Trie(sentences, times)
  this.prev = ''
  this.curNode = this.trie.root
}

AutocompleteSystem.prototype.input = function (c) {
  if (c === '#') {
    let count = this.trie.sentenceMap.get(this.prev)
    if (!count) {
      this.trie.add(this.prev)
      this.trie.sentenceMap.set(this.prev, 1)
    } else {
      this.trie.sentenceMap.set(this.prev, count + 1)
    }
    this.prev = ''
    this.curNode = this.trie.root
    return []
  }
  this.prev += c
  if (this.curNode) {
    this.curNode = this.trie.search(this.curNode, c)
    return (this.curNode && this.curNode.values.sort((a, b) => {
      let countB = this.trie.sentenceMap.get(b)
      let countA = this.trie.sentenceMap.get(a)
      if (countB === countA) {
        return a < b ? -1 : 1
      } else {
        return countB - countA
      }
    }) || []).slice(0, 3)
  } else {
    return []
  }
}