const removeDuplicates = function (nums) {
  let hash = {}
  let cur = 0
  for (let i = 0; i < nums.length; i++) {
    let num = nums[i]

    if (hash[num] === undefined) {
      hash[num] = true
      nums[cur] = num
      cur++
    }
  }
  return cur
}
