var wordBreak = function (s, wordDict) {
  if (!wordDict || wordDict.length == 0) return false
  let set = new Set([...wordDict])
  var dp = new Array(s.length + 1)
  dp.fill(false)
  dp[0] = true

  for (var i = 1; i <= s.length; i++) {
    for (var j = 0; j < i; j++) {
      if (dp[j] && set.has(s.substring(j, i))) {
        dp[i] = true
        break
      }
    }
  }
  return dp[s.length]
}

// https://leetcode.com/problems/word-break/discuss/43834/JavaScript-Working-Solution-using:-Trie-+-Recursion-+-Memoization
// JavaScript Working Solution using: Trie + Recursion + Memoization
// this solution is slow
