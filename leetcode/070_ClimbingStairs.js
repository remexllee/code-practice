const climbStairs = (n) => {
  let prev = 1
  let curr = 1

  for (let i = 1; i < n; i++) {
    [prev, curr] = [curr, prev + curr]
  }
  return curr
}

console.log(climbStairs(3))
console.log(climbStairs(5))