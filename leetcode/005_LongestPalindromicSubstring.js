// const longestPalindrome = (s) => {
//   if (s === null || s.length === 0) {
//     return ''
//   }
//
//   let result = ''
//   let len = 2 * s.length - 1
//   let left, right
//
//   for (let i = 0; i < len; i++) {
//     left = right = parseInt(i / 2)
//     if (i % 2 === 1) {
//       right++
//     }
//
//     let str = expandFromCenterAndCheckFroPalindrome(s, left, right)
//     if (str.length > result.length) {
//       result = str
//     }
//   }
//   return result
// }
//
// const expandFromCenterAndCheckFroPalindrome = (s, left, right) => {
//   while (left >= 0 && s.length && s[left] === s[right]) {
//     left--
//     right++
//   }
//   return s.substring(left + 1, right)
// }
//
// const s = 'babad'
// console.log(longestPalindrome(s))

var longestPalindrome = function (s) {
  var max = ''
  for (var i = 0; i < s.length; i++) {
    for (var j = 0; j < 2; j++) {
      var left = i
      var right = i + j
      while (s[left] && s[left] === s[right]) {
        left--
        right++
      }
      if ((right - left - 1) > max.length) {
        max = s.substring(left + 1, right)
      }
    }
  }
  return max
}

const s = 'babad'
console.log(longestPalindrome(s))
