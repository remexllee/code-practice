var leastInterval = function (tasks, n) {
  var mapFor26Tasks = new Array(26).fill(0)
  var maxFrequency = Number.MIN_VALUE
  for (let i = 0; i < tasks.length; i++) {
    const key = tasks[i].charCodeAt(0) - 'A'.charCodeAt(0)
    mapFor26Tasks[key]++
    maxFrequency = Math.max(maxFrequency, mapFor26Tasks[key])
  }
  var sameMaxFrequencyNum = mapFor26Tasks.filter(task => task === maxFrequency).length
  return Math.max(tasks.length, (n + 1) * (maxFrequency - 1) + sameMaxFrequencyNum)
}

// https://leetcode.com/articles/task-scheduler/
// https://zxi.mytechroad.com/blog/greedy/leetcode-621-task-scheduler/ 花花
// 公式 ( answer = (maxTaskFrequencyNumber - 1) * (interval + 1) + sameNumber
// 特殊例子， 如果totalTasksNumber > answer ? totalTasksNumber : answer
// We can schedule most requent tasks without idling
// => we can schedule all tha tasks without idling
// ans = # of tasks

const tasks1 = ['A', 'A', 'A', 'B', 'B', 'B']
console.log(leastInterval(tasks1, 2))

// const tasks2 = ['A', 'B', 'C', 'D', 'E', 'F']
// console.log(leastInterval(tasks2, 2))
//
// const tasks3 = ['A', 'A', 'A', 'D', 'E', 'F']
// console.log(leastInterval(tasks3, 2))
//
// const tasks4 = ['A', 'C', 'C', 'C', 'E', 'E', 'E']
// console.log(leastInterval(tasks4, 2))

// 每次执行A task必须间隔N的slot，里面可以是idle，也可以是其他不是A的task

// A，A，A，B，B，B, 2
// 可以安排为  A B Idle | A B Idle | A B    => 8

// A, B, C, D, E, F, 2
// A B | C D | E F  => 6

// A，A，A，B，C，D, 2
// 可以安排为  A B C | A D Idle | A  => 7

// A, C, C, C, E, E, E, 2
// C, E, A | C, E, Ide | C, E

// tasks = ['A', 'A', 'A', 'A', 'B', 'B', 'B', 'C', 'C', 'D', 'D', 'F', 'F', 'G']
// # tasks = 14 n = 2
// answer = (4 - 1) * (2 + 1) + 1 = 10

