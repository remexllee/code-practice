// const totalFruit = function (tree) {
//   let head = 0
//   let tail = 0
//   const fruits = new Map()
//
//   let res = 0
//   while (head < tree.length) {
//     fruits.set(tree[head], (fruits.get(tree[head]) || 0) + 1)
//     head += 1
//     while (fruits.size > 2) {
//       fruits
//       fruits.set(tree[tail], fruits.get(tree[tail]) - 1)
//       if (fruits.get(tree[tail]) === 0) {
//         fruits.delete(tree[tail])
//       }
//       tail++
//       console.log('refactoring:', fruits)
//     }
//     res = Math.max(res, head - tail)
//     console.log(fruits)
//   }
//   return res
// }


const totalFruit = function (tree) {
  let head = 0
  let tail = 0
  const fruits = new Map([...new Set(tree)].map(elem => [elem, 0]))

  let res = 0
  while (head < tree.length) {
    fruits.set(tree[head], (fruits.get(tree[head]) || 0) + 1)
    head += 1
    while (fruits.size > 2) {
      fruits.set(tree[tail], fruits.get(tree[tail]) - 1)
      if (fruits.get(tree[tail]) === 0) {
        fruits.delete(tree[tail])
      }
      tail++
      console.log('refactoring:', fruits)
    }
    res = Math.max(res, head - tail)
    console.log(fruits)
  }
  return res
}


// console.log(totalFruit([3, 3, 3, 1]))
console.log(totalFruit([3, 3, 3, 1, 2, 1, 1, 2, 3, 3, 4]))

const test = [3, 3, 3, 1, 2, 1, 1, 2, 3, 3, 4]
const map = new Map([...new Set(test)].map(elem => [elem, 0]))
// init dict from array, TODO: add to cheatsheet
console.log(map)

