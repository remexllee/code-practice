// https://leetcode.com/problems/linked-list-cycle/discuss/147380/Javascript-Solution-and-So-Easy-~

var hasCycle = function (head) {

  if (!head || !head.next) return false

  var slow = head
  var fast = head.next

  while (fast.next && fast.next.next) {
    slow = slow.next
    fast = fast.next.next
    if (slow == fast) return true
  }

  return false
}