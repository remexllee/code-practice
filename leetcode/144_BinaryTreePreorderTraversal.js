const TreeTool = require('../data-structure/Utils/TreeTool')

// Pre - Order Traversal ( root - leftChild - rightChild )
var preorderTraversal = function (root) {
  if (!root) return []

  const result = []
  const stack = [root]

  while (stack.length) {
    let currentNode = stack.pop()
    result.push(currentNode.val)
    if (currentNode.right) stack.push(currentNode.right)
    if (currentNode.left) stack.push(currentNode.left)
  }

  return result
}

const root = TreeTool.generateBinarySearchTree()

// Expect A, B, D, I, J, F, C, G, K, H
console.log(preorderTraversal(root))