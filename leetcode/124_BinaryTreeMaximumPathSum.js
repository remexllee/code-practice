// Explain: https://github.com/crimx/leetcope

var maxPathSum = function (root) {
  return Math.max(..._maxPathSum(root))
}

function _maxPathSum (root) {
  if (!root) { return [-Infinity, -Infinity] }
  const left = _maxPathSum(root.left)
  const right = _maxPathSum(root.right)
  return [
    Math.max(
      left[0],
      right[0],
      root.val + Math.max(0, left[1], right[1], left[1] + right[1])
    ),
    Math.max(left[1], right[1], 0) + root.val
  ]
}