var reorderList = function (head) {
  if (!head) {
    return
  }

  const stack = []

  let ptr = head
  let len = 0

  while (ptr) {
    stack.push(ptr)
    len += 1
    ptr = ptr.next
  }

  ptr = head
  len -= 1
  let counter = 0

  while (counter < Math.ceil(len / 2)) {
    ptr = stack[counter]
    ptr.next = stack[len - counter]
    counter += 1
    ptr.next.next = stack[counter]
  }

  stack[counter].next = null
}