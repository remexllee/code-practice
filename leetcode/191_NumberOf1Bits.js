// https://leetcode.com/problems/number-of-1-bits/discuss/55121/Javascript-or-Brian-Kernighan's-Algorithm

/**
 * @param {number} n - a positive integer
 * @return {number}
 */
var hammingWeight = function(n) {
  var count = 0;
  while (n) {
    n &= (n-1) ;
    count++;
  }
  return count;
};