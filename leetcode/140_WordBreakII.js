// https://leetcode.com/problems/word-break-ii/discuss/44384/Share-my-DP-JavaScript-solution

var wordBreak = function (s, wordDict) {
  let cache = new Map()
  let helper = function (s, wordDict, cache) {
    if (cache.has(s)) {
      return cache.get(s)
    }
    let res = []
    if (wordDict.indexOf(s) !== -1) {
      res.push(s)
    }
    for (let i = 1; i < s.length; i++) {
      let right = s.substring(i)
      if (wordDict.indexOf(right) === -1) {
        continue
      }
      let leftBreaks = helper(s.substring(0, i), wordDict, cache)
      leftBreaks.forEach((leftBreak) => {
        res.push(`${leftBreak} ${right}`)
      })
    }
    cache.set(s, res)
    return res
  }
  return helper(s, wordDict, cache)
}

let s = 'catsandog'
let wordDict = ['cats', 'dog', 'sand', 'and', 'cat']

console.log(wordBreak(s, wordDict))