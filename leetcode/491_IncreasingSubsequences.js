var findSubsequences = function (nums) {
  if (nums.length <= 1) return []

  let results = []
  getSubsequences(nums, 0, [], results)
  return results
}

function getSubsequences (nums, start, items, results) {
  let visited = new Set()
  for (let i = start; i < nums.length; i++) {
    let num = nums[i]
    if (visited.has(num)) continue
    if (items.length === 0 || (items.length > 0 && num >= items[items.length - 1])) {
      let newItems = items.concat([num])
      if (newItems.length >= 2) {
        results.push(newItems)
      }
      getSubsequences(nums, i + 1, newItems, results)
    }
    visited.add(num)
  }
}