/**
 * @param {number[]} nums
 * @return {number}
 */
// var maxSubArray = function (nums) {
//   let last = 0
//   let max = nums[0]
//
//   for (let num of nums) {
//     if (last >= 0) {
//       last += num
//     } else {
//       last = num
//     }
//
//     if (last > max) {
//       max = last
//     }
//   }
//   return max
// }


var maxSubArray = function (nums) {
  if (nums.length === 0) {
    return null
  }

  var maxVal = currMax = nums[0]
  for (var i = 1; i < nums.length; i++) {
    console.log(currMax)
    currMax = Math.max(nums[i], currMax + nums[i])
    maxVal = Math.max(currMax, maxVal)
  }
  return maxVal
}

const test = [-2, 1, -3, 4, -1, 2, 1, -5, 4]

// console.log(maxSubArray(test))
console.log(maxSubArray([-1]))
// console.log(maxSubArray([-2, -1]))