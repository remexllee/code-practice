var maxProfit = function (prices) {
  var buy1 = Number.MIN_SAFE_INTEGER
  var sell1 = 0
  var buy2 = Number.MIN_SAFE_INTEGER
  var sell2 = 0

  for (var i = 0; i < prices.length; i++) {
    buy1 = Math.max(buy1, -prices[i])
    sell1 = Math.max(sell1, prices[i] + buy1)
    buy2 = Math.max(buy2, sell1 - prices[i])
    sell2 = Math.max(sell2, prices[i] + buy2)
  }
  return sell2
}