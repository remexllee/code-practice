var numTrees = function (n) {
  if (n === 1) {
    return 1
  }
  let memo = [1, 1] // size of n

  for (let numNodes = 2; numNodes <= n; numNodes++) {
    let result = 0
    for (let j = 0; j < numNodes; j++) {
      let numLeft = j // number of nodes to the left
      let numRight = numNodes - (j + 1) // number of stuff to the right
      result = result + (memo[numLeft] * memo[numRight])
    }
    memo[numNodes] = result
  }

  return memo[n]
}