var numberOfBoomerangs = function (points) {
  var len = points.length
  var count = 0

  for (var i = 0; i < len; i++) {
    var map = new Map()
    for (var jk = 0; jk < len; jk++) {
      if (i === jk) {
        continue
      }
      var d = getDistance(points[i], points[jk])
      if (map.get(d)) {
        map.set(d, map.get(d) + 1)
      } else {
        map.set(d, 1)
      }
    }
    map.forEach(function (m) {
      count += m * (m - 1)
    })
  }

  return count
}

function getDistance (i, j) {
  return (i[0] - j[0]) * (i[0] - j[0]) + (i[1] - j[1]) * (i[1] - j[1])
}