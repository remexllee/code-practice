const TreeTool = require('../../Utils/TreeTool')
const root = TreeTool.generateBinaryTree()

var flatten = function (root) {
  flattenTree(root)
}

function flattenTree (node) {
  if (!node) return node
  var leftTail = flattenTree(node.left)
  var rightTail = flattenTree(node.right)

  if (!leftTail) return rightTail ? rightTail : node
  leftTail.right = node.right
  node.right = node.left
  node.left = null
  return rightTail ? rightTail : leftTail
}

flatten(root)
console.log(root)
