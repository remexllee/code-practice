// https://leetcode.com/problems/closest-binary-search-tree-value/discuss/70326/Concise-JavaScript-recursive-and-iterative-solutions

var closestValue = function (root, target) {
  var closest = root.val

  while (root) {
    if (Math.abs(root.val - target) < Math.abs(closest - target)) {
      closest = root.val
    }

    if (root.val === target) break // early terminate

    root = target > root.val ? root.right : root.left
  }

  return closest
}

var closestValue = function (root, target) {
  var child = target < root.val ? root.left : root.right

  if (!child) return root.val

  var closest = closestValue(child, target)

  return Math.abs(closest - target) < Math.abs(root.val - target) ? closest : root.val
}

