var isIsomorphic = function (s, t) {
  let mapS = new Map()
  let mapT = new Map()

  for (let i = 0; i < s.length; i++) {
    if (mapS.get(s[i]) !== mapT.get(t[i])) {
      return false
    }
    mapS.set(s[i], i + 1)
    mapT.set(t[i], i + 1)
  }
  return true
}

// console.log(isIsomorphic('paper', 'title'))
console.log(isIsomorphic('paper', 'pitle'))