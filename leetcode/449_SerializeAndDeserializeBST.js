/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * Encodes a tree to a single string.
 *
 * @param {TreeNode} root
 * @return {string}
 */
var serialize = function (root) {
  if (root === null) {
    return ''
  }
  const result = []
  const helper = function (node, result) {
    if (node) {
      result.push(node.val)
      helper(node.left, result)
      helper(node.right, result)
    }
  }

  helper(root, result)
  return result.join(',')
}

/**
 * Decodes your encoded data to tree.
 *
 * @param {string} data
 * @return {TreeNode}
 */
var deserialize = function (data) {
  if (!data) {
    return null
  }
  const arr = data.split(',')
  const helper = function (arr, min, max) {
    if (arr.length === 0) {
      return null
    }
    const val = ~~(arr[0])
    if (val < min || val > max) {
      return null
    }
    arr.shift()
    const root = new TreeNode(val)
    root.left = helper(arr, min, val)
    root.right = helper(arr, val, max)
    return root
  }
  return helper(arr, Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER)
}

/**
 * Your functions will be called as such:
 * deserialize(serialize(root));
 */