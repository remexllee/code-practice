/**
 * @param {string} s
 * @return {boolean}
 */
var validPalindrome = function (s) {
  let left = 0
  let right = s.length - 1

  while (left < right) {
    if (s[left] !== s[right]) {
      return isPartPalindromeValid(s, left + 1, right) || isPartPalindromeValid(s, left, right - 1)
    } else {
      left++
      right--
    }
  }
  return true
}

const isPartPalindromeValid = (s, left, right) => {
  while (left < right) {
    if (s[left] !== s[right]) {
      return false
    } else {
      left++
      right--
    }
  }

  return true
}

// console.log(validPalindrome('aba'))
// console.log(validPalindrome('abca'))
// console.log(validPalindrome('abcda'))
console.log(isPalindrome('abcba', 0, 4))