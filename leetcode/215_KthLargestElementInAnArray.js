const isGreatOrEqualTheKthNumber = (nums, target, k) => {
  let count = 0
  for (let num of nums) {
    if (num <= target) {
      console.log(num)
      count++
    } // find out how many number less than target
  }
  console.log(count, target, k)
  return count >= k
}

var findKthLargest = function (nums, k) {
  let left = Math.min(...nums)
  let right = Math.max(...nums)
  let kthLength = nums.length - k + 1 // important!!!

  while (left < right) {
    let mid = left + ((right - left) >> 1)
    if (isGreatOrEqualTheKthNumber(nums, mid, kthLength)) {
      right = mid
    } else {
      left = mid + 1
    }
  }
  return left
}

// console.log(findKthLargest([-1, 2, 0], 2))
console.log(findKthLargest([3, 5, 1, 1, 20], 1))
// console.log(findKthLargest([3, 2, 1, 5, 6, 4], 2))
// console.log(findKthLargest([1, 5, 5, 5, 5, 5, 20], 3))
// console.log(findKthLargest([3, 2, 1, 5, 6, 4], 2))

// 3 2 1 5 6 4
// lo = 1, hi = 6, mid = 3, isGreatOrEqualTheKthNumber false lo = 4, hi = 6
// lo = 4, hi = 6, mid = 5, isGreatOrEqualTheKthNumber true lo = 4 hi = 5
// lo = 4, hi = 5, mid = 4, isGreatOrEqualTheKthNumber false lo = 5 hi = 5

// console.log(findKthLargest([3, 2, 1, 5, 6, 4], 2))