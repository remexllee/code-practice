// const singleNumber = (nums) => {
//   let total = 0
//   for (let i = 0; i < nums.length; i++) {
//     let num = nums[i]
//     totol ^= num
//   }
//   return total
// }

// leetcode solution
// https://leetcode.com/problems/single-number/solution/

/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNumber = (nums) => nums.reduce((total, num) => total ^= num, 0)