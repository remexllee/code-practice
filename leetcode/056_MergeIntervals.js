var merge = function (intervals) {
  if (intervals.length == 0) return []
  var sorted = intervals.sort(function (a, b) {return a.start - b.start})

  // console.log(sorted)
  var merged = [sorted[0]]

  for (var i = 1; i < intervals.length; i++) {
    if (intervals[i].start > merged[merged.length - 1].end) {
      merged.push(intervals[i])
    } else {
      if (merged[merged.length - 1].end < intervals[i].end) {
        merged[merged.length - 1].end = intervals[i].end
      }
    }
  }
  return merged
}

// const sortInterval = (a, b) => a.start - b.start
//
// const initialMergedInterval = (intervals) => intervals[0] ? [intervals[0]] : []
//
// const mergeIteveral = (merged, current) => {
//   let lastMergedInterval = merged[merged.length - 1]
//   lastMergedInterval.end < current.start
//     ? merged.push(current)
//     : lastMergedInterval.end = Math.max(lastMergedInterval.end, current.end)
//   return merged
// }
//
// const merge = (intervals) => intervals
//   .sort(sortInterval)
//   .reduce(
//     mergeIteveral,
//     initialMergedInterval(intervals)
//   )
//

function Interval (start, end) {
  this.start = start
  this.end = end
}

const i1 = new Interval(1, 3)
const i2 = new Interval(2, 6)
const i3 = new Interval(8, 10)
const i4 = new Interval(15, 18)

const intervals = [i1, i2, i3, i4]

console.log(merge(intervals))

// Array.from([1, 2, 3]).reduce((accu, item, index) => {
//   console.log(index)
// }, [])
//
// // 0, 1, 2
//
//
// Array.from([1, 2, 3]).reduce((accu, item, index) => {
//   console.log(index)
// })
//
// // 1, 2

