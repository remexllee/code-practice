/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */

const twoSum = (nums, target) => {
  const map = new Map()
  for (let i = 0; i < nums.length; i++) {
    let num = target - nums[i]
    if (map.has(num)) {
      return [map.get(num), i]
    } else {
      map.set(nums[i], i)
    }
  }
  return []
}

////////////////

// var reverseString = function (s) {
//   let length = s.length
//   let half = length % 2 === 1 ? ~~(length / 2) + 1 : ~~(length / 2)
//   let stringArray = Array.from(s)
//   for (let i = 0; i < half; i++) {
//     [stringArray[i], stringArray[length - i]] = [stringArray[length - i], stringArray[i]]
//   }
//   return stringArray.join('')
// }
//
// console.log(reverseString('abcdefg'))
// console.log(reverseString('abcdef'))