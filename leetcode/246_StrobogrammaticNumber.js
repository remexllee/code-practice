var isStrobogrammatic = function (num) {
  var rev = ''
  var map = {
    6: 9, 9: 6, 1: 1, 8: 8, 0: 0
  }

  for (let i = num.length - 1; i >= 0; i--) {
    // must use hasOwnProperty, beacuse 0 will get 0
    if (map.hasOwnProperty(num[i])) rev += map[num[i]]
  }

  console.log(rev)
  return rev === num
}

console.log(isStrobogrammatic('2'))
/************
 * 1. define a map for strobogrammatic number, 6 <=> 9, 1 <=> 1, 0 <=> 0, 8 <=> 8
 * 2. use map create a revert string
 * 3. compare num with reverted string
 *************/