class WordDictionary {
  constructor () {
    this.map = {}
  }

  addWord (w) {
    const l = w.length
    this.map[l] = this.map[l] || []
    this.map[l].push(w)
  }

  match (exist, input) {
    for (let i = 0; i < input.length; i++) {
      if (input[i] !== '.' && input[i] !== exist[i]) return false
    }
    return true
  }

  search (w) {
    const l = w.length
    for (const ww of this.map[l] || []) {
      if (this.match(ww, w)) return true
    }
    return false
  }
}