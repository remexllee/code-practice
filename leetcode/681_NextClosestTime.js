/**
 * @param {string} time
 * @return {string}
 */

const timeStringify = (hour, min) =>
  `${hour.toString().padStart(2, '0')}:${min.toString().padStart(2, '0')}`

const getNextTime = (hour, min) => ++min === 60 ? [++hour % 24, 0] : [hour, min]

const isNextClosetTime = (time, curr) =>
  curr.split('').every(char => time.includes(char))

var nextClosestTime = function (time) {
  let currTime = time
  let [hour, min] = time.split(':').map(time => ~~(time))
  do {
    [hour, min] = getNextTime(hour, min)
    currTime = timeStringify(hour, min)
  } while (!isNextClosetTime(time, currTime))
  return currTime
}

/*****************
 * 1. convert time to hour and min string
 * 2. get next time string
 * 3. do while loop to find out the time which include all chars
 * ***************/