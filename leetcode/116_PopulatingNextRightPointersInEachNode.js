const connect = (root) => {
  if (root === null) {
    return
  }

  if (root.left) {
    root.left.next = root.right
  }

  if (root.right) {
    root.right.next = (root.next === null) ? null : root.next.left
  }

  connect(root.left)
  connect(root.right)
}
