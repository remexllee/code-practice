var islandPerimeter = function (grid) {
  var edges = 0
  grid.forEach((row, r) => {
    row.forEach((col, c) => {
      if (col === 1) {
        var left = grid[r][c - 1] || 0
        var right = grid[r][c + 1] || 0
        var top = (grid[r - 1] || [])[c] || 0
        var bottom = (grid[r + 1] || [])[c] || 0
        console.log('r: ', r , 'c: ', c, '|', left, top, right , bottom)
        edges += (4 - top - bottom - left - right)
      }
    })
  })
  return edges
}

const island = [
  [0, 1, 0, 0],
  [1, 1, 1, 0],
  [0, 1, 0, 0],
  [1, 1, 0, 0]
]

console.log(islandPerimeter(island))