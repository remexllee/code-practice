var lowestCommonAncestor = function (root, p, q) {
  //If the value of p is less than the root and q is less than the root, go to the left
  if (p.val < root.val && q.val < root.val) {
    return lowestCommonAncestor(root.left, p, q)
  }
  //If the value of p is greater than the root and q is greater than the root, go to the right
  else if (p.val > root.val && q.val > root.val) {
    return lowestCommonAncestor(root.right, p, q)
  }
  //We found it!
  else {
    return root
  }
}