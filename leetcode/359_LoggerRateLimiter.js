var Logger = function () {
  this.lastSeen = new Map()
  this.TIME_LIMIT = 10
}

Logger.prototype.shouldPrintMessage = function (timestamp, message) {
  if (this.lastSeen.has(message)) {
    if (this.lastSeen.get(message) + this.TIME_LIMIT > timestamp) {
      return false
    }
  }
  this.lastSeen.set(message, timestamp)
  return true
}
