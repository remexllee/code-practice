var groupAnagrams = function (strs) {
  const groups = {}
  for (let s of strs) {
    let key = s.split('').sort().join('')
    groups[key] = [...groups[key] || [], s]
  }
  return Object.values(groups)
}
const test = ['eat', 'tea', 'tan', 'ate', 'nat', 'bat']
console.log(groupAnagrams(test))
