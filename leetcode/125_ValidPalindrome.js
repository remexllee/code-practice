var isPalindrome = function (s) {
  if (s.length < 2) return true
  s = s.toLowerCase()

  const isCharValid = char => (char >= 'a' && char <= 'z' || char >= '0' && char <= '9')

  let left = 0
  let right = s.length - 1
  while (left < right) {
    if (!isCharValid(s[left])) {
      left++
    }
    else if (!isCharValid(s[right])) {
      right--
    }
    else if (s[left] !== s[right]) {
      return false
    } else {
      left++
      right--
    }
  }
  return true
}

console.log(isPalindrome('A man, a plan, a canal: Panama'))

// var isPalindrome = function (s) {
//   const str = s.replace(/[^a-zA-Z0-9]/g, '').toLowerCase()
//   const str = s.replace(/[^a-z0-9]/ig, '').toLowerCase()
//   // const str = s.replace(/\W/g, '').toLowerCase()
//   return str === str.split('').reverse().join('')
// }

// https://leetcode.com/problems/valid-palindrome/discuss/153000/Javascript-solution-without-using-Regex