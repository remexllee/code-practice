// https://leetcode.com/problems/find-peak-element/discuss/133199/JavaScript-linear-scan-+-binary-search-(recursive-+-iterative)-solutions
// template2

function findPeakElement (nums) {
  let left = 0
  let right = nums.length - 1

  while (left < right) {
    const mid = ~~(left + (right - left) / 2)
    console.log(mid, nums[mid], nums[mid + 1])
    if (nums[mid] > nums[mid + 1]) {
      right = mid
    } else {
      left = mid + 1
    }
  }
  return left
}

// console.log(findPeakElement([1, 2, 1, 3, 5, 6, 4]))
// console.log(findPeakElement([1, 2, 1, 3, 5, 6]))
console.log(findPeakElement([1, 2, 4, 3, 1, 0]))