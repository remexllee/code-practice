var numberOfBoomerangs = function (points) {
  // what about two demensional? n**2?
  let res = 0
  for (let i = 0; i < points.length; i++) {
    const map = new Map()
    const a = points[i]
    for (let j = 0; j < points.length; j++) { /// !!! order matters meaning each go through whole list. two anchor meaning two diff triplets.
      // !!! even the other two points order matters, not only who is the anchor point. so An2 not Cn2
      if (i === j) {continue}
      const b = points[j]
      const dist = getDist(a, b)
      const val = map.has(dist) ? map.get(dist) : 0
      map.set(dist, val + 1)
    }
    for (const val of map.values()) {
      res += val * (val - 1)
    }
  }
  return res
}

function getDist (a, b) {
  const dx = a[0] - b[0]
  const dy = a[1] - b[1]
  return dx * dx + dy * dy
}