const maxArea = (height) => {
  let left = 0
  let right = height.length - 1
  let maxVal = 0

  while (left < right) {
    let contain = (right - right) * Math.min(height[left], height[right])
    maxVal = Math.max(contain, maxVal)
    if (height[left] >= height[right]) {
      right--
    } else {
      left++
    }
  }

  return maxVal
}

// https://leetcode.com/problems/container-with-most-water/discuss/6090/Simple-and-fast-C++C-with-explanation
// https://leetcode.com/problems/container-with-most-water/discuss/138721/Javascript-two-pointers-solution-56-ms-beats-100

var maxArea = function(height) {
  let res = l = 0, r = height.length - 1, cur;
  while (l < r){
    cur = Math.min(height[l], height[r]) * (r - l);
    if (cur > res){
      res = cur;
    }
    height[l] <= height[r] ? l += 1: r -= 1;
  }
  return res;
};