var gameOfLife = function (board) {
  //count and append new board to end of current board (in place) but ignore them for calculations
  let rows = board.length
  for (let row = 0; row < rows; row++) {
    let newRow = []
    for (let col = 0; col < board[0].length; col++) {
      let cell = board[row][col]
      let n = liveNeighbors(board, row, col, rows)
      if (cell) { //alive
        if (n < 2 || n > 3) {
          newRow.push(0)
        } else {
          newRow.push(1)
        }
      } else if (!cell && n == 3) { //dead to life
        newRow.push(1)
      } else { //stay dead
        newRow.push(0)
      }
    }

    board.push(newRow)
  }

  //delete old board, leaving only the new board
  for (let r = 0; r < rows; r++) {
    board.shift()
  }

}

function liveNeighbors (board, row, col, rows) {
  let liveCnt = 0
  if (row != 0) {
    liveCnt += board[row - 1][col]
    if (col != 0) {
      liveCnt += board[row - 1][col - 1]
    }

    if (col != board[0].length - 1) {
      liveCnt += board[row - 1][col + 1]
    }
  }

  if (row != rows - 1) {
    liveCnt += board[row + 1][col]
    if (col != 0) {
      liveCnt += board[row + 1][col - 1]
    }

    if (col != board[0].length - 1) {
      liveCnt += board[row + 1][col + 1]
    }
  }

  if (col != 0) {
    liveCnt += board[row][col - 1]
  }

  if (col != board[0].length - 1) {
    liveCnt += board[row][col + 1]
  }

  return liveCnt
}