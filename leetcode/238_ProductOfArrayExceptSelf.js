// https://leetcode.com/problems/product-of-array-except-self/discuss/65663/Javascript-solution-if-anyone-is-interested

const productExceptSelf = function (nums) {
  const output = []
  let leftMult = 1
  let rightMult = 1

  for (let i = nums.length - 1; i >= 0; i--) {
    output[i] = rightMult
    rightMult *= nums[i]
  }

  console.log(output)
  for (let j = 0; j < nums.length; j++) {
    output[j] *= leftMult
    leftMult *= nums[j]
  }
  console.log(output)
  console.log('------------')
  return output
}

const input = [1, 2, 3, 4]
//const input = [2, 4, 6, 8]
console.log(productExceptSelf(input))