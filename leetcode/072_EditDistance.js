var minDistance = function (word1, word2) {
  const m = word1.length
  const n = word2.length
  const dp = new Array(n + 1).fill(0)
  for (let i = 0; i <= m; i++) {
    let last
    for (let j = 0; j <= n; j += 1) {
      if (i === 0) {
        dp[j] = j
      } else if (j === 0) {
        last = dp[j]
        dp[j] = i
      } else {
        let temp = dp[j]
        dp[j] = Math.min(
          last + (word1[i - 1] === word2[j - 1] ? 0 : 1),
          dp[j - 1] + 1,
          dp[j] + 1)
        last = temp
      }
    }
  }

  return dp[n]
}