// const subsets = (nums) => {
//   let results = []
//   combine(nums, 0, [], results)
//   return results
// }
//
// const combine = (nums, index, partial, results) => {
//   if (index === nums.length) {
//     results.push(partial)
//     return
//   }
//
//   combine(nums, index + 1, partial, results)
//   combine(nums, index + 1, partial.concat(nums[index]), results)
// }

/**
 * @param {number[]} nums
 * @return {number[][]}
 */
// var subsets = function (nums) {
//   let result = []
//   combine(nums, 0, [], result)
//   return result
//
//   function combine (nums, index, partial, result) {
//     // console.log(index, partial)
//     if (index === nums.length) {
//       // console.log(partial)
//       result.push(partial.slice(0))
//       return
//     }
//     combine(nums, index + 1, partial, result)
//     combine(nums, index + 1, partial.concat(nums[index]), result)
//   }
// }

// var subsets = function (nums) {
//   let arr = []
//   let res = [[]]
//   for (let i = 0, len = nums.length; i < len; i++) {
//     for (let j = 0, resLen = res.length; j < resLen; j++) {
//       arr[j] = []
//       arr[j].push(...res[j])
//     }
//     for (let j = 0, arrLen = arr.length; j < arrLen; j++) {
//       arr[j].push(nums[i])
//     }
//     res.push(...arr)
//   }
//   arr = null
//   return res
// }
//
// var helper = function (nums, temp = [], result = [[]], start = 0) {
//   for (let i = start; i < nums.length; i++) {
//     //include the element
//     temp.push(nums[i])
//     // Push a copy because we will pop the array at line 8
//     // which will end up changing the array in result arr as well
//     result.push([...temp])
//     helper(nums, temp, result, i + 1)
//     //exclude which triggers backtrack
//     temp.pop()
//   }
//   return result
// }
// var subsets = function (nums) {
//   nums.sort((a, b) => a - b)
//   return helper(nums)
// }

const subsets = (nums) => {

}

console.log(subsets([1, 2, 3]))

