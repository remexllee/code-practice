// let findMedianSortedArrays = function (nums1, nums2) {
//
//   let totalLength = nums1.length + nums2.length
//   let midLength = totalLength / 2
//   let index1 = 0
//   let index2 = 0
//   let curr
//   let last
//
//   while ((index1 + index2) <= midLength) {
//     if (curr) {
//       last = curr
//     }
//     if (nums1[index1] === undefined || nums2[index2] === undefined) {
//       curr = nums1[index1] === undefined ? nums2[index2++] : nums1[index1++]
//     } else if (nums1[index1] <= nums2[index2]) {
//       curr = nums1[index1++]
//     } else {
//       curr = nums2[index2++]
//     }
//   }
//   return totalLength % 2 == 0 ? (last + curr) / 2 : curr
// }

// const findMedianSortedArrays = function (nums1, nums2) {
//   let n1 = nums1.length
//   let n2 = nums2.length
//   if (n1 > n2) {
//     return findMedianSortedArrays(nums2, nums1)
//   }
//
//   let k = ~~((n1 + n2 + 1) / 2)
//   let l = 0
//   let r = n1
//
//   while (l < r) {
//     let m1 = l + ~~((r - l) / 2)
//     let m2 = k - m1
//     if (nums1[m1] < nums2[m2 - 1]) {
//       l = m1 + 1
//     } else {
//       r = m1
//     }
//   }
//
//   let m1 = l
//   let m2 = k - 1
//
//   console.log(m1, m2)
//
//   let c1 = Math.max(
//     m1 <= 0 ? Number.MIN_SAFE_INTEGER : nums1[m1 - 1],
//     m2 <= 0 ? Number.MIN_SAFE_INTEGER : nums2[m2 - 1])
//
//   if ((n1 + n2) % 2 == 1) {
//     return c1
//   }
//
//   let c2 = Math.min(
//     m1 >= n1 ? Number.MAX_SAFE_INTEGER : nums1[m1],
//     m2 >= n2 ? Number.MAX_SAFE_INTEGER : nums2[m2]
//   )
//
//   return (c1 + c2) * 0.5
// }

const findMedianSortedArrays = (nums1, nums2) => {
  let m = nums1.length
  let n = nums2.length
  if (m > n) { // to ensure m<=n
    return findMedianSortedArrays(nums2, nums1)
  }
  let iMin = 0, iMax = m, halfLen = ~~((m + n + 1) / 2)
  while (iMin <= iMax) {
    let i = ~~((iMin + iMax) / 2)
    let j = halfLen - i
    if (i < iMax && nums2[j - 1] > nums1[i]) {
      iMin = i + 1 // i is too small
    }
    else if (i > iMin && nums1[i - 1] > nums2[j]) {
      iMax = i - 1 // i is too big
    }
    else { // i is perfect
      let maxLeft = 0
      if (i == 0) { maxLeft = nums2[j - 1] }
      else if (j == 0) { maxLeft = nums1[i - 1] }
      else { maxLeft = Math.max(nums1[i - 1], nums2[j - 1]) }
      if ((m + n) % 2 == 1) {
        return maxLeft
      }

      let minRight = 0
      if (i == m) { minRight = nums2[j] }
      else if (j == n) { minRight = nums1[i] }
      else { minRight = Math.min(nums2[j], nums1[i]) }

      return (maxLeft + minRight) / 2.0
    }
  }
}

const nums1 = [1, 3]
const nums2 = [2]

// const nums1 = [1, 5, 6, 10]
// const nums2 = [2, 3, 8, 9]

console.log(findMedianSortedArrays(nums1, nums2))
