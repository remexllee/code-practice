var findItinerary = function (tickets) {
  const map = {}
  const result = []
  for (let i = 0; i < tickets.length; i++) {
    const [from, to] = tickets[i]
    if (!map[from]) {
      map[from] = [to]
    } else {
      map[from].push(to)
    }
  }

  for (const prop in map) {
    map[prop].sort()
  }

  makeItinerary('JFK')

  function makeItinerary (from) {
    const tos = map[from]
    while (tos && tos.length > 0) {
      makeItinerary(tos.shift())
    }
    result.unshift(from)
  }

  return result
}

const ticket = [['MUC', 'LHR'], ['JFK', 'MUC'], ['SFO', 'SJC'], ['LHR', 'SFO']]
console.log(findItinerary(ticket))