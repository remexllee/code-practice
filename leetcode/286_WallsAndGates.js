function explore (i, maxY, j, maxX, rooms, d) {
  // out of range
  if (maxY === i || i < 0 || maxX === j || j < 0) return

  // it's already a better way
  if (rooms[i][j] < d) return

  // improve the path
  rooms[i][j] = d

  // explore up, down, left, right
  explore(i - 1, maxY, j, maxX, rooms, d + 1)
  explore(i + 1, maxY, j, maxX, rooms, d + 1)
  explore(i, maxY, j - 1, maxX, rooms, d + 1)
  explore(i, maxY, j + 1, maxX, rooms, d + 1)
}

var wallsAndGates = function (rooms) {
  for (let i = 0, maxY = rooms.length; i < maxY; i++) {
    for (let j = 0, maxX = rooms[i].length; j < maxX; j++) {
      if (rooms[i][j] === 0) {
        explore(i, maxY, j, maxX, rooms, 0)
      }
    }
  }
}