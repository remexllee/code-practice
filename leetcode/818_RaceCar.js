function racecar (target) {
  let queue = [[0, 1]]

  for (let len = 0, seen = new Set(); queue.length; len++) {
    const next = []
    for (let [pos, s] of queue) {
      if (pos === target) return len
      if (seen.has(pos + ':' + s) || pos < 0 || pos > 2 * target) continue

      seen.add(pos + ':' + s)
      next.push([pos + s, s + s])
      next.push([pos, s > 0 ? -1 : 1])
    }
    queue = next
  }
}