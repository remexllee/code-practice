/**
 * @param {string} s
 * @return {string[]}
 */
var removeInvalidParentheses = function (s) {
  let queue = new Set([s]) // use set as a Queue
  while (queue.size) {
    const next = new Set() // create new Queue for every level
    for (let item of queue) {
      if (isValid(item)) {
        return [...queue].filter(isValid)
      }
      for (let i = 0; i < item.length; i++) {

        const str = item.slice(0, i) + item.slice(i + 1)
        console.log(str, i, item.slice(0, i), ' + ', item.slice(i + 1))
        next.add(str)
      }
    }
    queue = next
  }
  return ['']
}

const isValid = (str) => {
  let count = 0
  for (let ch of str) {
    if (ch === '(') {
      count++
    } else if (ch === ')') {
      count--
    }

    if (count < 0) return false
  }

  return count === 0
}

console.log(removeInvalidParentheses('()())()'))

console.log('-------------------')
console.log('()())()'.slice(0, 0)) // ''
console.log('()())()'.slice(0, 1))