/**
 * @param {number[]} nums
 * @return {number}
 */
var findNumberOfLIS = function (nums) {

}

var twoSum = function (nums, target) {
  let map = new Map()

  for (let i = 0; i < nums.length; i++) {
    let diff = target - nums[i]
    if (map.has(diff)) {
      return [i, map.get(diff)]
    } else {
      map.set(nums[i], i)
    }
  }
  return []
}

console.log(twoSum([2, 7, 11, 15], 9))