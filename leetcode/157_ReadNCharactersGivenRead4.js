var solution = function (read4) {
  var internalBuf = []
  return function (buf, n) {
    let readChars = 0
    while (n > 0) {
      if (internalBuf.length === 0) {
        if (read4(internalBuf) === 0) {
          return readChars
        }
      }

      buf.push(internalBuf.shift())
      readChars++
      n--
    }
    return readChars
  }
}

var solution = function (read4) {
  /**
   * @param {character[]} buf Destination buffer
   * @param {number} n Maximum number of characters to read
   * @return {number} The number of characters read
   */
  return function (buf, n) {
    let eof = false      // end of file flag
    let total = 0        // total bytes have read
    let tmp = [] // temp buffer

    while (!eof && total < n) {
      let count = read4(tmp)

      // check if it's the end of the file
      eof = count < 4

      // get the actual count
      // if count is smaller means there is no more to read
      // if n - total is smaller means we don't have to read anymore
      // for example 'ab', 1 => a, if without this check, it will get `ab`
      count = Math.min(count, n - total)

      // copy from temp buffer to buf
      for (let i = 0; i < count; i++) {
        buf[total++] = tmp[i]
      }
    }

    return total
  }
}

var solution = function (read4) {
  let temp = []
  return function (buf, n) {
    while (buf.length < n) {
      // if no bytes unused from last call, read 4 bytes
      if (temp.length === 0) { read4(temp) }
      if (temp.length === 0) { break } // end of file
      while (buf.length < n && temp.length > 0) {
        buf.push(temp.shift()) // copy from temp to buf
      }
    }
    return buf.length
  }
}