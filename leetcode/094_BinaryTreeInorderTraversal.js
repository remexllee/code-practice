function inorderTraversal (root) {
  const stack = []
  const res = []

  while (root || stack.length) {
    if (root) {
      stack.push(root)
      root = root.left
    } else {
      root = stack.pop()
      res.push(root.val)
      root = root.right
    }
  }

  return res
}

const inorderTraversal = root => {
  if (!root) return []

  return [...inorderTraversal(root.left), root.val, ...inorderTraversal(root.right)]
}