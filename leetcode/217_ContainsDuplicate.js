// https://leetcode.com/problems/contains-duplicate/discuss/155867/Javascript-beats-97-using-set

var containsDuplicate = function (nums) {
  if (nums.length === 0) { return false }
  var set = new Set(nums)
  return !(set.size === nums.length)
}