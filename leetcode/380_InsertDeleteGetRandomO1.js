const RandomizedSet = function () {
  this.nums = [] // we save the actual values here
  this.pos = {}  // we save the indices of all our values here
}

RandomizedSet.prototype.insert = function (val) {
  if (this.pos[val] === undefined) {
    this.nums.push(val)                    // push the value into the array
    this.pos[val] = this.nums.length - 1   // save the last index into pos
    return true
  }
  return false
}

RandomizedSet.prototype.remove = function (val) {
  if (this.pos[val] !== undefined) {
    const valIdx = this.pos[val]                      // save index of this val
    const lastItem = this.nums[this.nums.length - 1]  // grab last item in array

    this.nums[valIdx] = lastItem   // swap the last item with val (in the array)
    this.pos[lastItem] = valIdx    // update indices in pos to reflect the swap

    // last item in array is now the val we want to remove
    this.nums.pop()       // remove val from nums array
    delete this.pos[val]  // remove val from pos object

    return true
  }
  return false
}

RandomizedSet.prototype.getRandom = function () {
  const idx = Math.floor(Math.random() * this.nums.length)
  return this.nums[idx]
}
