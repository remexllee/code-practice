// https://leetcode.com/problems/palindrome-linked-list/discuss/132756/Recursive-Javascript-solution-O(n)-space-and-memory-beats-98-of-solutions

var isPalindrome = function (head) { // Recursive solution
  if (head == null || head.next == null) return true // Empty LL or LL with length of 1

  let result = isPalindromeHelper(head, head)
  if (result != false) return true
  else return false
}

function isPalindromeHelper (slow, fast) {
  if (fast == null) return slow
  else if (fast.next == null) return slow.next

  let mirror = isPalindromeHelper(slow.next, fast.next.next)

  if (mirror == false) return false // mirror is bool type
  else if (slow.val != mirror.val) return false // mirror is Node type
  else if (slow.val == mirror.val) return mirror.next
}