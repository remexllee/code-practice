/**
 * @param {number} n
 * @return {boolean}
 */
// var isHappy = function(n) {
//   var tmp;
//
//   while (n !== 1) {
//     tmp = 0;
//     while (n > 0) {
//       var digit = Math.pow(n % 10, 2);
//       tmp += digit;
//       n = Math.floor(n / 10);
//
//       if (digit === n && digit !== 1) {
//         return false;
//       }
//     }
//
//     n = tmp;
//   }
//
//   return true;
// };

// https://leetcode.com/problems/happy-number/discuss/56956/My-straightforward-solution-in-JS
/**
 * @param {number} n
 * @return {boolean}
 */
const isHappy = function(n) {
  let seen = {}
  while (n !== 1 && !seen[n]) {
    seen[n] = true
    n = sumOfSquares(n)
  }
  return n === 1 ? true : false
};

const sumOfSquares = (numString) => numString.toString().split('').reduce((sum, num) => sum + Math.pow(num, 2), 0)