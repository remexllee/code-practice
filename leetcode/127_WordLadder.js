var ladderLength = function (beginWord, endWord, wordList) {
  const visited = new Set()
  wordList = new Set(wordList)
  const queue = [{word: beginWord, length: 1}]

  while (queue.length) {
    const curr = queue.shift()
    if (curr.word === endWord) {
      return curr.length
    }

    for (let i = 0; i < curr.word.length; i++) {
      for (let c of 'abcdefghijklmnopqrstuvwxyz') {
        const next = curr.word.substr(0, i) + c + curr.word.substr(i + 1)
        if (wordList.has(next) && !visited.has(next)) {
          visited.add(next)
          queue.push({
            word: next,
            length: curr.length + 1
          })
        }
      }
    }
  }
  return 0
}

// const wordList = ['hot', 'dot', 'dog', 'lot', 'log', 'cog']
// console.log(ladderLength('hit', 'cog', wordList))

const word = 'hit'

for (let i = 0; i < word.length; i++) {
  for (let j = 'a'.charCodeAt(0); j <= 'z'.charCodeAt(0); j++) {
    const next = word.substr(0, i) + String.fromCharCode(j) + word.substr(i + 1)
    console.log(next)
  }
  console.log('------------------------------')
}