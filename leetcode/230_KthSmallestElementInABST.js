// https://leetcode.com/problems/kth-smallest-element-in-a-bst/discuss/133005/JavaScript-solution

function kthSmallest (root, k) {
  return getOrderedArray(root)[k - 1]
}

function getOrderedArray (node) {
  if (!node) return []

  return [
    ...getOrderedArray(node.left),
    node.val,
    ...getOrderedArray(node.right)
  ]
}