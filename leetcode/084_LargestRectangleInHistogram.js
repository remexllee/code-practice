const largestRectangleArea = heights => {
  // need to debug
  let maxArea = 0

  if (heights === null || heights.length === 0) {
    return maxArea
  }

  let stack = []
  let i = 0

  while (i < heights.length) {
    if (stack.length === 0 || heights[stack[stack.length - 1]] <= heights[i]) {
      stack.push(i++)
    } else {
      let height = heights[stack.pop()]
      let width = stack.length === 0 ? i : i - stack[stack.length - 1] - 1
      maxArea = Math.max(height * width, maxArea)
    }
  }
  return maxArea
}
