class MovingAverage {
  constructor (size) {
    this.size = size
    this.sum = 0
    this.nums = []
  }

  next (val) {
    this.sum += val
    if (this.nums.length === this.size) {
      this.sum -= this.nums.shift()
    }
    this.nums.push(val)
    return this.sum / this.nums.length
  }
}