const removeNthFromEnd = (head, n) => {
  let n1 = new ListNode()
  let n2 = new ListNode()

  let dummy = n2

  n1.next = head
  n2.next = head

  while (n > 0 && n1) {
    n1 = n1.next
    n--
  }

  if (n > 0) {
    return head
  }

  while (n1 && n1.next) {
    n1 = n1.next
    n2 = n2.next
  }

  n2.next = n2.next.next
  return dummy.next
}
