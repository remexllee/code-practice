const TreeTool = require('../../Utils/TreeTool')

// Post - Order Traversal ( leftChild - rightChild - root )
var postorderTraversal = function (root) {
  if (!root) return []

  const result = []
  const stack = [root]

  while (stack.length) {
    let currentNode = stack.pop()
    result.unshift(currentNode.val)
    if (currentNode.left) stack.push(currentNode.left)
    if (currentNode.right) stack.push(currentNode.right)
  }

  return result
}

const root = TreeTool.generateBSTByHardCode()

// Expect I, J, D, F, B, K, G, H, C, A
console.log(postorderTraversal(root))