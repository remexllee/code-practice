var NumMatrix = function (matrix) {
  if (!matrix.length) return
  let dp = [new Array(matrix[0].length + 1).fill(0)]
  matrix.map((row, i) => {
    let prefixSum = 0
    dp[i + 1] = [0]
    row.map((val, j) => {
      prefixSum += val
      dp[i + 1][j + 1] = dp[i][j + 1] + prefixSum
    })
  })
  this.dp = dp

}

NumMatrix.prototype.sumRegion = function (row1, col1, row2, col2) {
  let dp = this.dp
  row2++
  col2++
  return dp[row2][col2] + dp[row1][col1] - dp[row1][col2] - dp[row2][col1]
}
