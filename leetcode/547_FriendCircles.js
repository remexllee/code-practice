var findCircleNum = function (M) {
  let personAlreadyInCircle = Array(M.length).fill(0)
  let count = 0

  for (let i = 0; i < M.length; i++) {
    // if this person is not in any circle
    // count++ and find all person connected to him and mark them in circle
    if (personAlreadyInCircle[i] === 0) {
      personAlreadyInCircle[i] = 1
      count++
      addRelationship(personAlreadyInCircle, i, M)
    }
  }
  return count
}

/**
 * Find all friend that is connected to person i in grid, update arr to indicate that they are in a circle.
 * @param {number[]} arr Array indicates if person i is in any circle
 * @param {number} i Index of person
 * @param {number[][]} grid Relationship grid
 */
function addRelationship (arr, i, grid) {
  for (let j = 0; j < grid.length; j++) {
    if (arr[j] === 0 && grid[i][j] === 1) {
      arr[j] = 1
      addRelationship(arr, j, grid)
    }
  }
}
