const dirs = [[1, 0], [-1, 0], [0, 1], [0, -1]]
var updateMatrix = function (matrix) {
  let queue = []
  let m = matrix.length
  let n = matrix[0].length
  for (var i = 0; i < m; i++) {
    for (var j = 0; j < n; j++) {
      if (matrix[i][j] === 0) {
        queue.push([i, j])
      } else {
        matrix[i][j] = Number.MAX_VALUE
      }
    }
  }

  while (queue.length) {
    const [i, j] = queue.shift()
    for (var k = 0; k < dirs.length; k++) {
      let newRow = dirs[k][0] + i
      let newCol = dirs[k][1] + j
      if (newRow < 0 || newRow >= m || newCol < 0 || newCol >= n || matrix[newRow][newCol] <= matrix[i][j] + 1) {
        continue
      }

      matrix[newRow][newCol] = matrix[i][j] + 1
      queue.push([newRow, newCol])
    }
  }

  return matrix
}