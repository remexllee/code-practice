// var firstUniqChar = function (s) {
//   let map = s.split('').reduce((acc, char) => {
//     if (acc.hasOwnProperty(char)) acc[char]++
//     else acc[char] = 1
//     return acc
//   }, {})
//
//   for (let i = 0; i < s.length; i++) {
//     if (map[s[i]] === 1) return i
//   }
//   return -1
// }

var firstUniqChar = function (s) {
  let map = {}

  for (let c of s) {
    map[c] = map[c] ? map[c] + 1 : 1
  }

  for (let i = 0; i < s.length; i++) {
    if (map[s[i]] === 1) {
      return i
    }
  }
  return -1
}

const s1 = 'leetcode'
const s2 = 'loveleetcode'

console.log(firstUniqChar(s1))
console.log(firstUniqChar(s2))