function traverseTree (node, parentNode) {
  if (node === null) {return}
  node.parent = parentNode
  traverseTree(node.left, node)
  traverseTree(node.right, node)
}

var distanceK = function (root, target, K) {
  if (root === null || target === null || K < 0) {return []}
  traverseTree(root, null)
  const q = []
  q.push(target)
  let dist = 0
  const nodes = []
  let visited = {}
  while (q.length > 0) {
    const qLen = q.length
    for (let i = 0; i < qLen; i++) {
      const qTop = q.shift()
      visited[qTop.val] = true
      if (dist === K) {
        nodes.push(qTop.val)
      }
      if (qTop.left && !visited[qTop.left.val]) q.push(qTop.left)
      if (qTop.right && !visited[qTop.right.val]) q.push(qTop.right)
      if (qTop.parent && !visited[qTop.parent.val]) q.push(qTop.parent)
    }
    dist++
  }
  return nodes
}
