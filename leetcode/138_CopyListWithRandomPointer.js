// https://leetcode.com/problems/copy-list-with-random-pointer/discuss/136000/Clean-JavaScript-solution-using-hashmap

function copyRandomList (head) {
  return copy(head, {})
}

function copy (node, map) {
  if (!node) return node
  if (map[node.label]) return map[node.label]

  let n = new RandomListNode(node.label)

  map[node.label] = n

  n.next = copy(node.next, map)
  n.random = copy(node.random, map)

  return n
}

// https://leetcode.com/problems/copy-list-with-random-pointer/discuss/43520/JavaScript-O(n)-solution-using-ES6-Map

var copyRandomList = function (head) {
  if (!head) return null
  const dummy = new RandomListNode()
  const map = new Map()

  let src = head
  let dest = dummy
  while (src && !map.has(src)) {
    dest.next = new RandomListNode(src.label)
    map.set(src, dest.next)
    src = src.next
    dest = dest.next
  }

  for (let [src, dest] of map) {
    dest.random = map.get(src.random) || null
  }

  return dummy.next
}