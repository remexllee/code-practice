const array0to14 = Array.from(Array(15).keys())
// [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ]

const array1to15 = Array.from(new Array(15), (val, index) => index + 1)
console.log(array1to15)

// Create array sequence `[0, 1, ..., N-1]` in one line
// https://www.jstips.co/en/javascript/create-range-0...n-easily-using-one-line/

console.log(new Array(5))
console.log(Array(5))

/**
 * @param {number} n
 * @return {string[]}
 */


var fizzBuzz = function (n) {
  return Array.from(Array(n), (val, index) => index + 1).map(number => {
    if (number % 15 === 0) {
      return 'FizzBuzz'
    } else if (number % 5 === 0) {
      return 'Buzz'
    } else if (number % 3 === 0) {
      return 'Fizz'
    } else {
      return number.toString()
    }
  })
}

// not fast