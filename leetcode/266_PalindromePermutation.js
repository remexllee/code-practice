// var canPermutePalindrome = function (s) {
//   let map = new Map()
//   for (let i = 0; i < s.length; i++) {
//     if (map.has(s[i])) {
//       map.set(s[i], map.get(s[i]) + 1)
//     } else {
//       map.set(s[i], 1)
//     }
//   }
//   return [...map.values()].filter(value => value % 2 === 1).length <= 1
// }
//

var canPermutePalindrome = function (s) {
  let map = {}
  for (let c of s) {
    map[c] = map[c] ? map[c] + 1 : 1
  }
  return result = Object.values(map).filter(value => value % 2 === 1 ).length <= 1
}

// console.log(canPermutePalindrome('aab'))
console.log(canPermutePalindrome('aaa'))