var minMeetingRooms = function (intervals) {
  let sortByStart = intervals.slice().sort((a, b) => a.start - b.start)
  let sortByEnd = intervals.slice().sort((a, b) => a.end - b.end)

  let endIndex = 0
  let rooms = 0
  // for loop in the length of intervals
  for (let i = 0; i < intervals.length; i++) {
    if (sortByStart[i].start < sortByEnd[endIndex].end) {
      rooms++
    } else {
      endIndex++
    }
  }
  return rooms
}

//console.log(minMeetingRooms([[2, 15], [36, 45], [9, 29], [16, 23], [4, 9]]))
const example = [
  {start: 0, end: 30},
  {start: 5, end: 10},
  {start: 15, end: 20}
]

// console.log('Example: ', example)
console.log(minMeetingRooms(example))