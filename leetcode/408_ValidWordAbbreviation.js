var validWordAbbreviation = function (word, abbr) {
  if (word === null || abbr === null) return false
  var num = 0
  var idx = 0

  abbr = abbr.split('')

  for (var c in abbr) { // in for index, of for item
    if (abbr[c] === '0' && num === 0) return false // for case func ('a', '01')
    if (abbr[c] >= '0' && abbr[c] <= '9') {
      num = num * 10 + ~~(abbr[c])
    } else {
      idx += num
      if (idx >= word.length || abbr[c] != word.charAt(idx)) return false
      num = 0
      idx++
    }
  }

  return idx + num == word.length
}

const word = 'internationalization'
const abbr = 'i12iz4n'
console.log(validWordAbbreviation(word, abbr))


// have a bug
// var validWordAbbreviation = function (word, abbr) {
//   let array = abbr.split(/(\d+)/)
//
//   let offset = 0
//   for (let item of array) {
//     if (item !== '') {
//       if (~~(item) > 0) {
//         offset += ~~(item)
//       } else {
//         if (word.substring(offset, offset + item.length) !== item) {
//           return false
//         } else {
//           offset += item.length
//         }
//       }
//     }
//   }
//   return true
// }

// console.log(validWordAbbreviation('a', '2'))