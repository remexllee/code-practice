var longestConsecutive = function (nums) {
  const table = new Set(nums)

  let highestSeq = 0
  table.forEach(num => {
    if (table.has(num - 1)) {
      return
    }
    let next = num + 1
    while (table.has(next)) {
      next += 1
    }
    if (next - num > highestSeq) {
      highestSeq = next - num
    }
  })
  return highestSeq
}