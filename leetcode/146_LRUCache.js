class LRUCache {
  constructor (capacity) {
    this.capacity = capacity
    this.map = new Map()
  }

  get (key) {
    if (!this.map.has(key)) { return -1 }
    let val = this.map.get(key)
    this.map.delete(key)
    this.map.set(key, val)
    return val
  }

  put (key, value) {
    if (this.map.has(key)) { this.map.delete(key) }
    this.map.set(key, value)
    let keys = this.map.keys()
    while (this.map.size > this.capacity) { this.map.delete(keys.next().value) }
  }
}

LRUCache = cache = new LRUCache(2)

cache.put(1, 1)
cache.put(2, 2)
console.log(cache.get(1))
cache.put(3, 3)
console.log(cache.get(2))
cache.put(4, 4)
console.log(cache.get(1))
console.log(cache.get(3))
console.log(cache.get(4))

// O(1) solution
// https://leetcode.com/problems/lru-cache/discuss/45962/JavaScript-O(1)-time-using-map-and-doubly-linked-list