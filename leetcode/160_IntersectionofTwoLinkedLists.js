// https://leetcode.com/problems/intersection-of-two-linked-lists/discuss/142888/two-pointers-Javascript-solution

var getIntersectionNode = function (headA, headB) {
  if (!headA || !headB) return null
  var p1 = headA
  var p2 = headB
  while (p1 && p2 && p1 !== p2) {
    p1 = p1.next
    p2 = p2.next

    if (p1 == p2) return p1
    if (!p1) p1 = headB
    if (!p2) p2 = headA
  }
  return p1
}