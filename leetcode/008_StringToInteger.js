/**
 * @param {string} str
 * @return {number}
 */
var myAtoi = function (str) {
  let num = 0
  let baseCharCode = '0'.charCodeAt(0)
  let sign = 1

  str = str.trim()
  if (str[0] === '+' || str[0] === '-' ) {
    if (str[0] === '-') {
      sign = -1
    }

    str = str.substring(1)
  }

  for (let i = 0; i < str.length; i++) {
    let c = str[i]
    let charCode = c.charCodeAt(0) - baseCharCode

    if (0 <= charCode && charCode <=9) {
      num *= 10
      num += charCode
    } else {
      break
    }
  }

  let maxInt = Math.pow(2, 31) - 1
  let minNegInt = -Math.pow(2, 31)

  num = sign * num

  if (0 < num && maxInt < num) {
    return maxInt
  }

  if (num < 0 && num < minNegInt) {
    return minNegInt
  }

  return num
}

console.log(myAtoi('42'))
console.log(myAtoi('-42'))
console.log(myAtoi('4193 with words'))
console.log(myAtoi('words and 987'))
console.log(myAtoi('-91283472332'))
