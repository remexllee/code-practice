var compareVersion = function (version1, version2) {
  const v1 = version1.split('.').map(Number)
  const v2 = version2.split('.').map(Number)
  const shorter = v1.length < v2.length ? v1.length : v2.length
  let i
  for (i = 0; i < shorter; i++) {
    if (v1[i] > v2[i]) return 1
    if (v1[i] < v2[i]) return -1
  }
  while (v1[i] !== undefined) {
    if (v1[i] !== 0) return 1
    i++
  }
  while (v2[i] !== undefined) {
    if (v2[i] !== 0) return -1
    i++
  }
  return 0
}