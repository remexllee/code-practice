var countNodes = function (root) {
  if (!root) return 0
  if (root.left === null) return 1
  let height = 0
  let nodesum = 0
  let curNode = root
  while (curNode.left) {
    nodesum += (1 << height)
    height++
    curNode = curNode.left
  }
  let totalNode = nodesum + countLastLevel(root, height)
  return totalNode
}

function countLastLevel (root, height) {
  if (height === 1) {
    if (root.right !== null) {
      return 2
    } else if (root.left !== null) {
      return 1
    } else {
      return 0
    }
  }
  let midNode = root.left
  let curHeight = 1
  while (curHeight < height) {
    curHeight++
    midNode = midNode.right
  }
  if (midNode === null) {
    return countLastLevel(root.left, height - 1)
  }
  else {
    return (1 << (height - 1)) + countLastLevel(root.right, height - 1)
  }
}