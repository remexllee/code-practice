const canJump = (nums) => {
  let numLeft = nums[0]

  for (let i = 1; i < nums.length; i++) {
    numLeft--
    if (numLeft < 0) {
      return false
    }
    numLeft = Math.max(nums[i], numLeft)
  }

  return numLeft >= 0
}
