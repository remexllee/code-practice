var sortedArrayToBST = function (nums) {
  if (!nums) return null

  const helper = (low, high) => {
    if (low > high) return null

    const mid = ~~((high + low) / 2)
    const root = new TreeNode(nums[mid])
    root.left = helper(low, mid - 1)
    root.right = helper(mid + 1, high)
    return root
  }

  return helper(0, nums.length - 1)
}