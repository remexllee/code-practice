const NestedIterator = function (nestedList) {
  const flattenArray = (nestedList, list) => {
    for (var i = 0; i < nestedList.length; i++) {
      if (nestedList[i].isInteger()) {
        list.push(nestedList[i].getInteger())
      } else {
        flattenArray(nestedList[i].getList(), list)
      }
    }
  }

  this.list = []
  this.index = -1
  flattenArray(nestedList, this.list)
}

NestedIterator.prototype.hasNext = function () {
  return this.index < this.list.length - 1
}

NestedIterator.prototype.next = function () {
  this.index++
  return this.list[this.index]
}