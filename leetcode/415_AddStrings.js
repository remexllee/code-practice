var addStrings = function (num1, num2) {
  let output = ''
  let carry = 0
  let p1 = num1.length - 1
  let p2 = num2.length - 1
  while (p1 >= 0 || p2 >= 0 || carry) {
    let val = carry
    if (p1 >= 0) {
      val += parseInt(num1[p1])
      p1--
    }
    if (p2 >= 0) {
      val += parseInt(num2[p2])
      p2--
    }
    output = val % 10 + output
    carry = Math.floor(val / 10)
  }
  return output
}