/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
  let result1 = 0
  let result2 = 0

  for (let i = 0; i < nums.length; i = i + 2) result1 += nums[i]
  for (let i = 1; i < nums.length; i = i + 2) result2 += nums[i]

  return Math.max(result1, result2)
}

const test = [1, 2, 3, 1]
const test2 = [2, 7, 9, 3, 1]
const test3 = [2, 1, 1, 2] // failure case

// 牛逼解法
const rob1 = function (nums) {
  if (nums.length === 0) { return 0 }
  if (nums.length === 1) { return nums[0] }
  const totals = [nums[0], Math.max(nums[0], nums[1])]
  for (let i = 2; i < nums.length; i++) {
    totals.push(Math.max(nums[i] + totals[i - 2], totals[i - 1]))
  }
  return totals[totals.length - 1]
}

console.log(rob1(test))
// console.log(rob(test2))