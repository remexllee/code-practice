var KthLargest = function (k, nums) {
  this.k = k
  this.nums = nums.sort(function (a, b) {return b - a})
}

const getUpperBoundDescOrder = (nums, target) => {
  let left = 0
  let right = nums.length

  while (left < right) {
    let mid = ~~(left + (right - left) / 2)
    if (nums[mid] > target) {
      left = mid + 1
    } else {
      right = mid
    }
  }
  return left
}

KthLargest.prototype.add = function (val) {
  let l = this.nums.length
  if (l < 1) {
    this.nums.push(val)
    return val
  }
  let upper = getUpperBoundDescOrder(this.nums, val)
  if (upper === l - 1) {
    this.nums.push(val)
  }
  this.nums.splice(upper, 0, val)
  return this.nums[this.k - 1]
}

// let k = 3
// let arr = [1, 2, 3, 5, 6, 7, 8]
// let kthLargest = new KthLargest(3, arr)
// console.log(kthLargest.add(4))

let k = 3
let arr = [4, 5, 8, 2]
let kthLargest = new KthLargest(3, arr)
console.log(kthLargest.add(3))   // returns 4
console.log(kthLargest.add(5))   // returns 5
console.log(kthLargest.add(10))  // returns 5
console.log(kthLargest.add(9))   // returns 8
console.log(kthLargest.add(4))   // returns 8

// let arr = [5, -1]
// let kthLargest = new KthLargest(3, arr)
// console.log(kthLargest.add(2))   // returns 4
// console.log(kthLargest.add(1))   // returns 5
// console.log(kthLargest.add(-1))  // returns 5
// console.log(kthLargest.add(3))   // returns 8
// console.log(kthLargest.add(4))   // returns 8



