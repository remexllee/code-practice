function TreeNode (val) {
  this.val = val
  this.left = this.right = null
}

/***************
 *
 *      1
 *     / \
 *    2  3
 *   / \
 *  4  5
 *
 *  preOrder: 1, 2, 4, 5, 3
 *  inOrder: 4, 2, 5, 1, 3
 *  PostOrder: 4, 5, 2, 3, 1
 *
 ********************/

const root = new TreeNode(1)
const left1 = new TreeNode(2)
const right1 = new TreeNode(3)
const left1left = new TreeNode(4)
const left1right = new TreeNode(5)

root.left = left1
root.right = right1
left1.left = left1left
left1.right = left1right

const isValidBST = root => {
  if (!root) return true
  let stack = []
  let pre = null

  while (root || stack.length !== 0) {
    while (root) {
      stack.push(root)
      root = root.left
    }

    root = stack.pop()
    if (pre && root.val <= pre.val) return false
    pre = root
    root = root.right
  }

  return true
}

const inorderTraversal = (root) => {
  let list = []
  if (!root) return list
  let stack = []
  while (root || stack.length) {
    while (root) {
      stack.push(root)
      root = root.left
    }

    root = stack.pop()
    list.push(root.val)
    root = root.right
  }
  return list
}

var isValidBST = function (root) {
  if (!root) {
    return true // Sanity check for passing test case '[]'
  }

  function helper (root, min, max) {
    if (!root) {
      return true // We hit the end of the path
    }

    if ((min !== null && root.val <= min) || (max !== null && root.val >= max)) {
      return false // current node's val doesn't satisfy the BST rules
    }

    // Continue to scan left and right
    return helper(root.left, min, root.val) && helper(root.right, root.val, max)
  }

  return helper(root, null, null)
}

console.log(isValidBST(root))
console.log(inorderTraversal(root))