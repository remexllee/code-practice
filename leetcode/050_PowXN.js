// https://segmentfault.com/a/1190000003756872

/**
 * @param {number} x
 * @param {number} n
 * @return {number}
 */
var myPow = function (x, n) {
  if (n < 0) {
    // n为负数， 返回倒数
    return 1 / pow(x, -n)
  } else {
    return pow(x, n)
  }
}

const pow = (x, n) => {
  // 递归终止条件
  if (n === 0) {
    return 1
  }
  if (n === 1) {
    return x
  }
  let val = pow(x, ~~(n / 2)) // use `>> 1` will cause max stack over flow
  // 根据奇数还是偶数返回不同的值
  if (n % 2 === 0) {
    return val * val
  } else {
    return val * val * x
  }
}

console.log(myPow(2.00000, 10))
// console.log(myPow(2.10000, 3))
// console.log(myPow(2.00000, -2))
