// pass
var isValid = function (s) {
  const pairs = {')': '(', '}': '{', ']': '['}

  const stack = []
  for (let char of s) {
    const lastStackChar = stack[stack.length - 1]
    if (pairs.hasOwnProperty(char) && lastStackChar === pairs[char]) {
      stack.pop()
    } else {
      stack.push(char)
    }
  }

  return stack.length === 0
}

// https://leetcode.com/problems/valid-parentheses/discuss/156647/Semantic-JavaScript-Stack