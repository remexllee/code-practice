var isSymmetric = function (root) {
  if (root === null) return true
  let queL = [], queR = []
  queL.push(root.left)
  queR.push(root.right)

  while (queL.length !== 0 && queR.length !== 0) {
    let l = queL.shift()
    let r = queR.shift()
    if (l === null && r === null) continue
    else if (l === null || r === null) return false
    else if (l.val != r.val) {
      return false
    }
    queL.push(l.left)
    queL.push(l.right)
    queR.push(r.right)
    queR.push(r.left)
  }
  return (queL.length === 0 && queR.length === 0)
}
