const binarySearchT1 = (nums, target) => {
  if (nums === null || nums.length === 0) return -1

  let left = 0
  let right = nums.length - 1

  while (left <= right) {
    let mid = ~~(left + (right - left) / 2)
    if (nums[mid] === target) return mid
    else if (nums[mid] < target) left = mid + 1
    else right = mid - 1
  }

  return -1
}

const input = [1, 2, 3, 13, 15, 26, 57, 78]
// console.log(binarySearchT1(input, 3))
// console.log(binarySearchT1(input, 10))

const binarySearchT2 = (nums, target) => {
  if (!nums || nums.length === 0) return -1

  let left = 0
  let right = nums.length

  while (left < right) {
    let mid = ~~(left + (right - left) / 2)
    if (nums[mid] < target) left = mid + 1
    else right = mid
  }

  // Post-processing
  // End Condition: left == right
  console.log(left, right)
  if (left != nums.length && nums[left] === target) return left
  return -1
}

// console.log(binarySearchT2(input, 3))
// console.log(binarySearchT2(input, 90))

const binarySearchT3 = (nums, target) => {
  if (nums === null || nums.length === 0) return -1
  let left = 0

  let right = nums.length - 1
  while (left + 1 < right) {
    let mid = ~~(left + (right - left) / 2)
    if (nums[mid] < target) left = mid
    else right = mid
  }

  // Post-processing
  // End condition: left + 1 = right

  console.log(left, right)
  if (nums[left] === target) return left
  if (nums[right] === target) return right
  return - 1
}

console.log(binarySearchT3(input, 3))
console.log(binarySearchT3(input, 90))
