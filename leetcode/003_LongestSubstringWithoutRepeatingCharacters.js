var lengthOfLongestSubstring = function (s) {
  if (!s || s === undefined || s.length === 0) return 0
  const len = s.length
  const set = new Set()
  let ans = 0, i = 0, j = 0

  while (i < len && j < len) {
    if (!set.has(s[j])) {
      set.add(s[j++])
      ans = Math.max(ans, j - i)
    } else {
      set.delete(s[i++])
    }
  }
  return ans
}
console.log(lengthOfLongestSubstring('abcabcbb'))
console.log(lengthOfLongestSubstring('bbbbb'))
console.log(lengthOfLongestSubstring('pwwkew'))
