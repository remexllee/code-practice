// function getCount (mid, m, n) {
//   let c = n, count = 0
//   for (let r = 1; r <= m; r++) {
//     while (c >= 1 && r * c > mid) c--
//     count += c
//   }
//   return count
// }
//
// var findKthNumber = function (m, n, k) {
//   let lo = 1, hi = m * n
//
//   while (lo < hi) {
//     let mid = lo + Math.floor((hi - lo) / 2)
//     if (getCount(mid, m, n) < k) lo = mid + 1
//     else hi = mid
//   }
//   return lo
// }

const getCount = (target, m, n) => {
  let count = 0
  for (let i = 0; i <= m; i++) {
    let temp = Math.min(~~(target / i), n)
    count += temp
  }
  return count
}

var findKthNumber = function (m, n, k) {
  let low = 1
  let high = m * n

  while (low < high) {
    let mid = (high + low) >> 1
    let count = getCount(mid, m, n)
    if (count >= k) {
      high = mid
    } else {
      low = mid + 1
    }
  }
  return low
}

console.log(findKthNumber(3, 3, 5))
console.log(findKthNumber(2, 3, 6))