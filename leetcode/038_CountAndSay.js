// https://leetcode.com/problems/count-and-say/discuss/123618/Simple-easy-to-read-Javascript-solution-(56ms-beats-97.61-solutions)

/**
 * @param {number} n
 * @return {string}
 */
var countAndSay = function (n) {
  function countRepeats (word) {
    let count = 0, i = 0
    let number = word[0]
    let result = ''
    for (i = 0; i < word.length; i++) {
      if (word[i] === number) {
        count++
      } else {
        result += count + word[i - 1]
        count = 1
        number = word[i]
      }
    }
    return result + count + word[i - 1]
  }

  let lastResult = '1'
  for (let i = 1; i < n; i++) {
    lastResult = countRepeats(lastResult)
  }

  return lastResult
}

console.log(countAndSay(1))
console.log(countAndSay(2))
console.log(countAndSay(3))
console.log(countAndSay(4))
console.log(countAndSay(5))
