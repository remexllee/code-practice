var splitBST = function (root, V) {
  if (root == null) {
    return [null, null]
  }

  if (root.val > V) {
    var left = splitBST(root.left, V)
    root.left = left[1]
    return [left[0], root]
  } else {
    var right = splitBST(root.right, V)
    root.right = right[0]
    return [root, right[1]]
  }
}

// var splitBST = function (root, V) {
//   var l = new TreeNode(0)
//   var r = new TreeNode(0)
//   var res = []
//   helper(root, V, l, r)
//   res.push(r.right)
//   res.push(l.left)
//   return res
// }
//
// var helper = function (node, v, l, r) {
//   if (node === null) return
//   if (node.val <= v) {
//     r.right = node
//     var right = node.right
//     node.right = null
//     helper(right, v, l, node)
//   }
//   else {
//     l.left = node
//     var left = node.left
//     node.left = null
//     helper(left, v, node, r)
//   }
// }