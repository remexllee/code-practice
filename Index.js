

//CHALLENGE:
// Implement your own Sorted Linked List of Users.

// The User class is a class that holds the User's name and age.

//  The list should be sorted in ascending order based on their age.

//  The list should have:
//      - `first()` method that returns the first (youngest) User
//      - `add(User user)` method that adds a User
//      - `printAllUsers()` method that prints all users from the list to the console

// Hint:
// Start by creating a User class

class User {
  constructor (name, age) {
    this.name = name
    this.age = age
  }
}

class Node {
  constructor (user) {
    this.user = user
    this.next = null
  }
}

class UserList {
  constructor () {
    this.head = null
  }

  first () {
    return this.head
  }

  add (user) {
    if (this.head) {
      let current = this.head
      let previous = null
      while (current) {
        if (user.age < current.user.age) {
          if (previous) {
            previous.next = new Node(user)
            previous.next.next = current
          } else {
            this.head = new Node(user)
            this.head.next = current
          }
          break
        }
        previous = current
        current = current.next
      }

    } else {
      this.head = new Node(user)
    }
  }

  printAllUser() {
    let current = this.head
    while(current) {
      console.log(`${current.user.name}: ${current.user.age}`)
      current = current.next
    }
  }
}

const user1 = new User('Tom', 21)
const user2 = new User('Jerry',11)
const user3 = new User('Mary', 41)
const user4 = new User('Mary', 1)

// 21 11 41

const userList = new UserList()
userList.add(user1)
userList.add(user2)
userList.add(user3)
userList.add(user4)

// console.log(userList.first())
userList.printAllUser()