# Binary Tree

### Traversal list
* [94. Binary Tree Inorder Traversal](https://leetcode.com/problems/binary-tree-inorder-traversal/description/)
* [102. Binary Tree Level Order Traversal](https://leetcode.com/problems/binary-tree-level-order-traversal/description/)
* [103. Binary Tree Zigzag Level Order Traversal](https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/description/)
* [105. Construct Binary Tree from Preorder and Inorder Traversal](https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/description/)
* [106. Construct Binary Tree from Inorder and Postorder Traversal](https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/description/)
* [107. Binary Tree Level Order Traversal II](https://leetcode.com/problems/binary-tree-level-order-traversal-ii/description/)
* [144. Binary Tree Preorder Traversal](https://leetcode.com/problems/binary-tree-preorder-traversal/description/)
* [145. Binary Tree Postorder Traversal](https://leetcode.com/problems/binary-tree-postorder-traversal/description/)


### Recursion list

Ethan Li的讲解
https://segmentfault.com/a/1190000003748894

jeantimex的讲解 101. Symmetric Tree
https://leetcode.com/problems/symmetric-tree/discuss/33073/JavaScript-recursive-and-iterative-solutions

jeantimex的讲解 99. Recover Binary Search Tree
https://leetcode.com/problems/recover-binary-search-tree/discuss/32532/My-JavaScript-solution

ethan li的讲解 Count Complete Tree Nodes
https://segmentfault.com/a/1190000003818177


* [98. Validate Binary Search Tree](https://leetcode.com/problems/validate-binary-search-tree/description/)
* [99. Recover Binary Search Tree](https://leetcode.com/problems/recover-binary-search-tree/discuss/32637/JavaScript-solution-with-inorder-traversal)
* [101. Symmetric Tree](https://leetcode.com/problems/symmetric-tree/description/)
* [111. Minimum Depth of Binary Tree](https://leetcode.com/problems/minimum-depth-of-binary-tree/description/)
* [104. Maximum Depth of Binary Tree](https://leetcode.com/problems/maximum-depth-of-binary-tree/description/)
* [110. Balanced Binary Tree](https://leetcode.com/problems/balanced-binary-tree/description/)
* [114. Flatten Binary Tree to Linked List](https://leetcode.com/problems/flatten-binary-tree-to-linked-list/description/)
* [236. Lowest Common Ancestor of a Binary Tree](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/description/)
* [235. Lowest Common Ancestor of a Binary Search Tree](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/description/)

 