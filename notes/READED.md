# 求和问题的总结

## 题目list

* [1. Two Sum](https://leetcode.com/problems/two-sum/description/)
* [167. Two Sum II - Input array is sorted](https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/description/)
* [2 sum III - data structure design] 
* [653. Two Sum IV - Input is a BST](https://leetcode.com/problems/two-sum-iv-input-is-a-bst/description/)
* [15. 3Sum](https://leetcode.com/problems/3sum/description/)
* [16. 3Sum Closest](https://leetcode.com/problems/3sum-closest/description/)
* [3 sum smaller]
* [18. 4Sum](https://leetcode.com/problems/4sum/description/)
* [89. k Sum](https://www.lintcode.com/problem/k-sum/description)
* [90. k Sum II](https://www.lintcode.com/problem/k-sum/description)

## 总结

[3Sum 4Sum 3Sum Closet 多数和](https://segmentfault.com/a/1190000003740669#articleHeader1)

* two sum 如果只是返回值的话, 可以先sort. 但是如果返回index的话，需要使用hashmap。

