class Hero {
  constructor (name, specialAbility) {
    this._name = name
    this._specialAbility = specialAbility
  }

  getDetails () {
    return `${this._name} can ${this._specialAbility}`
  }
}

const IronMan = new Hero('Iron Man', 'fly')
const SpiderMan = new Hero('Spider Man', 'jump')
console.log(IronMan.getDetails())
console.log(SpiderMan.getDetails())