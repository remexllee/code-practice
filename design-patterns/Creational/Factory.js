class BallFactory {
  constructor () {
    this.createBall = function (type) {
      let ball
      if (type === 'football' || type === 'soccer') {
        ball = new Football()
      } else if (type === 'basketball') {
        ball = new Basketball()
      }
      return ball
    }
  }
}

class Football {
  constructor () {
    this._type = 'football'
    this.kick = function () {
      return 'You kicked the football'
    }
    this.roll = function () {
      return 'The football is rolling'
    }
  }
}

class Basketball {
  constructor () {
    this._type = 'basketball'
    this.bounce = function () {
      return 'You bounced the basketball'
    }
    this.roll = function () {
      return 'The basketball is rolling'
    }
  }
}

const factory = new BallFactory()
const basketball = factory.createBall('basketball')

console.log(basketball.constructor.name)
console.log(basketball.roll())
console.log(basketball.bounce())

console.log('----------------------')

const football = factory.createBall('football')
console.log(football.constructor.name)
console.log(football.roll())
console.log(football.kick())