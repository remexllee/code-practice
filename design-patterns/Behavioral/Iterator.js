class IteratorClass {
  constructor (data) {
    this.index = 0
    this.data = data
  }

  [Symbol.iterator] () {
    return {
      next: () => {
        if (this.index < this.data.length) {
          return {value: this.data[this.index++], done: false}
        } else {
          this.index = 0 // to reset iteration status
          return {done: true}
        }
      },
    }
  }
}

// using Generator
function * iteratorUsingGenerator (collection) {
  var nextIndex = 0

  while (nextIndex < collection.length) {
    yield collection[nextIndex++]
  }
}

const arr = ['Hi', 'Hello', 'Bye']
const collection = new IteratorClass(arr)
let i = 0
for (const val of collection) {
  console.log(val)
}

const gen = iteratorUsingGenerator(['Hi', 'Hello', 'Bye']);
console.log(gen.next().value)
console.log(gen.next().value)
console.log(gen.next().value)
console.log(gen.next().value)

let a = 65
let b = a ^ (1 << 5)
console.log(b)