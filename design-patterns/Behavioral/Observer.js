// http://www.thedevnotebook.com/2017/08/the-observer-pattern-in-javascript.html
class Subject {
  constructor () {
    this._observers = []
  }

  subscribe (observer) {
    this._observers.push(observer)
  }

  unsubscribe (observer) {
    this._observers = this._observers.filter(obs => observer !== obs)
  }

  fire (change) {
    this._observers.forEach(obsercer => obsercer.update(change))
  }
}

class Observer {
  constructor (state) {
    this.state = state
    this.initialState = state
  }

  update (change) {
    let state = this.state
    switch (change) {
      case 'INC':
        this.state = ++state
        break
      case 'DEC':
        this.state = --state
        break
      default:
        this.state = this.initialState
    }
  }
}

const sub = new Subject()
const obs1 = new Observer(1)
const obs2 = new Observer(20)

sub.subscribe(obs1)
sub.subscribe(obs2)
sub.fire('INC')
console.log(obs1.state) // 2
console.log(obs2.state) // 21
sub.fire('DEC')
console.log(obs1.state) // 1
console.log(obs2.state) // 20
sub.fire('INC')
sub.fire('INC')
sub.fire('INC')
sub.fire('Nothing') // reset to initial state
console.log(obs1.state) // 1
console.log(obs2.state) // 20
sub.unsubscribe(obs2)
sub.fire('DEC')
console.log(obs1.state) // 0
console.log(obs2.state) // 20 since not fire `DEC`

