/*
	It is a behavioural design pattern that allows encapsulation
	of alternative algorithms for a particular task. It defines a
	family of algorithms and encapsulates them in such a way that
	they are interchangeable at runtime without client interference or knowledge.
*/

// encapsulation
class Commute {
  travel (transport) {
    return transport.travelTime()
  }
}

class Vehicle {
  travelTime () {
    return this._timeTaken
  }
}

class Bus extends Vehicle {
  constructor () {
    super()
    this._timeTaken = 10
  }
}

class Taxi extends Vehicle {
  constructor () {
    super()
    this._timeTaken = 5
  }
}

class PersonalCar extends Vehicle {
  constructor () {
    super()
    this._timeTaken = 3
  }
}


const commute = new Commute()
console.log(commute.travel(new Taxi()))
console.log(commute.travel(new Bus()))
console.log(commute.travel(new PersonalCar()))