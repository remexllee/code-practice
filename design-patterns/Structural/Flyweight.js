class Icecream {
  constructor (flavour, price) {
    this.flavour = flavour
    this.price = price
  }
}

class IcecreamFactory {
  constructor () {
    this._icecreams = []
  }

  createIcecream (flavour, price) {
    let icecream = this.getIcecream(flavour)
    if (icecream) {
      return icecream
    } else {
      const newIcecream = new Icecream(flavour, price)
      this._icecreams.push(newIcecream)
      return newIcecream
    }
  }

  getIcecream (flavour) {
    return this._icecreams.find(icecream => icecream.flavour === flavour)
  }
}

const factory = new IcecreamFactory()
const vanilla = factory.createIcecream('vanilla', 10)
console.log(vanilla.constructor.name)
console.log(vanilla.flavour)
console.log(vanilla.price)

console.log('-------------------------------------')
factory.createIcecream('chocolate', 15)
const chocolate = factory.getIcecream('chocolate')
console.log(chocolate.constructor.name)
console.log(chocolate.flavour)
console.log(chocolate.price)

console.log('-------------------------------------')
const chocoVanilla = factory.createIcecream('chocolate and vanilla', 15)
const vanillaChoco = factory.createIcecream('chocolate and vanilla', 15)
console.log(chocoVanilla === vanillaChoco)