class Book {
  constructor (title, author, price) {
    this._title = title
    this._author = author
    this.price = price
  }

  getDetails () {
    return `${this._title} by ${this._author}`
  }
}

function giftWrap (book) {
  book.isGiftWrapped = true
  book.unwrap = function () {
    book.isGiftWrapped = false
    return `Unwrapped ${book.getDetails()}`
  }
  return book
}

function hardBindBook (book) {
  book.isHardBound = true
  book.price += 5
  return book
}

const alchemist = giftWrap(new Book('The Alchemist', `Paulo Coelho`, 10))
console.log(alchemist.isGiftWrapped)
console.log(alchemist.unwrap())
console.log(alchemist.isGiftWrapped)

console.log('---------------------')
const inferno = hardBindBook(new Book('Inferno', 'Dan Brown', 15))
console.log(inferno.isHardBound)
console.log(inferno.price)