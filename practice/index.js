// class TicTacToe {
//   constructor () {
//     this.board = [new Array(3).fill(''), new Array(3).fill(''), new Array(3).fill('')]
//   }
//
//   setToken (r, c, token) {
//     if (r < this.board.length || c < this.board[0].length) {
//       this.board[r][c] = token
//     }
//   }
//
//   isFullFilled () {
//     for (let i = 0; i < this.board.length; i++) {
//       for (let j = 0; j < this.board[i].length; j++) {
//         if (this.board[i][j] === '') {
//           return false
//         }
//       }
//     }
//     return true
//   }
//
//   print () {
//     for (let i = 0; i < this.board.length; i++) {
//       console.log(this.board[i].map(value => value ? value : '-').join('|'))
//     }
//   }
// }
//
// // const input = [
// //   ['X', '-', '-'],
// //   ['-', 'O', 'X'],
// //   ['-', '-', '-']
// // ]
//
//
//
// class TicTacToeAI {
//   constructor () {
//
//   }
//
//   move () {
//
//   }
//
//   inGame () {
//     const game = new TicTacToe()
//     game.setToken(0, 0, 'X')
//     if (game.isFullFilled()) {
//       console.log('Game Done')
//     } else {
//       // move
//     }
//     game.print()
//
//   }
// }
//
// const AIGame = new TicTacToeAI()
// AIGame.inGame()

const matrix = new Array(5).fill('').map(() => new Array(4).fill(''))
// console.log(matrix)
//
// const midHeight = ~~(matrix.length / 2)
// const midWidth = ~~(matrix[0].length / 2)
//
// console.log(midHeight, midWidth)
// matrix[midHeight][midHeight] = 2
// console.log(matrix)

const row = 3
const column = 3

const board = new Array(row).fill(0).map(() => new Array(column).fill(0))
console.log(board)
board[0][0] = 1
console.log(board)

function isFullFilled (board) {
  return !board.some(row => row.includes(1))
}
console.log(isFullFilled(board))